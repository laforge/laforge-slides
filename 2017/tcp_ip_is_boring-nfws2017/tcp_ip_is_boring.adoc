TCP/IP is Boring: Tour of Cellular Protocol Stacks
==================================================
:author:	Harald Welte <laforge@gnumonks.org>
:copyright:	2017 by Harald Welte (License: CC-BY-SA)
:backend:	slidy
:max-width:	45em


== Overview / Intro

[role="incremental"]
* everyone (here) is familiar in-depth with TCP/IP
* almost nobody (here) has insight into telecom protocols
* let me take you on a tour
[role="incremental"]
** where protocol stacks are deep
** where acronyms are plentiful
* some people also call this _applied protocol archeology_
* a lot of this is still in use, every day, by millions if not billions of subscribers

== Personal Background

* involved in non-profit ISP of early 1990ies
** built a lot of technology ourselves
** contact with UUCP, TCP/IP, OSPF, PPP, ISDN PRI/BRI, ...
* involved with netfilter/iptables 1999-2007
* always looking for interesting non-TCP/IP protocols
** 2006: OpenPCD / librfid: ISO14443 / ISO15693 RFID protocols
** 2008-present: Cellular Protocol Stacks

== Personal Philosophy

* communications systems and protocols are a big fascination
* I admit I'm even interested more in them for themselves, rather than
  for their use
* Personal conviction
** Every protocol (stack) should be well understood
** There should be FOSS for experimentation
*** transmitter
*** receiver
*** dissector (e.g. wireshark)

== The Internet-Centric World

I assume this is where most audience members are coming from:

* We assume TCP/IP + Ethernet are everywhere
* Most obscure protocols people might know: SCTP, DCCP
* Open Source implementations lead the market (Linux, BSD)
* IETF is open to anyone, no formal membership required
** you still need to be able to afford to travel to meetings

== Classic Digital Circuit Switched Telephony

* Based on 8kHz / 8bit PCM audio channels
* E1/T1 bit-synchronous interfaces (1.5/2MBps) 
* TDMA structure with 24/31 timeslots
* one 64k slot used for signaling
** HDLC framing (with CRC)
** MTP (_Message Transfer Part_) L2 + L3
** TUP (_Telephony User Part_) or ISUP (_ISDN User Part_) for call signaling

== Telephony Specification Bodies

* ITU-T: Technical ITU Specs for international network interfacing
* ANSI: American National Standards Institute
* ETSI: European Telecommunications Standardization Institute
* TTC: Japanese Telecommunication Technology Committee
* 3GPP: 3rd Generation Partnership Project


== Telephony Signaling Oddities

* Many countries/regions have their own dialects
** Even inside ETSI region, there are e.g. French + German variants(!)
* Differences even at very basic level such as address field size:
** ANSI: 24bit Point Codes
** ITU-T: 14bit Point Codes
** Japan: 16bit Point Codes

=> Specific Translators required at boundaries between national/international networks

== Conceptual Differences

* Circuit vs. Packet
** In TCP/IP, we assume sender/receiver is identified in each packet
** In Cellular: Sender/Receiver are often implicitly identified based on circuit / time-slot!
* Signaling vs. User Plane
** In TCP/IP, we assume user payload is above normal protocol stack
** In Cellular: Different protocol stacks, or no protocol / header at all for user plane!

== GSM Um
image::images/Gsm_structures.svg[]
~image by Tsaitgaist~

== Cellular Protocols: GSM Um

* Um (_U mobile_) modelled after ISDN U (User) interface
* L2: LAPDm (_LAPD mobile_) modelled after ISDN LAPD (Q.921)
* L3: CC (_Call Control_) almost identical to ISDN L3 (Q.931)
* L3: New RR (_Radio Resource_) for radio-specific aspects
* L3: New MM (_Mobility Management_) for subscriber mobility

== GSM Abis
image::images/Gsm_structures.svg[]
~image by Tsaitgaist~

== Cellular Protocols: GSM Abis

* Uses E1/T1 Line/Circuit from ISDN
* Signaling Slot
** L2: ISDN L2 (LAPD, Q.921)
** L3: RSL (TS 48.058) + OML (TS 12.21)
** L4+ RR/CC/MM of Um interface
* Traffic Slots
** 64k Slots divided in 16k sub-slots
** one 16k sub-slot for GSM-encoded voice
** TRAU (_Transcoder / Rate Adaption Unit_) Frames

== GSM A
image::images/Gsm_structures.svg[]
~image by Tsaitgaist~

== Cellular Protocols: GSM A

* Uses E1/T1 Line/Circuit from ISDN
* Signaling Slot
** SS7 MTP2 + MTP3
** SS7 SCCP (_Signaling Connection Control Part_) in Connection-Oriented Mode
** BSSAP/BSSMAP for BSC-MSC signaling (TS 48.008)
* Traffic Slots
** Uncompressed 64k PCM Audio like in ISDN

== GSM Core (H/E etc.)
image::images/Gsm_structures.svg[]
~image by Tsaitgaist~

== Cellular Protocols: Core Network / Roaming Interface

* Uses E1/T1 Line/Circuit from ISDN
* Cellular specific Signaling
** SS7 MTP2 + MTP3
** SS7 SCCP (_Signaling Connection Control Part_) in Connection-Less Mode
** TCAP (_Transaction Capabilities Application Part_)
** MAP (_Mobile Application Part_) + CAP (_CAMEL Application Part_)
*** specified in ASN.1 with Information Object Classes; BER encoding
* Voice Call Signaling
** SS7 MTP2 + MTP3
** SS7 ISUP (_ISDN User Part_)
* Traffic Slots
** Uncompressed 64k PCM Audio like in ISDN

== Cellular Protocols: SIM-ME Interface

* Pretty much like most other processor smart cards:
** ISO 7816-1 for physical interface
** ISO 7816-2 for electrical interface
** ISO 7816-3 for framing
** ISO 7816-4 _Inter-Industry Commands for Information Interchange_
* GSM TS 11.11 for detailed file/directory/APDU specs
* ETSI TS 102221 + 3GPP TS 31.102 for USIM

== Cellular Protocols: GPRS Um

* re-use existing GSM Um PHY
** same TDMA, modulation, ...
** new coding schemes (different amount of FEC, TS 45.002)
* L2: New RLC/MAC instead of LAPDm (TS 44.060)
** unacknowledged + acknowledged mode
** specified in new syntax: CSN.1 (Concrete Syntax Notation)
*** seems it was create specifically for GPRS
*** specification full of non-trivial syntax errors (till today!)
*** no FOSS code generators (till today!)
* L3: GPRS LLC (_Logical Link Control_, TS 44.064)
** unacknowledged + acknowledged mode
* L3: GPRS SNDCP (_Sub-Network Dependent Convergence Protocol_, TS 44.065)
* User-IP (or PPP) inside SNDCP

== GPRS Gb
image::images/Gsm_structures.svg[]
~image by Tsaitgaist~

== Cellular Protocols: GPRS Gb

* Between PCU (Protocol Control Unit) and SGSN (Serving GPRS Support Node)
* Classic Transport
** E1/T1 physical layer
** L2: Frame Relay
* Modern Transport
** IP + UDP replace E1/T1 + FR, or
** FR over GRE over IP
* L3: NS (_Network Services_, TS 48.016)
* L3: BSSGP (_BSS Gateway Protocol_, TS 48.018)
* Above: GPRS LLC as on GPRS Um interface

== GPRS Gp
image::images/Gsm_structures.svg[]
~image by Tsaitgaist~

== Cellular Protocols: GPRS Gp

* Between SGSN (Serving GPRS Support Node) and GGSN (GPRS Gateway Support Node)
* IP as transport layer (yay!)
* GTP (_GPRS Tunneling Protocol_, TS 29.060)
* User-IP traffic inside GTP

== GPRS Control Plane

image::images/gprs_control_stack.svg[width="100%"]

== GPRS User Plane

image::images/gprs_user_stack.svg[width="100%"]

== Cellular Protocols: UMTS (aka WCDMA aka 3G)

* Uu interface on radio: Completely new
* Iub interface NodeB -> RNC: Completely new
** RRC protocol: ASN.1; UPER encoding
* Iu-CS interface RNC -> MSC
** SS7 E1/MTP2/MTP3 (or MTP3b via ATM)
** SS7 SCCP Connection-Oriented
** RANAP (RAN Application Part): ASN.1; APER encoding
* Iu-PS interface RNC -> SGSN
** like Iu-CS above

== UMTS PS Control Plane

image::images/umts_ps_control.svg[width="100%"]

== UMTS PS User Plane

image::images/umts_ps_user.svg[width="100%"]

== Evolution towards IP Transport

* 1999-2007: IETF SIGTRAN WG
** Transmission of Signalling over IP
** SCTP as reliable transport
** Not one, but many "competing" stacks on top
*** Result: Various incompatible products

image::images/sigtran_stackings.svg[width="100%"]

== Further Information

* http://osmocom.org/ contains FOSS implementations of
** GSM Um, Abis, A
** GPRS Gb, Gp, Gi
** UMTS IuCS, IuPS, Iuh

== EOF

End of File
