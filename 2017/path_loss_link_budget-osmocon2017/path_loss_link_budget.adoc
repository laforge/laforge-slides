Path Loss and Link Budget
=========================
:author:	Harald Welte <hwelte@sysmocom.de>
:copyright:	2017 by Harald Welte (License: CC-BY-SA)
:backend:	slidy
:max-width:	45em

[[rf-path-loss]]
== Path Loss

A fundamental concept in planning any type of radio communications link
is the concept of 'Path Loss'.  Path Loss describes the amount of
signal loss (attenuation) between a receive and a transmitter.

As GSM operates in frequency duplex on uplink and downlink, there is
correspondingly an 'Uplink Path Loss' from MS to BTS, and a 'Downlink
Path Loss' from BTS to MS.  Both need to be considered.

It is possible to compute the path loss in a theoretical ideal
situation, where transmitter and receiver are in empty space, with no
surfaces anywhere nearby causing reflections, and with no objects or
materials in between them.  This is generally called the 'Free Space
Path Loss'.

[[rf-path-loss]]
== Path Loss

Estimating the path loss within a given real-world terrain/geography is
a hard problem, and there are no easy solutions.   It is impacted, among
other things, by

 * the height of the transmitter and receiver antennas
 * whether there is line-of-sight (LOS) or non-line-of-sight (NLOS)
 * the geography/terrain in terms of hills, mountains, etc.
 * the vegetation in terms of attenuation by foliage
 * any type of construction, and if so, the type of materials used in
   that construction, the height of the buildings, their distance, etc.
 * the frequency (band) used.  Lower frequencies generally expose better
   NLOS characteristics than higher frequencies.

The above factors determine on the one hand side the actual attenuation
of the radio wave between transmitter and receiver.  On the other
hand, they also determine how many reflections there are on this path,
causing so-called 'Multipath Fading' of the signal.

== Radio Propagation Models

Over decades, many different radio propagation models have been designed
by scientists and engineers.  They might be based on empirical studies
condensed down into relatively simple models, or they might be based on
ray-tracing in a 3D model of the terrain.

Several companies have developed (expensive, proprietary) simulation
software that can help with this process in detail.  However, the
results of such simulation also depend significantly on the availability
of precise 3D models of the geography/terrain as well as the building
structure in the coverage area.

In absence of such simulation software and/or precise models, there are
several models that can help, depending on the general terrain:

== Common Path Loss Models

[[path-loss-models]]
.List of common path loss models
[options="header",cols="20%,20%,20%,40%"]
|===
|Type|Sub-Type|Bands|Name
|Terrain|-|850, 900, 1800, 1900|ITU terrain model
|Rural|Foliage|850, 900, 1800, 1900|One woodland terminal model
|City|Urban|850, 900|Okumura-Hata Model for Urban Areas
|City|Suburban|850, 900|Okumura-Hata Model for Suburban Areas
|City|Open|850, 900|Okumura-Hata Model for Open Areas
|City|Urban|1800, 1900|COST-231 Hata Model
|Indoor|-|900, 1800, 1900|ITU model for indoor attenuation
|===

In <<path-loss-models>> you can see a list of commonly-used path loss
models.  They are typically quite simple equations which only require
certain parameters like the distance of transmitter and receiver as well
as the antenna height, etc.  No detailed 3D models of the terrain are
required.

== RF Power in a Wireless Link

image::link_budget.png[width="90%"]

[[rf-link-budget]]
== Link Budget

The link budget consists of the total budget of all elements in the
telecommunication system between BTS and MS (and vice-versa).

This includes

* antenna gains on both sides
* coaxial cabling between antenna and receiver/transmitter
* losses in duplexers, splitters, connectors, etc
* gain of any amplifiers (PA, LNA)
* path loss of the radio link between the two antennas

== Simplified Link Budget Equation

The simplified link budget equations looks like this:

 Rx Power (dBm) = Tx Power (dBm) + Gains (dB) − Losses (dB)

Gains is the sum of all gains, including

* Gain of the transmitter antenna
* Gain of the receiver antenna
* Gain of any PA (transmitter) or LNA (receiver)

Losses is the sum of all losses, including

* Loss of any cabling and/or connectors on either side
* Loss of any passive components like duplexers/splitters on either side
* Path Loss of the radio link

== Link Budget Equation vs. Path Loss

* Using the Link Budget equation and resolving it for the path loss will
  give you an idea of how much path loss on the radio link you can afford
   while still having a reliable radio link.

* Resolving the path loss into a physical distance based on your path
  loss model will then give you an idea about the coverage area that
  you can expect.

NOTE:: The Rx Power substituted in the Link budget equation is
determined by the receiver sensitivity.  It is customary to add some
some safety margin to cover for fading.

== RF Link

image::ap_to_client.png[width="90%"]


== Uplink Link Budget

[graphviz]
----
digraph G {
	rankdir = LR;
	MS -> MSAnt -> Path -> BTSAnt -> Cabling -> Duplexer -> Cable -> BTS;
	MSAnt [label="MS Antenna"];
	BTSAnt [label="BTS Antenna"];
}
----

The transmit power of a MS depends on various factors, such as the MS
Power Class, the frequency band and the modulation scheme used.

[options="header"]
.Typical MS transmit power levels
|===
|Power Class|Band|Modulation|Power
|4|850 / 900|GMSK|33 dBm (2 W)
|1|1800 / 1900|GMSK|30 dBm (1 W)
|E2|850 / 900|8PSK|27 dBm (0.5 W)
|E2|1800 / 1900|8PSK|26 dBm (0.4 W)
|===

The minimum reference sensitivity level of a normal GSM BTS is specified
in 3GPP TS 05.05 and required to be at least -104 dBm.  Most modern BTSs
outperform this significantly.

FIXME: Example calculation (spreadsheet screenshot?)

== Downlink Link Budget

[graphviz]
----
digraph G {
	rankdir = LR;
	BTS -> Cable -> Duplexer -> Cabling -> BTSAnt -> Path -> MSAnt -> MS;
	MSAnt [label="MS Antenna"];
	BTSAnt [label="BTS Antenna"];
}
----

The transmit power of the BTS depends on your BTS model and any possible
external power amplifiers used.

The minimum reference sensitivity level of a GSM MS is specified in 3GPP
TS 05.05 and can typically be assumed to be about -102 dB.

FIXME: Example calculation (spreadsheet screenshot?)


== Optimization of the Link Budget

If the coverage area determined by the above procedure is insufficient,
you can try to change some of the parameters, such as

* increasing transmit power by adding a bigger PA
* increasing antenna gain by using a higher gain antenna
* reducing cable losses by using better / shorter coaxial cables
* increasing the height of your antenna

include::rf.adoc[]

== The End

Questions?
