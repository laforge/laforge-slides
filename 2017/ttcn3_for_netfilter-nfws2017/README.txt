From TTCN3_P.pdf: 17, 18, 20, 21, 22, 24, 25, 26, 27, 36

types: 37, 39, 40, 41, 46, 61, 62, 65, 124

templates: 154, 155, 159, 163, 167, 192, 

sequential / alt: 297, 208, 209, 210, 211, 215, 216


* lists of titan repositories @ eclipse
https://github.com/eclipse?utf8=%E2%9C%93&q=titan&type=&language=
* modules for DNS, SCTP, SIP, IKEv2, HTTP, DHCP, IP, SMPP, SNMP, HTTP,
  TCP, UDP, RTP, XMPP, DHCPv6, ICMP, RTSP, SMTP, ICMPv6, ...

* ETSI TTCN-3 Test Suites:
  http://www.ttcn-3.org/index.php/downloads/publicts/publicts-etsi
  including IPv6, SIP, IMS, DIAMETER, WiMax, GTPv2-C, ...

