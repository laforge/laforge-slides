Osmocom BTS Hardware Support
============================
:author:	Harald Welte <hwelte@sysmocom.de>
:copyright:	2017 by Harald Welte (License: CC-BY-SA)
:backend:	slidy
:max-width:	45em


== Overview

[graphviz]
----
include::osmocom_ran_options.dot[]
----


== Clasic E1/T1 based BTS

Supported Vendors/Dialects

* Siemens (only BS-11 tested)
* Ericsson RBS2xxx (RBS2307, 2308, 2111 tested)
* Nokia InSite, MetroSite

[options="header"]
|===
|Pro|Con
|available inexpensively from decomissioned sites | appears a bit antiquated
|up to 12 TRX                                     | not many people familiar with E1/T1 anymore
|high RF output power                             | no convenient testing/debugging of E1/T1 issues
|rugged mechanical build, high MTTF           | high power consumption
|                                             | older models no EGPRS, no AMR
|===

== Classic E1/T1 based BTS

image::images/har2009-bs11_at_tree.jpg[width="80%"]

== Clasic E1/T1 based BTS

image::images/har2009-bs11_antennas2.small.jpg[width="50%",role="gimmick_right"]

* This is how it all started
* E1 based BTS (Siemens BS-11)
* HAR 2009 - Dutch Hacker Camp
* Antennas mounted with duct tape to tree
* E1 back-haul over CAT5 to OpenBSC running in tent

== Ericsson RBS 2308

image::images/rbs2308.jpg[width="60%",role="gimmick_right"]

* Many RBS2000 models
* All very similar on protocol
* Not all models tested
* Good results with RBS2308 + RBS2111

== ip.access nanoBTS

image::images/nanoBTS_small.png[width="40%",role="gimmick_right"]

* PoE-enabled single-TRX 200mW indoor BTS
* GPRS/GSM only models and EGPRS-enabled models
* available in band-specific versions for all four bands
* proprietary BTS and PCU inside
** lots of PCU crashes reported by users :(
** no way for us to fix it
* No fully dynamic channels (TCH/F + TCH/H + PDCH)


== sysmoBTS

image::images/symoBTS_v2D_front_1024.jpg[width="35%",role="gimmick_right"]

* sysmocom builds family of GSM BTS based on OsmoBTS + OsmoPCU
* revenue from this sales used to cross-subsidize OsmoBTS development
* *osmo-bts-sysmo* uses shared-memory /dev to talk to PHY
* *osmo-pcu* uses shared-memory /dev to talk to PHY

[options="header",cols="2,1,1,1,1,1"]
|===
|Model            | RF Pwr |TRX|Outdoor|PoE | Quad-Band
|sysmoBTS 1002    |  0.2 W | 1 | No  | No   | Yes
|sysmoBTS 1002 OD |  0.2 W | 1 | Yes | Yes  | Yes
|sysmoBTS 1020    |  2.0 W | 1 | Yes | Yes  | No
|sysmoBTS 1100    | 10.0 W | 1 | Yes | No   | No
|sysmoBTS 2050    | 2x 5 W | 2 | Yes | No   | No
|sysmoBTS 2100    | 2x10 W | 2 | Yes | No   | No
|===

== Nuran LiteCell 1.5

image::images/litecell15.jpg[width="40%",role="gimmick_right"]

* 10W 2-TRX Outdoor BTS 
* *osmo-bts-litecell15* uses shared-memory /dev to talk to PHY
* *osmo-pcu* uses shared-memory /dev to talk to PHY


== Octasic OCTBTS with OCTPHY-2G

image::images/octbts_3600_photo__large.png[width="40%",role="gimmick_right"]

* not a ready-to-deploy BTS product, more a BTS development board
** no enclosure, no PA, no filters
* proprietary PHY runs in Octasic DSP
** raw Ethernet frames towards *osmo-bts-octphy*
** unix domain pcu-socket to *osmo-pcu*
* series of different board models (3000, 3500, 3600) with different
  number of DSPs, radio interfaces, ARM/x86 processor core
* two TRX per DSP possible
* not all voice codecs supported
* EGRPS integration with OsmoPCU not working yet


== EOF

End of File
