The netfilter framework in Linux 2.4

Linux 2.4 provides a sophisticated infrastructure, called netfilter, which is the basis for packet filtering, network address translation and packet mangling.

The whole firewalling implementation has been rewritten from scratch. 

Netfilter is a clean, abstract and well-defined interface to the network stack. It is easily extendable due to its modular concept.

The presentation covers the following topics:

- Netfilter concepts
	- Infrastructure provided by the network stack
	- IP tables 
- Packet filtering
	- The builtin matches and targets
	- Stateful Firewalling (Connection Tracking)
- Network address translation 
	- Source NAT, destination NAT, Masquerading, transparent proxying
- Packet mangling
- Queuing packets to userspace
- Current work / Future / Netfilter-related projects

Harald Welte <laforge@gnumonks.org>
