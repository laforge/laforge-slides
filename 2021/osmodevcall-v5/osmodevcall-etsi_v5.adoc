Retronetworking: ETSI V5 interface in PSTN
==========================================
:author:	Harald Welte <laforge@osmocom.org>
:copyright:	2021 by Harald Welte (License: CC-BY-SA)
:backend:	slidy
//:max-width:	45em

== Overview

What this talk is about

* Retronetworking
* digital telephony switching of the 1990ies
* my research into ETSI V5
* writing FOSS code for the LE side to talk to real-world AN

== General Acronyms / Definitions

[horizontal]
TE:: Terminal Equipment (Phone, Modem, ...)
NT:: [ISDN] Network Termination (e.g. NTBA in Germany)
ISDN:: Integrated Services Digital Network
PSTN:: Public Switched Telephony Network
BRA:: Basic Rate (ISDN) Access (2x 64k B-Channels)
PRA:: Primary Rate (ISDN) Access (30x 64k B-Channels)
E1:: Primary Rate interface, 2048 kBps

== History of telephone exchanges

* 1878: manually switched
* 1900: electromechanically switched
* 1965: electronic switching, analog
* 1976: digital switching

== Trends in PSTN networks since ~ 1970

* more and more telephone subscribers (and hence, lines)
* capacity of switches increases as technology proceeds
* operational cost of each exchange is substantial
* trend towards centralization of exchanges
** fewer exchanges (= switches)
** more subscribers per exchange

This creates the problem of managing a vast amount of subscriber line wiring

Response: Modular architecture

== Modular Digital Telephone Exchange

* don't connect every subscriber local loop to a line card in the exchange
* have remote _access multiplexers_ aggregating subscriber lines
* reduces amount of cables terminating at a single geographical location
* helps reducing the subscriber loop length
* helps increasing capacity of the exchange by attaching more remote subscribers without the need of more
  exchanges

Vendors came up with their respective proprietary access multiplexers.

ETSI V5 is the attempt to replace vendor proprietary access multiplexers with an interoperable,
standards-based system.

== The V5 AN / LE functional split

V5 _disaggregates_ the telephone exchange into two parts:

* *Access Network (AN)*
* *Local Exchange (LE)*

image::g964_figure14_pstn_user_port.png[width=1800,align="center"]

The E1 based V5 interface can of course be carried over higher-order PDH/SDH

== The V5.1 architecture

image::g964_figure2_architecture.png[width=1400,align="center"]

== V5 Acronyms / Definitions

[horizontal]
AN:: Access Network
LE:: Local Exchange
FE:: Functional Element
LT:: Line Termination
LC:: Line Circuit
ET:: Exchange Termination


== V5.related 1 specifications

* V5.1 interface
** ITU-T G.964
** ETSI ETS 300 324-1
** ETSI TR 150
* Q3 interface (management)
** ITU-T Q.512
** ETS ETS 300-376-y, ETS 300 378-y (Q3AN)
** ETS ETS 300-377-z, ETS 300 379-z (Q3LE)

== V5.1 Protocol Stack

== V5.1 Protocol Architecture

image::g964_figure6_protocol_arch.png[width=1400,align="center"]

== LAPV5: Data Link Layer

image::g964_figure11_dl_mgmt.png[width=1300,align="center"]


== Control Protocol

* Common Control
** Re-provisioning
** Switch-over to new variant
** Restart
* Per-Port Control
** Blocking
** Unblocking
** Activation / Deactivation (ISDN only)

== Control Protocol (PSTN)

* administrative blocking / unblocking
** UNBLOCK + ACK
** BLOCK + ACK
** Functional Elements FE201-FE205 used

== Control Protocol (ISDN)

* administrative blocking / unblocking
** UNBLOCK + ACK
** BLOCK + ACK
** Functional Elements FE101-FE106 and FE201-FE208 used
** D-CHANNEL BLOCK/UNBLOCK
* access activation
** ACTIVATE ACCESS
** DEACTIVATE ACCESS
* miscellaneous
** PERFORMANCE GRADING

== PSTN subscriber

* _Control Protocol_ handles low-level physical layer management
* _PSTN Protocol_ handles analog siganling (on-hook/off-hook, pulse dialing)

image::g964_figure14_pstn_user_port.png[width=1800,align="center"]

== PSTN protocol

* analogue line states
* timing and duration of analogue signals
* voltage/frequency of metering pulses
* ringing current
* FSMs for AN and LE side
* Messages In PSTN protocol
** ESTABLISH + ESTABLISH-ACK
** SIGNAL + SIGNAL-ACK
** DISCONNECT + DISCONNECT-COMPLETE
** STATUS + STATUS-ENQUIRY
** PROTOCOL-PARAMETER

== PSTN protocol - Originating Call

image::g964_figureB1_pstn_originating_call.png[width=1300,align="center"]

== PSTN protocol - Terminating Call

image::g964_figureB2_pstn_terminating_call.png[width=1300,align="center"]

== PSTN protocol - Example IE

image::g964_table24_pstn_steady_state.png[]

== ISDN subscriber

image::g964_figure31_isdn_model.png[width=1300,align="center"]

== ISDN subscriber

* _Control Protocol_ handles low-level physical layer management
* _Frame Relay_ forwards Q.921 (LAPD) over V5
** Actual ISDN L2 (Q.921) and L3 (Q.931) processing all in LE
** not to be confused with the Packet WAN technology Frame Relay!

As ISDN already has digital signaling (Q.921/Q.931), no need for any V5 specific ISDN protocol (like PSTN
protocol).  Signaling from user is _relayed_ to LE via V5 (and vice-versa).

== ISDN LAPD Frame Relay

image::g964_figure13_frame_relay.png[width=1300,align="center"]

== Control protocol - ISDN port activation

[mscgen]
----
include::isdn-control.msc[]
----



== V5.2: Successor of V5.1

V5.2 introduces the following major changes:

* up to 16xE1 per V5.2 interface (V5.1: 1xE1)
* support for ISDN PRA (Primary Rate Access) subscribers
* concentration (overprovisioning of local loop vs V5 circuits)
* fail-over between links

The new features are added in a backwards compatible manner

== The V5.2 architecture

image::g965_figure2_architecture.png[width=1300,align="center"]


== V5.2 specifications

All of the V5.1 related specs plus the following new ones:

* ITU-T G.965
* ETSI ETS 300 347-1


== V5.2 Protocol Stack

* LAPV5 Data Link Layerlike in V5.1
* L3 protocols like in V5.1 (PSTN, control)
* additional, new L3 protocols on top of LAPV5
** link control protocol
** protection protocol
** BCC (Bearer Channel Connection)



== V5.2 Protocol Architecture

image::g965_figure6_protocol_architecture.png[width=1400,align="center"]

== V5.2 BCC

* dynamically allocate B-channels on V5 to B-channels of subscribers
* messages
** ALLOCATION + COMPLETE / REJECT
** DE-ALLOCATION + COMPLETE / REJECT
** AUDIT + COMPLETE
** AN FAULT + ACK
** PROTOCOL ERROR

== V5.2 link control protocol

* identification of different links in V5.2 interface
* blocking and coordinated unblocking of V5.2 links

== V5.2 protection protocol

* fail-over between different E1-links on V5.2 interface
* monitoring of flag, data link layer establishment
* mapping of logical / physical channels

== Q3 interface (management)

* Full Complex ISO/OSI protocol Stack

image:q812_q3_upper_layer_stack.png[]

== Nokia EKSOS

* Nokia EKSOS N20: 512 bearer channels, 17-slot rack
* important modules
** Node Control Unit
** PSTN unit (32xPSTN)
** ISDN BRA U-interface unit (16xU)
** TR2M4 unit (4xE1)
* NodeManager software

== Nokia EKSOS N20

image:nokia_eksos_n20_photo.jpg[width=1300,align="center"]

== Nokia EKSOS N20 Node Control Unit

image:nokia_eksos_ncu.png[width=1000,align="center"]

== Nokia EKSOS PSTN + ISDN Unit

image:nokia_eksos_pstn_isdn.png[width=850,align="center"]



== Fujitsu/DeTeWe ALIAN

* Most common modules in FSX2000
** MPU-H (Main Processor Unit)
** HOS (Housekeeping, subscriber line test)
** NMI (Network Management Interface)
** TSA (Trunk to tributary cross-connect)
** V5MC (V5.2 termination message control)
** BRA12T (12x ISDN U)
** SLM15G (15x POTS line)
** E1B21 (21x E1 trunk or tributary)
** E1BU4 (4x E1 trunk or tributary)
* FLEXR management software

== Fujitsu/DeTeWe ALIAN

image:fujitsu_dtw_alian_photo.jpg[width=1300,align="center"]

== Keymile MileGate

Keymile MileGate is a combined POTS/ISDN/DSL subscriber access platform

* Most common V5 related modules
** PCOM1/2 (V5 AN)
** TUXA1 (12x POTS)
** SUIT1 (16x ISDN PRA U)
** SUPx4 (48x POTS)

== Siemens FastLink ONU

* Most common V5 related modules 
** IUL84 (8x ISDN BRA)
** SUB102 (10x POTS)
** SUB162 (16x POTS)
** SUB322 (32x POTS)

== The V5 master plan

* collect decommissioned V5 access multiplexers
* create some kind of working setup out of them
** similar to the PBX-based _Osmocom retronetworking_ setup at last xxC3
* deploy that at future hacker events
** allow retronetworking interested people to get analog and Uk0 subscriber lines

== Required for V5 master plan

* V5 Access Multiplexers (almost done)
* Equipment documentation (done)
* Configuration/Mgmt Software (done)
* E1 interface (done, icE1usb or DAHDI cards)
* V5 protocol stack for LE side (in progress)
** not supported in FreeSwitch, Asterisk or even yate
** at least a wireshark disector exists (yay!)

== Towards a FOSS implementation for V5 LE role

* started an implementation of the LE side of V5
* based around libosmocore and its TLV decoder + FSM
* current status (WIP, about 3k lines of code)
** core data structures
** Control protocol encoding + decoding
** Common Control FSM
** Port control FSM
** V5.2 Link Control Protocol FSM

Waiting for actual V5 AN equipment to start testing it.

== V5 LE PBX integration

* how to integrate with PBX?
** we definitely want to reuse Q.921 + Q.931 of these
** B-channels could be accessed directly via DAHDI
** D-channels would need to go through _Frame Relay_ de-concentration first

[graphviz]
----
digraph G {
  rankdir=LR;
  subgraph cluster_AN {
    subgraph cluster_subA {
      label="Subscriber A";
      TE_A;
      NT_A;
      TE_A -> NT_A [label="S0"];
    }
    subgraph cluster_subB {
      label="Subscriber B";
      TE_B;
      NT_B;
      TE_B -> NT_B [label="S0"];
    }
    subgraph cluster_subC {
      label="Subscriber C";
      TE_C;
    }
    subgraph cluster_subD {
      label="Subscriber D";
      TE_D;
    }
    AN;
    NT_A -> AN [label="U"];
    NT_B -> AN [label="U"];
    TE_C -> AN [label="POTS"];
    TE_D -> AN [label="POTS"];
  }

  subgraph cluster_LE {
    label="LE";
    DAHDI [shape="record",label="<DAHDI> DAHDI | <TS1> TS1 | <TS2> TS2 | <TS15> TS15 | <TSn> TSn"];
    PBX [label="PBX\nQ.921\nQ.931"];
    V5D [shape="diamond"];

    DAHDI:TS15 -> V5D [label="V5"];
    DAHDI:TS1 -> PBX [label="B-Channel"];
    DAHDI:TS2 -> PBX [label="B-Channel"];
    DAHDI:TSn -> PBX [label="B-Channel"];

    V5D -> PBX [label="LAPDm Sub A"];
    V5D -> PBX [label="LAPDm Sub B"];
    V5D -> PBX [label="PSTN Ctrl Sub C"];
    V5D -> PBX [label="PSTN Ctrl Sub D"];
  }
  AN -> DAHDI:DAHDI [label="E1"];
}
----


== EOF

End of File
