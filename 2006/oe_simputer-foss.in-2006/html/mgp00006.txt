OpenEmbedded
 Introduction to OE
 
 What is OpenEmbedded (OE)
  Not a distribution, but distribution building framework
  Not a software program
  Consists of thousands of rules
      Rules for definition of a machine type (78)
      Rules for definition of a distribution (32)
      Rules for individual packages (4095)
  Plus a program to interpret those rules
      bitbake
  "One system to rule them all" 
 
