#include <linux/module.h>
#include <linux/config.h>
#include <linux/skbuff.h>
#include <linux/ip.h>

#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Harald Welte <laforge@netfilter.org>");
MODULE_DESCRIPTION("5CLT workshop module");

static unsigned int
workshop_fn(unsigned int hooknum,
	    struct sk_buff **pskb,
	    const struct net_device *in,
	    const struct net_device *out,
	    int (*okfn)(struct sk_buff *))
{
	struct iphdr *iph = (*pskb)->nh.iph;
	/* do whatever we want to do */

	printk(KERN_NOTICE "packet from %u.%u.%u.%u received\n",
		NIPQUAD(iph->saddr));

	return NF_ACCEPT;
}

static struct nf_hook_ops workshop_ops = {
	.list = { .prev = NULL, .next = NULL },
	.hook = &workshop_fn,
	.pf = PF_INET,
	.hooknum = NF_IP_PRE_ROUTING,
	.priority = NF_IP_PRI_LAST-1
};

static int __init init(void)
{
	int ret = 0;

	ret = nf_register_hook(&workshop_ops);
	if (ret < 0) {
		printk(KERN_ERR "something went wrong while registering\n");
		return ret;
	}

	printk(KERN_DEBUG "workshop netfilter module successfully loaded\n");
	return ret;
}

static void __exit fini(void)
{
	nf_unregister_hook(&workshop_ops);
}

module_init(init);
module_exit(fini);
