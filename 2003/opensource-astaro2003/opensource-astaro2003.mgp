%include "default.mgp"
%default 1 bgrad
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
%nodefault
%back "blue"

%center
%size 7


What is Open Source / Free Software ?



%center
%size 4
by

Harald Welte <hwelte@astaro.com>
Harald Welte <laforge@gnumonks.org> 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
What is Free / Open Source Software (FOSS)
Contents


	The traditional (proprietary) software model
	The Free / Open Source software model
	Important Free / Open Source software licenses
	Difference Free Software / Open Source
	History of Free / Open Source software
	Who is behind FOSS?
	Development Process
	Thanks



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
What is Free / Open Source Software (FOSS)
The traditional (proprietary) software model

	traditional software model
		product-oriented
		vendor finances development of software
		business model of software based on secret source code
		same copy of software object code is sold under a very restrictive license
		license fees refinance cost of development
		enforcement of restrictive license guarantees revenue
		advantages
			proven business model
		disadvantage
			vendor has to develop everything on his own or buy licenses of 3rd party software
			less flexibility for the customer
			does the customer trust the 'black box' you are selling?
			if vendor goes out of business, no bugfixes/updates

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
What is Free / Open Source Software (FOSS)
The free / open source software model

	Open Source / Free Software model
		service based
		individual parties contribute code parts
		software is distributed for free 
		software source code is distributed under very permissive license
		service / support / customization refinance development
		advantages
			vast amount of available FOSS can be used as foundation for own products
			source code is available for peer review
			bug fixes for free, people just send you patches
			new features impelemented by your users!
		disadvantage
			business model has yet to prove scalability

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
What is Free / Open Source Software (FOSS)
Difference Free Software / Open Source

	difference free software / open source 
		free software
			term 'free software' (free as in freedom, not beer) introduced by Richard Stallman / FSF 1984.
			focus on political/ethical/philosophical freedom
		open source 
			term 'open source' software (OSS) introduced by OSI in 1997
			focus on technological advantage by means of source review
		most FOSS licenses match both definitions, OSS less restrictive
		FOSS is _not_ to be mistaken as freeware / shareware!

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
What is Free / Open Source Software (FOSS)
Important FOSS licenses


	important free / open source license
		BSD (Berkeley Systems Derivate) style license
			permits any use of the sourcecode as long as copyright notice remains
		GPL (GNU General Public License)
			source for resulting binary has to be provided
			ensures that derivates of free software are still free
		LGPL (GNU Lesser General Public License)
			permits linking with non-gpl code (mainly used for libraries)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
What is Free / Open Source Software (FOSS)
History of Free / Open Source Software

	history of free / open source software
		initially software always for free in source (e.g. IBM S/360)
		as hardware gets less expensive, companies start to license software for money
		some people (Stallman, et. al.) didn't want to give up the freedom they're used to.
		1983: GNU project is founded, goal: Implementation of a free UNIX-like operating system
		1984: Free Software Foundation is established as non-for-profit legal entity behind the GNU project
		1991: Linus Torvalds releases the first version of the Linux Kernel under the GNU GPL license.  Together with the other parts from the GNU project and others, a 100% free operating system is available
		1994-1999: FOSS is increasingly recognized as reliable, stable alternative to proprietary software, esp. in the server + networking market
		2000-2003: FOSS is increasingly considered as an alternative on the desktop (see recent decision by Munich city administration, respective laws in latin america, ...)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
What is Free / Open Source Software (FOSS)
Who is behind FOSS?

	individuals
		computer enthusiasts motivated by
			fight: david <-> goliath
			ability to show how poorly implemented most proprietary software is
			ability to gain more experience / better reputation
		experienced end-users
		independent consultants
			looking for a solution to a particular problem and already have 95% by using existing FOSS
	organizations
		commercial entities who recognize the value of FOSS
			contributions to existing projects
			start of new projects 
			contracting consultants and FOSS companies for implementation of missing features
		mixed FOSS / proprietary companies (like Astaro)
			use FOSS as foundation for their proprietary solutions
			have a vital need for a reliable and up-to-date foundation, thus contribute back to and/or fund FOSS
		academic institutions (e.g. exim, cyrus)
			are traditionally involved in the exchange of research results.  Why treat software differently?

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
What is Free / Open Source Software (FOSS)
Development Process

	development process, communication
		everybody who agrees to the license can contribute code
		project is usually started by a single developer or a small group
	different actors in development process
		maintainer: official person to maintain the code, responsible
		core team: small group of leaders behind the project
		developers: people who write code on a regular basis
		contibutors: people who contribute a single feature or a bug fix from time to time
		users: people who use the software, often organized on mailinglists, newsgroups, user groups, .. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
What is Free / Open Source Software (FOSS)
Development Process

		main communication medium are mailinglists
		every developer can be contacted directly via email
		leaders/managers are people with the best technical skills, unlike the 'commercial world' where you need certain diploma, connections, ...
		communication is random.  no manager <-> manager talk about technical stuff they don't understand

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
What is Free / Open Source Software (FOSS)
Thanks

	in the name of the netfilter/iptables project, thanks to Astaro for funding
		particular tasks on my schedule
		equipment (dual Opteron below my desk)
		my travel expenses to many FOSS conferences
		the netfilter developer workshop in August 2003 (Budapest, HU)


