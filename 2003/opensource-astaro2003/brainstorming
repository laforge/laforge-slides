What is open source? How does it work? 
Who writes code for nothing and why?

- traditional software model
	- product-oriented
	- company finances development of software
	- same copy of software object code is sold under a very restrictive
	  license
	- license fees refinance cost of development
	- enforcement of restrictive license guarantees revenue
	- advantages
		- proven business model
	- disadvantage
		- have to develop everything on your own or buy licenses of 3rd
		  party software
		- less flexibility for the customer
		- does the customer trust the 'black box' you are selling?
		- if vendor goes out of business, no bugfixes/updates

- open source model
	- service based
	- individual parties contribute code parts
	- software is distributed for free 
	- software is distributed under very permissive license
	- service / support / customization refinance development
	- advantages
		- vast amount of available FOSS can be used as foundation for
		  own products
		- source code is available for peer review
		- bug fixes for free, people just send you patches
		- new features impelemented by your users!
	- disadvantage
		- business model has yet to be proven to work
	
- important open source license
	- BSD style license
		- permits any use of the sourcecode as long as copyright notice
		  remains
	- GPL (GNU General Public License)
		- source for resulting binary has to be provided
		- ensures that derivates of free software are still free
	- LGPL (GNU Lesser General Public License)
		- permits linking with non-gpl code (mainly used for libraries)

- difference free software / open source 
	- term 'free software' (free as in freedom, not beer) introduced by
	  Stallman / FSF 1984.
	  	- focus on political/ethical/philosophical freedom
	- open source software (OSS) introduced by OSI in 1997
		- focus on technological advantage by means of source review
	- most FOSS licenses match both definitions, OSS less restrictive

- history of FOSS
	- initially software always for free in source (e.g. IBM S/360)
	- as hardware gets less expensive, companies start to license
	  software for money
	- some people (Stallman, et. al.) didn't want to give up the freedom
	  they're used to.
	- 1983: GNU project is founded, goal: Implementation of a free UNIX-like
	  operating system
	- 1984: Free Software Foundation is established as non-for-profit legal
	  entity behind the GNU project
	- 1991: Linus Torvalds releases the first version of the Linux Kernel
	  under the GNU GPL license.  Together with the other parts from the
	  GNU project and others, a 100% free operating system is available
	- 1994-2000: Free Software is increasingly recognized as reliable,
	  stable alternative to proprietary software

- Who is behind FOSS?
	- in the beginning mostly computer enthusiasts with academic background
		- motivation through
			- fight: david <-> goliath
			- to show how bad most proprietary software is
			- to make the internet a better place
			- to work together with _very_ good programmers
			- to gain more experience / better reputation
	- more and more commercial entities recognize the value of FOSS
		- contributions to existing projects
		- start of new projects
		- contracting consultants and FOSS companies for implementation
		  of missing features
	- experienced end-users
	- independent consultants
	- academic institutions (e.g. exim, cyrus)
	- mixed FOSS / proprietary companies (like Astaro)
		- use FOSS as foundation for their proprietary solutions
		- have a vital need for a reliable and up-to-date foundation,
		  thus contribute back to and/or fund FOSS

- development process, communication
	- everybody who agrees to the license can contribute code
	- project is usually started by a single developer or a small group
	- different actors:
		- maintainer: official person to maintain the code, responsible
		- core team: small group of leaders behind the project
		- developers: people who write code on a regular basis
		- contibutors: people who contribute a single feature or a bug
		  fix from time to time
		- users: people who use the software, often organized on
		  mailinglists, newsgroups, user groups, .. 
	- main communication medium are mailinglists
	- every developer can be contacted directly via email
	- leaders/managers are people with the best technical skills, unlike the 'commercial world' where you need certain diploma, connections, ...
	- communication is random.  no manager <-> manager talk about technical
	  stuff they don't understand
