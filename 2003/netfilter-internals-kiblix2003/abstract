Linux 2.4.x netfilter/iptables firewalling internals

  The Linux 2.4.x kernel series has introduced a totally new kernel firewalling subsystem.  It is much more than a plain successor of ipfwadm or ipchains.

  The netfilter/iptables project has a very modular design and it's
sub-projects can be split in several parts: netfilter, iptables, connection
tracking, NAT and packet mangling.

  While most users will already have learned how to use the basic functions
of netfilter/iptables in order to convert their old ipchains firewalls to
iptables, there's more advanced but less used functionality in
netfilter/iptables.

  The presentation covers the design principles behind the netfilter/iptables
implementation.  This knowledge enables us to understand how the individual
parts of netfilter/iptables fit together, and for which potential applications
this is useful.

Topics covered:

- overview about the internal netfilter/iptables architecture
	- the netfilter hooks inside the network protocol stacks
	- packet selection with IP tables
	- how is connection tracking and NAT integrated into the framework
- the connection tracking system
	- how good does it track the TCP state?
	- how does it track ICMP and UDP state at all?
	- layer 4 protocol helpers (GRE, ...)
	- application helpers (ftp, irc, h323, ...)
	- restrictions/limitations
- the NAT system
	- how does it interact with connection tracking?
	- layer 4 protocol helpers
	- application helpers (ftp, irc, ...)
- misc
	- how far is IPv6 firewalling with ip6tables?
	- advances in failover/HA of stateful firewalls
	- ivisible firewalls with iptables on a bridge
	- userspace packet queueing with QUEUE
	- userspace packet logging with ULOG

Requirements:  
- knowledge about the TCP/IP protocol family
- knowledge about general firewalling and packet filtering concepts
- prior experience with linux packet filters

Audience:
- firewall administrators
- network developers
