#include <linux/module.h>
#include <linux/sk_buff.h>
#include <linux/netfilter_ipv4/ip_tables.h>
#include <linux/netfilter_ipv4/ipt_workshop.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Harald Welte <laforge@netfilter.org>");
MODULE_DESCRIPTION("OLS2003 workshop iptables module");

static int ws_match(const struct sk_buff *skb, const struct net_device *in,
		    const struct net_device *out, const void *matchinfo,
		    int offset, const void *hdr, u_int16_t datalen,
		    int *hotdrop)
{
	const struct ipt_ws_info *info = matchinfo;
	const struct iphdr *iph = skb->nh.iph;

	if (iph->ttl == info->ttl)
		return 1;

	return 0;
}

static int ws_checkentry(const char *tablename, const struct ipt_ip *ip,
			 void *matchinfo, unsigned int matchsize,
			 unsigned int hook_mask)
{
	if (matchsize != IPT_ALIGN(sizeof(struct ipt_ws_info)))
		return 0;

	return 1;
}

static struct ipt_match ws_match = {
	.list = { .prev = NULL, .next = NULL },
	.name = "workshop",
	.match = &ws_match,
	.checkentry = &ws_checkentry,
	.destroy = NULL,
	.me = THIS_MODULE
};

static int __init init(void)
{
	return ipt_register_match(&ws_match);
}

static void __exit fini(void)
{
	ipt_unregister_match(&ws_match);
}

module_init(init);
module_exit(fini);
