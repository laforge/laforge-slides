#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <linux/netfilter_ipv4/ip_tables.h>
#include <linux/netfilter_ipv4/ipt_workshop.h>

static void help(void)
{
	printf(
"workshop match v%s options:\n"
"  --ttl	TTL value\n"
, IPTABLES_VERSION);
}

static void init(struct ipt_entry_match *m, unsigned int *nfcache)
{
	/* caching not implemented yet */
	*nfcache |= NFC_UNKNOWN;
}

static int parse(int c, char **argv, int invert, unsigned int *flags,
		 const struct ipt_entry *entry, unsigned int *nfcache,
		 struct ipt_entry_match **match)
{
	struct ipt_ws_info *info = (struct ipt_ws_info *) (*match)->data;

	check_inverse(optarg, &invert, &optind, 0);

	if (invert)
		exit_error(PARAMETER_PROBLEM, "invert not supported");

	if (*flags)
		exit_error(PARAMETER_PROBLEM, 
			   "workshop: can't specify parameter twice");

	if (!optarg)
		exit_error(PARAMETER_PROBLEM,
			   "workshop: you must specify a value");

	switch (c) {
		case 'z':
			info->ttl = atoi(optarg);
			/* FIXME: range 0-255 */
			*flags = 1;
			break;
		default:
			return 0;
	}

	return 1;
}

static void final_check(unsigned int flags)
{
	if (!flags)
		exit_error(PARAMETER_PROBLEM,
			   "workshop match: you must specify ttl");
}

static void print(const struct ipt_ip *ip,
		  const struct ipt_entry_match *match,
		  int numeric)
{
	const struct ipt_ws_info *info = (struct ipt_ws_info *) match->data;

	printf("workshop match TTL=%u ", info->ttl);

}

static void save(const struct ipt_ip *ip,
		 const struct ipt_entry_match *match)
{
	const struct ipt_ws_info *info = (struct ipt_ws_info *) match->data;

	printf("--ttl %u ", info->ttl);
}

static struct option opts[] = {
	{ "ttl", 1, 0, 'z' },
	{ 0 }
};

static struct iptables_match ws = {
	.next = NULL,
	.name = "workshop",
	.version = IPTABLES_VERSION,
	.size = IPT_ALIGN(sizeof(struct ipt_ws_info)),
	.userspacesize = IPT_ALIGN(sizeof(struct ipt_ws_info)),
	.help = &help,
	.init = &init,
	.parse = &parse,
	.final_check = &final_check,
	.print = &print,
	.save = &save,
	.extra_opts = opts
};

void _init(void)
{
	register_match(&ws);
}
