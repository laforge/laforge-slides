How to interact with the Free Software Community
Practical Rules

2. Interfaces
If there is a standard interface, use it
Don't invent new interfaces, try to extend existing ones
If there is an existing interface in a later (e.g. development) release upstream, backport that interface
Don't be afraid to touch API's if they're inefficient
Remember, you have the source and _can_ change them

