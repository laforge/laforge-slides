How to (not) use GPL Software
Dual Licensing

The copyright holder (often the original author) can provide alternative licensing
Some projects do this as a business model (reiserfs, MySQL)
In some projects it's impossible due to the extremely distributed copyright (e.g. Linux kernel)
However, in smaller projects it never hurts to ask whether there would be interest in providing an alternative (non-copyleft) licensing

