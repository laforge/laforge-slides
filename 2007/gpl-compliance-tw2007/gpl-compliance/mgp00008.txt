How to (not) use GPL Software
Derivative Works

What is a derivative work?
Not dependent on any particular kind of technology (static/dynamic linking, dlopen, whatever)
Even while the modification can itself be a copyrightable work, the combination with GPL-licensed code is subject to GPL.
As soon as code is written for a specific non-standard API (such as the iptables plugin API), there is significant indication for a derivative work
This position has been successfully enforced out-of-court with two Vendors so far (iptables modules/plugins).

