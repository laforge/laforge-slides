Year 2017 Osmocom retrospective
===============================
:author:	Harald Welte <laforge@gnumonks.org>
:copyright:	2018 by Harald Welte (License: CC-BY-SA)
:backend:	slidy
:max-width:	45em

== 2017 - a year of change

Osmocom CNI (Cellular Network Infrastructure) has changed a lot:

* software changes
* team / developer changes
* sysmocom company focus changes

== 2017 - CNI Software changes

* OsmoBSC migration from SCCPlite to 3GPP AoIP
* OsmoMGW as integral part of both BSC and MSC
* NITB split into separate MSC, HLR, BSC
* 3G (IuCS, IuPS) goes mainline

== 2016/2017/2018 - Team changes

* Q1 2016: Jacob Erlbeck leaves sysmocom
** unfortunately a complete loss to Osmocom, particularly OsmoPCU
* 2017: Holger Freyther leaves sysmocom
** unfortunately also shift of focus away from Osmocom :(
** immense loss to the project in terms of skill and capacity
* Q1 2018: Max Suraev leaves sysmocom
** another loss of lots of Osmocom knowledge

== sysmocom changes

* we used to have to do lots of non-Osmocom work to cross-subsidize Osmocom
** big distraction of resources in 2014/2015, now gone
* we used to cross-subsidize Osmocom development by hardware sales
** not happening as much anymore
* we now work almost 100% on Osmocom
** R&D projects, support contracts and grants

== split NITB aftermath (the good parts)

* biggest architectural change since we started in 2008
* lots of good reasons and design improvements
** finite state machines with proper timeouts / clean-up
** proper 3GPP AoIP with interoperability tesing
** no synchronous HLR database access
** HLR access from OsmoMSC and OsmoSGSN
** 2G/3G authentication over GERAN and UTRAN

== split NITB aftermath (the bad parts)

[role="incremental"]
* never-ending list of breakage
** actual regressions of things that used to work before
** things that were _known omissions_ during the restructuring
* some commercial users stuck with SCCPlite and thus old @osmo-bsc-sccplite@
** almost none of the new features or bug fixes there
** no automatic testing
** back-ports time-consuming

== split NITB aftermath (lessons learned)

* overall complexity of Osomcoom cellular is quite stunning now
* absence of proper functional testing has caused massive fall-out
* the split architecture allows for betteer testing of smaller parts of the system
* my personal main focus of the last 5+ months:
[role="incremental"]
** testing, testing, testing, testing
** testing, testing, testing, testing
** some more testing
** even more testing


== Osmocom CNI testing (1/2)

[role="incremental"]
* unit test (autotest, like we always had)
** test individual functions / APIs of libraries / programs
** executed during "make check" and hence before any patch can get merged
* automatized functional tests in TTCN-3
** test _external_ visible behavior on interfaces such as Abis, A, GSUP, GTP, MNCC, PCUIF, CTRL, VTY, ...
** executed nightly by Jenkins (could be more frequently)

== Osmocom CNI testing (2/2)

[role="incremental"]
* osmo-gsm-tester
** tests entire Osmoocom network with BTS/BSC/MSC/HLR/PCU/SGSN/GGSN/...
** uses real BTS + MS hardware (over coaxial cable)
** automatic execution multiple times per day
* interop tests
** against NG40 RAN + CN simulator from NG4% (A / Gb / Iu level)
** not fully automatized yet


== Osmocom project health (CNI)

[role="incremental"]
* lots of funded developments, *but*
** primarily _enterprise features_ required by professional users
* dominance of sysmocom is problematic
** sustainable FOSS has no single point of failure!
* we need more contributions from third parties
** particularly those that benefit commercially from Osmocom

== Osmocom project health (other projects)

[role="incremental"]
* OsmocomTETRA dead since 2012, occasional small fixes
* No OsmocomBB ports to other PHY/chip yet
* OsmocomDECT completely dead
* Erlang core network projects dead
* Smalltalk projects dead (AFAICT)
* SIMtrace dead for years, about to be resurrected

== Osmocom project health (other projects)

[role="incremental"]
* gr-osmosdr very low commit ratio
* rtl-sdr no commits in 2015-2017
* but it's not that bad... (see next slide)

== Osmocom project health (other projects)

[role="incremental"]
* gr-gsm, fake_trx and trxcon a welcome change in OsmocomBB
* osmocom-analog (jolly to the rescue)
* osmo-fl2k (soon! now! this year!)

== Osmocom status (CNI)

[role="incremental"]
* CS RAN (BTS, BSC) is quite strong/complete these days
** ready to be used with 3rd party CN
* PS RAN (PCU) suffers from lack of attention
** lack of automatic testsuite with decent coverage
** lack of uplink multi-slot any many EGPRS features
* CS CN (MSC, HLR) 
** in halthy state, but lack of TCAP/MAP interface limits us to non-roaming networks
* PS CN (SGSN, GGSN) 
** in good health, now with IPv6 supoprt and kernel GTP acceleration

== Osmocom outlook (CNI)

* 2G still in demand by lots of use cases (rural, maritime, ...)
** if we had TCAP/MAP interface, many more deployments possible
* 3G has some users but lack of FOSS RNC limits us to femtocells
* 4G is deployed in parallel to 2G in many scenarios
** Osmocom 2G stack needs 2G-4G integration (SGs, DIAMETER)
** Osmocom needs to contribute  to FOSS 4G projects (nextepc, srsLTE)
* irony: Now that it's possible to do properly funded Osmocom development, we have less people involved than in the early days :(

If we don't manage to focus on 4G soon, interest in 2G will diminish soon

== Osmocom outlook (other projects)

[role="incremental"]
* activity of original/traditional Osmocom developers decreased
* without attracting more developers, a lot of projects will remain dormant and/or never realize their potential
* I'd love to work on TETRA, DMR or other mobile communications technology
** but lack of developers and contributors even for 2G makes me stuck in 2G CNI land :(

== Personal Request

[role="incremental"]
* Osmocom needs you!
* we've lost too many friends already
* please don't leave Osmocom; please don't leave me

== Further Reading

See my detailed 2017 review:

* http://osmocom.org/news/84

== EOF

End of File
