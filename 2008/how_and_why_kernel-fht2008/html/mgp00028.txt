How and why to work with the Linux kernel community
How to submit your code mainline?


The FOSS code quality requirements are _extremely_ high
It's not a surprise that Linux is generally considered much more stable than competitors
Code needs to be maintainable
Linux supports old hardware ages beyond their EOL
Thin of MCA, VLB, Decnet, IPX networking, ...
So unless you respect the development culture, your code is likely to get rejected!
Post your driver at the respective mailing lists
Release early, release often
Don't hesitate to ask for feedback and suggestions if you are not 100% sure what is the right way to implement a certain feature

