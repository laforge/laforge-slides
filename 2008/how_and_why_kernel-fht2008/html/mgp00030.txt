How and why to work with the Linux kernel community
Techncal differences


In the MS world, almost all interfaces are MS defined
In the Linux world, Linux is only the OS kernel
All other interfaces are specified by their respective projects
Often there are many alternatives, e.g. for graphical drivers
X.org project (X11 window server, typical desktop)
DirectFB project (popular in embedded devices like TV set-top boxes)
Qt/Embedded (popular in certain proprietary Linux-based mobile phones)
Every project has it's own culture, including but not limited to
coding style
patch submission guidelines
software license
communication methods

