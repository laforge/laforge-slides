How and why to work with the Linux kernel community
Linux and binary compatibility


Linux and binary compatibility
Drivers usually run inside the OS kernel
Linux doesn't have any stable kernel-internal ABI
Linux doesn't even have stable kernel-internal API
Only the ABI to userspace is stable/fixed

Thus, every minor Linux release can break in-kernel ABI+API
This is why binary-only drivers simply don't work!

