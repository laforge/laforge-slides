How and why to work with the Linux kernel community
Practical Rules

1. Much more communication
It's not a consumer/producer model, but cooperative!
Before you start implementation, talk to project maintainers
It's likely that someone has tried a similar thing before
It's likely that project maintainers have already an idea how to proceed with implementation
Avoid later hazzles when you want your code merged upstream

