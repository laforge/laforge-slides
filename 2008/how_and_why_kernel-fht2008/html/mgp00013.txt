How and why to work with the Linux kernel community
Contributors


Contributors
are people not part of a specific development team
usually "very active users" of a particular program

Role
find / document / fix bugs that they find themselves
contribute bug reports, documentation or code
participate in discussion on features or problems

