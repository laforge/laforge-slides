How and why to work with the Linux kernel community
Practical Rules

3. Merge your code upstream
Initially you basically have to create a fork
Development of upsteram project continues sometimes at high speed
If you keep it out of tree for too long time, conflicts arise
Submissions might get rejected in the first round
Cleanups needed, in coordination with upstream project
Code will eventually get merged
No further maintainance needed for synchronization between your contribution and the ongoing upstream development
Don't be surprised if your code won't be accepted if you didn't discuss it with maintainers upfront and they don't like your implementation

