How and why to work with the Linux kernel community
Practical Rules

6. Avoid fancy business models
If you ship the same hardware with two different drivers (half featured and full-featured), any free software will likely make full features available on that hardware.

