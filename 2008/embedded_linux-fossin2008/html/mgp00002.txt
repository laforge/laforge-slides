How to do Embedded Linux [not] right
Introduction

Who is speaking to you?
an independent Free Software developer, consultant and trainer
13 years experience using/deploying and developing for Linux on server and workstation
10 years professional experience doing Linux system + kernel level development 
strong focus on network security and embedded
expert in Free and Open Source Software (FOSS) copyright and licensing
digital board-level hardware design, esp. embedded systems
active developer and contributor to many FOSS projects
thus, a techie, who will therefore not have fancy animated slides ;)

