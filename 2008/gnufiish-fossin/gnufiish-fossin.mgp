%include "default.mgp"
%default 1 bgrad
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
%nodefault
%back "blue"

%center
%size 7

Reverse Engineering 
and 
Porting Linux
to a 
Windows Mobile PDA Phone

%center
%size 4
by

Harald Welte <laforge@gnumonks.org>


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
Introduction

Who is speaking to you?
		an independent Free Software developer, consultant and trainer
		13 years experience using/deploying and developing for Linux on server and workstation
		10 years professional experience doing Linux system + kernel level development 
		strong focus on network security and embedded
		expert in Free and Open Source Software (FOSS) copyright and licensing
		digital board-level hardware design, esp. embedded systems
		active developer and contributor to many FOSS projects
		thus, a techie, who will therefore not have fancy animated slides ;)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
Introduction


My involvement in Linux on mobile phones
	2003/2004: gpl-violations.org / Motorola A780
	2004: Started OpenEZX for A780 (now E680, A1200, E6, ...)
	06/2006-11/2007: Lead System Architect at Openmoko, Inc.
	10/2008: Started the 'gnufiish' project

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
Introduction


Linux on mobile phones
	is hardly something new
	Vendors have been doing this since 2003, e.g.
		Motorola EZX 
			(A760, A768, A780, E680, A1200, E6, ...)
		Motorola MAGX
			(ROKR2v8, ...)
		lots of unknown Chinese vendors (E28, Haier, ..)
	however, no 'really open' devices
		proprietary UI libraries
		proprietary kernel extensions
		often no full source code
		cryptographically locked down

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
Openmoko


Linux on mobile phones
	Openmoko is many things
		the hardware
			GTA01 (Neo 1973)
			GTA02 (Neo FreeRunner)
		the various UI's
			One GTK+ based)
			One is a mixture of Qtopia, GTK+ and e17
			One is FSO + e17 based
		the distribution (based on Openembedded)
		the company (Openmoko, Inc.)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
Openmoko


Why I'm not working on/for/with Openmoko hardware?

	Not true, I still contribute to Openmoko :)
	Linux kernel port is quite complete and stable
	Hardware has its limits
		GPRS-only (no EDGE, UMTS, HSDPA)
		quite big and heavy
		no option for keyboard 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
Community based projects


Linux mobile phone community ports

	The vendor ships WM or other OS, community replaces it
	xda-developers.com community
		mostly focused on HTC devices
		way too little developers fro too many devices
		hardware product cycles getting shorter / faster
		many new devices based on completely undocumented chipsets

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
Community based projects


Linux mobile phone community ports

	More smaller / fragmented projects
	Most based on the fact that somebody bought the device and started osme hacking
	Most are stuck
		either in a quite early stage (kernel boots, not many drivers)
		or advanced but hardware already end-of-life
	Conclusion:
		We need a new project with more prospect for success
		Needs to be stable and full-feature while hardware still available

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
Community based projects


Linux mobile phone community ports

	What if you want to start from scratch?
		choose hardware that is as documented as possible
		choose hardware where most peripherals have drivers
		choose hardware that has good support in mainline Linux

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
Community based projects


How to find such a Linux-friendly device?

	Look at hardware details of available devices
		Use Google to find out what hardware they use
		Use FCC database to get PCB photographs
		Look at WM firmware images (registry/...)
		At some point you buy one and take it apart

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
Linux-friendly hardware


The E-TEN glofiish device family

	various devices with different parameters
		screen full-VGA or QVGA
		EDGE-only, UMTS or HSDPA
		keyboard or no keyboard
		GPS or no GPS
		Wifi or no Wifi
	application processor is always the same (S3C2442)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
Linux-friendly hardware

I went through this process
	I found the E-TEN glofiish devices
	They are very similar to Openmoko
		Samsung S3C2442 SoC MCP with NAND+SDRAM
		TD028TTEC1 full-VGA LCM
	Other hardware parts reasonably supported/known
		Marvell 8686/libertas WiFi (SPI attached)
		SiRF GPS (UART attached)
		CSR Bluetooth (UART attached)
	Only some unknown parts
		CPLD for power management and kbd matrix
		Ericsson GSM Modem (AT commandset documented!)
		Cameras (I don't really care)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
Project gnufiish


Project 'gnufiish'
	Port Linux to the E-TEN glofiish devices
	Initially to the M800 and X800
	Almost all glofiish have very similar hardware
	Openmoko merges all my patches in their kernel!
	Official inclusion to Openmoko distribution

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
Project gnufiish

gnufiish Status
		Kernel (2.6.24/2.6.27) booted on _first attempt_
	Working
		I2C host controller
		I2C communication to CPLD and FM Radio
		USB Device mode (Ethernet gadget)
		Touchscreen input
		LCM Framebuffer
		LCM Backlight control
		GPS and Bluetooth power control
		GPIO buttons
	In the works
		Audio Codec driver (50% done)
		GSM Modem (SPI) driver (80% done)
		M800 Keyboard + Capsense driver (25% done)
		SPI glue to libertas WiFi driver (70% done)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
HOWTO

How was this done?
	Various reverse engineering techniques
		Take actual board apart, note major components
		Use HaRET (hardwar reverse engineering tool)
		Find + use JTAG testpads
		Find + use serial console
		Disassemble WinMobile drivers

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
Take hardware apart

Opening the case and void your warranty
%image "x800_backside_nobat_nocover.jpg"
Note the convenient test pads beneath the battery 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
Take hardware apart

Opening the case
%image "x800_opening_the_case.jpg" 800x600
If you have a bit of experience in taking apart devices, you can do that without any damage...

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
Take hardware apart

The Mainboard with all its shielding covers
%image "x800_mainboard_with_shielding.jpg" 800x600
Obvoiusly, the shielding needs to go

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
Take hardware apart

The application processor section
%image "x800_application_processor.jpg"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
Take hardware apart

The HSDPA modem section
%image "x800_hsdpa_modem.jpg"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
Take hardware apart

The backside
%image "x800_backside_with_lcm.jpg"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
JTAG pins

	Find + use JTAG testpads
		JTAG is basically a long shift register
		Input, Output, Clock (TDI, TDO, TCK)
		Therefore, you can try to shift data in and check if/where it comes out
		Automatized JTAG search by project "jtagfinder" by Hunz (German CCC member)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
JTAG pins

Find + use JTAG testpads
%image "x800_dbgconn_closeup.jpg"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
JTAG pins

Find + use JTAG testpads
%image "x800_debcon_pcb.jpg"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
JTAG pins

Find + use JTAG testpads
%image "x800_jtagfinder_probes.jpg"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
JTAG pins

Find + use JTAG testpads
%image "x800_jtagfinder.jpg"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
JTAG pins


Found JTAG pins
	Chain 1
		Samsung S3C2442 Application Processor
		Has standard ARM JTAG ICE
	Chain 2
		CPLD programming interface
	Remaining work
		find the nTRST and nSRST pins

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
Serial console


How to find the serial console
	Just run some code that you think writes to it
	Use a Scope to find typical patterns of a serial port
	I haven't actually done (or needed) this on the glofiish yet, but on many other devices
	RxD pin is harder to find, just trial+error usually works as soon as you have some interactive prompt that echo's the characters you write
	Don't forget to add level shifter from 3.3/5V to RS232 levels

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
What's HaRET

What is HaRET
	a Windows executable program for any WinCE based OS
	offers a control interface on a TCP port
	connect to it using haretconsole (python script) on Linux PC
	supports a number of popular ARM based SoC (PXA, S3C, MSM)
	features include
		GPIO state and tracing
		MMIO read/write
		virtual/physical memory mapping
		IRQ tracing (by redirecting IRQ vectors)
		load Linux into ram and boot it from within WinCE

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
Using HaRET

Using HaRET
	run the program on the target device
	connect to it using haretconsole over USB-Ethernet
	read GPIO configuration
		Create GPIO funciton map based on SoC data sheet
	watch for GPIO changes
		remove the signal from the noise
		exclude unitneresting and frequently changing GPIOs
	watch for GPIO changes while performing certain events
		press every button and check
		start/stop peripherals
		insert/eject SD card

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
Using HaRET


Using HARET
	watch for IRQ changes/events
		e.g. you see DMA3 interrupts while talking to the GSM
		read MMIO config of DMA controller to determine user: SPI
		read SPI controller configuration + DMA controller configuration
		find RAM address of data buffers read/written by DMA
	haretconsole writes logfiles
		you can start to annotate the logfiles 
	of course, all of this could be done using JTAG, too.
		but with HaRET, you mostly don't need it!!!

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
Disassembling WinCE drivers


Disassmbling WinCE drivers
	is the obvious thing to do, right?
	is actually not all that easy, since
		WinCE doesn't allow you to read the DLLs
			not via ActiveSync neither WinCE filesystem API's
		Apparently, they are pre-linked and not real files anymore
	luckily, there are tools in the 'ROM cooking' scene
		hundreds of different tools, almost all need Windows PC
		therefore, not useful to me
	conclusion: Need to understand the ROM image format

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
Disassembling WinCE ROM files

Disassembling WinCE ROM files
	'datextract' to extract different portions like OS image
	'x520.pl' to remove spare NAND OOB sectors from image and get a file
	split resulting image in bootsplash, cabarchive and disk image
	'xx1.pl' to split cabarchive into CAB files
	'partextract' to split disk image in partitions
	'SRPX2XIP.exe' (wine) to decompress XPRS compressed partition0+1
	'dumpxip.pl' to dump/recreate files in partition0 and 1
	'ImgfsToDump.exe' to dump/recreate files in partition2 (imagefs)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
Disassembling WinCE Drivers


Disassembling WinCE Drivers
	Now we finally have the re-created DLL's with the drivers
	Use your favourite debugger/disassembler to take them apart
	I'm a big fan of IDA (Interactive Disassembler)
		The only proprietary software that I license+use in 15 years
		There's actually a Linux x86 version
		Was even using it with qemu on my Powerbook some years back

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
Disassembling WinCE Drivers

Important drivers
	pwrbtn.dll: the power button ?!?
	spkphn.dll: high-level device management
	i2c.dll: S3C24xx I2C controller driver
	spi.dll: The GSM Modem SPI driver
	Sergsm.dll: S3C24xx UART driver, NOT for GSM
	SerialCSR.dll: CSR Bluetooth driver
	fm_si4700.dll: The FM Radio (I2C)
	battdrvr.dll: Battery device (I2C)
	keypad.dll: Keypad+Keyboard+Capsense (I2C)
	GSPI8686.dll: Marvell WiFi driver (SPI)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
Disassembling WinCE Drivers

Disassembling WinCE drivers
	Is typically hard, they're completely stripped
	Windows drivers are very data-driven, not many symbols/functions
	However, debug statements left by developers are always helpful
	After some time you get used to it
	You know your hardware and the IO register bases
		take it from there, look at register configuration
	What I've learned about WinCE driver development 
		... would be an entirely separate talk
	MSDN luckily has full API documentation

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
WinCE Registry


WinCE has a registry, too
	I never really understood what this registry is all about, but it doesn't matter ;)
	You can use 'synce-registry' to dump it to Linux
	Contains important information about
		how drivers are interconnected
		various configuration parameters of drivers


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
Links



	http://wiki.openezx.org/Glofiish_X800
	http://git.openezx.org/?p=gnufiish.git
	http://eten-users.eu/
	http://wiki.xda-developers.com/


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
Reverse Engineering and Porting Linux to a WM PDA Phone
Thanks


Thanks to
		Openmoko, Inc. for trying to create more open phones
		Hunz for his jtagfinder
		xda-developers.org for all their work on WinCE tools
		eten-users.eu for the various ETEN related ROM cooking projects
		Willem Jan Hengeveld (itsme) for his M700 ROM tools
		An undisclosed Indian Company for showing commercial interest in this project
		Samsung, for having 100% open source driver for their SoC's
		Ericsson, for publishing the full AT command set for their modems

