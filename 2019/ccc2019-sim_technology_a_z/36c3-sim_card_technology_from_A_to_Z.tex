\documentclass[aspectratio=169,11pt]{beamer}
\usetheme{default}
%\setbeamertemplate{frametitle}{}
\newenvironment{myline}
    %{\usebeamerfont{frametitle}\usebeamercolor[fg]{frametitle}\vfill\centering}
    {\usebeamerfont{frametitle}\vfill\centering}
    {\par\vfill}

\usetheme{Warsaw}
\usecolortheme{whale}

\title{SIM card technology from A(PDU) to X(RES)}
%\subtitle{Subtitle}
\author{Harald~Welte}
\date[Dec 2019, 36C3]{Chaos Communication Congress 2019}
\institute{osmocom.org}


\begin{document}

\begin{frame}
\titlepage
\end{frame}


\begin{frame}{Outline}
\begin{itemize}
	\item Relevant Specs + Spec Bodies
	\item Card Interfaces, Protocols
	\item Card File System
	\item SIM Evolution from 2G to 5G
	\item SIM Toolkit
	\item OTA (Over The Air)
\end{itemize}
\end{frame}


\begin{frame}{About the speaker}
\begin{itemize}
	\item Free Software + OSHW developer for more than 20 years
	\item Used to work on the Linux kernel from 1999-2009
	\item working with contact chip cards since ~1999, contactless since ~2006
	\item developing FOSS in cellular communications (Osmocom) since ~2008
	\begin{itemize}
		\item developed various SIM card related tools in software an hardware
	\end{itemize}
	\item Living and working in Berlin, Germany.
\end{itemize}
\end{frame}

\begin{frame}{Relation of SIM card specifications}
\includegraphics[width=150mm]{sim_card_specs.png}
\end{frame}

\begin{frame}{Relevant specification bodies/sources}
\begin{itemize}
	\item ISO (Integrated Circuit[s] Card)
	\item ITU (Telecom Charge Cards)
	\item ETSI (where GSM was originally specified)
	\item 3GPP (where 3G to 5G was specified)
	\item GlobalPlatform Card Specification
	\item Sun/Oracle JavaCard API, Runtime, VM
	\item GSMA
\end{itemize}
\end{frame}


% from APDU to Z... ?

\begin{frame}{The SIM: Subscriber Identity Module}
\begin{itemize}
	\item probably anyone in the audience has at least one, likely more
	\item ubiquitous; every device withe cellular connectivity has at least one
	\item not many people outside the telecom industry ever look at them in much detail
	\item SIM card hacking (in the security sense) has a tradition at CCC since at least 1998
	\begin{itemize}
		\item Vodafone Germany SIM card cloning: https://ftp.ccc.de/software/gsm/gsm\_hack.tar.gz
		\item SIM card simulator in Turbo C (1998): https://ftp.ccc.de/software/gsm/SIM\_sim.zip
	\end{itemize}
	\item meanwhile: SIM technology stack is getting more complex and deep
	\item let's recap what SIM cards actually are, and what they do
\end{itemize}
\end{frame}


\begin{frame}{Classic SIM in early GSM}
	\begin{figure}
	\centering
	\includegraphics[width=80mm]{c-netz-karte.jpg}
	\end{figure}
\begin{itemize}
	\item Idea of storing subscriber identity predates GSM (e.g. C-Netz since 1988)
	\item GSM from the very beginning introduces concept of SIM card
	\item store subscriber identity outside of the phone
	\item store some network related parameters
	\begin{itemize}
		\item static (like access control class)
		\item dynamic (like TMSI, Kc, ...)
	\end{itemize}
	\item full credit card size so it can be used in radios installed in (rented, shared company) cars.
\end{itemize}
\end{frame}


\begin{frame}{DIN EN ISO/IEC 7816}
\begin{itemize}
	\item the {\em mother of all smart card} spec
	\item "Integrated circuit(s) cards with contacts"
	\item 15 parts, most relevant are below:
	\begin{itemize}
		\item Part 1: Physical characteristics
		\item Part 2: Dimensions and location of the contacts
		\item Part 3: Electronic signals and transmission protocols
		\item Part 4: Interindustry commands for interchange
		\begin{itemize}
			\item Why not international inter-industry commands for interworking information interchange?  Anyone?
		\end{itemize}
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{GSM TS 11.11}
\begin{itemize}
	\item {\em Specification of the Subscriber Identity Module - Mobile Equipment (SIM-ME) Interface}
	\item repeats (and some times amends) large portions of 7816-1/2/3/4
	\begin{itemize}
		\item Section 4: physical characteristics
		\item Section 5: electronic signals, transmission protocol
	\end{itemize}
	\item but also specifies what makes the SIM a SIM: Information model, file system, commands
	\item last, but not least how to execute authentication: RUN GSM ALGORITHM
\end{itemize}
\end{frame}

\begin{frame}{Physical Smart Card Interface}
\begin{itemize}
	\item Relevant pins:
	\begin{itemize}
		\item VCC: Provides supply voltage (5V, 3V or 1.8V)
		\item CLK: Provides a clock signal (1 .. 5 MHz default)
		\item RST: To reset the card
		\item IO: bidirectional serial communications
	\end{itemize}
	\item Activation sequence triggers card to send ATR (Answer To Reset)
\end{itemize}
\begin{figure}
\centering
\includegraphics[width=100mm]{7816_activation.png}
\end{figure}
\end{frame}

\begin{frame}{Bit transmission level}
\begin{itemize}
	\item despite the clock, communication is asynchronous!
	\item baud rate derived from divided clock
	\item no defined phase relationship between clock and data
	\item serial data is just like UART/RS232, ... but:
	\begin{itemize}
		\item one line for both Rx and Tx
		\item direction changes once after every byte (ACK in T=0)
		\item direction changes every few bytes (TPDU state machine)
		\item timings are actually not very well specified
	\end{itemize}
\end{itemize}
\begin{figure}
\centering
\includegraphics[width=100mm]{7816_frame.png}
\end{figure}
\end{frame}

\begin{frame}{Smart Card Communication}
\begin{itemize}
	\item based on APDU (Application Protocol Data Unit) as per ISO 7816-4
	\begin{itemize}
		\item CLA (class byte)
		\item INS (instruction byte)
		\item P1, P2 (parameter bytes)
		\item Lc (command length)
		\item Command data
		\item Le (expected response length)
		\item Response data
		\item SW (status word)
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Smart Card Transmission Protocol}
\begin{itemize}
	\item different protocols transceive APDUs from/to cards
	\item T=0 most commonly used with SIM cards
	\item T=1 also possible but rarely used in SIM
	\begin{itemize}
		\item specs require phones to implement both T=0 and T=1
		\item SIM card can be either T=0 or T=1
		\item T=1 more used in banking / crypto smart card world
	\end{itemize}
	\item APDU gets mapped to protocol-specific TPDU (Transmission Protocol Data Unit)
	\begin{itemize}
		\item: Example Command TPDU: A0 A4 00 00 02 3F 00
		\item: Example Response TPDU: 90 00 (just status word)
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Card Filesystem}
\begin{itemize}
	\item most smart cards contain file system abstraction
	\item cannot be mounted (not exposed like a block device / USB drive!)
	\item access based on file-level commands (analogy: more like MTP/PTP)
	\item some similarities to general-purpose (computer) OS file systems:
	\begin{itemize}
		\item MF: Master File (root directory)
		\item DF: Dedicated File (subdirectory)
		\item EF: Elementary File (data file)
	\end{itemize}
	\item However, much more comprehensive than computer OS file systems, e.g.
	\begin{itemize}
		\item {\em transparent EF}: opaque stream of data, like PC
		\item {\em linear fixed EF}: fixed-size records, seekable
		\item {\em cyclic fixed EF}: ring buffer of records, seekable
		\item {\em incrementable}: for monotonically incrementing counters
	\end{itemize}
	\item Each file has Access Control Conditions (ACC)
	\begin{itemize}
		\item read/write/update only after PIN1/PIN2/ADM-PIN
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Key SIM Card Commands}
\begin{itemize}
	\item SELECT (file)
	\item READ RECORD / UPDATE RECORD
	\begin{itemize}
		\item for record-oriented EF
	\end{itemize}
	\item READ BINARY / UPDATE BINARY
	\begin{itemize}
		\item for transparent EF
	\end{itemize}
	\item CHANGE CHV / DISABLE CHV / ENABLE CHV
	\begin{itemize}
		\item CHV: Card Holder Verification (PIN)
	\end{itemize}
	\item RUN GSM ALGORITHM
	\begin{itemize}
		\item ask SIM to execute authentication algorithm in card
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{SIM card filesystem hierarchy}
\parbox{.4\textwidth}{
\begin{itemize}
	\item MF (3F00)
	\begin{itemize}
		\item DF\_TELECOM (7F10)
		\begin{itemize}
			\item EF\_SMS
			\item EF\_MSISDN
			\item ...
		\end{itemize}
		\item DF\_GSM (7F20)
		\begin{itemize}
			\item EF\_IMSI
			\item EF\_Kc
			\item ...
		\end{itemize}
		\item EF\_ICCID
		\item ...
	\end{itemize}
\end{itemize}
}\hfill\parbox{.6\textwidth}{
	\includegraphics[width=80mm]{sim_fs.png}
}
\end{frame}


\begin{frame}{3G: ETSI UICC and the 3GPP USIM}
\begin{itemize}
	\item The GSM SIM was fully specified by ETSI in TS 11.11
	\item As GSM specs moved from ETSI to 3GPP, card specs were split:
	\begin{itemize}
		\item ETSI UICC (Universal Integrated Circuit Card)
		\begin{itemize}
			\item like a {\em base class} abstracting out those parts that are not cellular
				related, or at very least not 3GPP network related
		\end{itemize}
		\item 3GPP USIM Application on top of UICC
		\begin{itemize}
			\item specifies those parts specifically relevant to 3GPP networks
			\item implemented in ADF\_USIM (Application Dedicated File)
			\item ADF can be entered via SELECT, similar to classic DF
		\end{itemize}
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{4G: The ISIM}
\begin{itemize}
	\item 3G/LTE reuses the existing 3G Authentication (UMTS AKA)
	\item 4G/LTE simply reuses existing USIM
	\item some new optional files were introduced in ADF\_USIM
	\item IMS (IP Multimedia System used for not only VoLTE) specifies ISIM application
	\begin{itemize}
		\item stores additional IMS related parameters like SIP server / user identity
		\item presence of ISIM not required for IMS to work
		\item if present, ISIM application present next to USIM (and possibly SIM)
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{SIM cards in 5G}
\begin{itemize}
	\item 5G reuses existing 3G/4G USIM
	\item some new optional files were introduced in ADF\_USIM
	\item SUCI (Subscriber Concealed Identifier) can optionally be computed by SIM
	\begin{itemize}
		\item this is the only feature requiring different card / apps on card
	\end{itemize}
\end{itemize}
\end{frame}


\begin{frame}{SIM card hardware}
\begin{itemize}
	\item processor core
	\begin{itemize}
		\item many different vendors and architectures, from 8-bit 8051 to 32bit ARM
		\item today quite often ARM SCxxx "Secure Core" family
		\item documentation on hardware, often even simple one-page data sheets not public
	\end{itemize}
	\item built-in RAM
	\item built-in ROM (at least boot loader, possibly also OS)
	\item built-in flash (file system storage, possibly also OS, applications)
	\item contrary to expensive crypto smart cards, SIM card chip mostly selected purely by low cost
	\begin{itemize}
		\item blame pre-paid cards for that
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{SIM card software}
\begin{itemize}
	\item Every Smart Card has a Card Operating System (COS)
	\item Cards without COS are simple memory cards (like I2C EEPROM), insufficient for SIM
	\item Card OS for Crypto Smart Cards (banking, access control) often publicly known
	\item SIM Card OS are rarely known / publicly documented or even named
	\item Example: ARM not only offers SIM card CPU core designs, but also OS (Kigen OS)
	\item SIM Card OS is {\em implementation detail}, almost everything relevant is standardized
		across OS vendors
\end{itemize}
\end{frame}

\begin{frame}{SIM card software modularity}
\begin{itemize}
	\item Early SIM cards were (likely) monolithic,
	\begin{itemize}
		\item no separation between OS and SIM application
	\end{itemize}
	\item Today, SIM cards software is modular
	\begin{itemize}
		\item Core OS
		\item Applications (SIM, USIM, ISIM, ...)
	\end{itemize}
	\item traditionally, OS very chip/hardware dependent, non-portable
	\item traditionally, applications very OS dependent, non-portable
\end{itemize}
\end{frame}


\begin{frame}{Java Cards}
\begin{itemize}
	\item independent of SIM cards, Java Smart Cards have been developed
	\item based on Java Card Platform specifications by Sun (now Oracle)
	\item first cards in 1996 by Schlumberger (now Gemalto)
	\item independent of SIM cards, Java Smart Cards have been developed in 1996 by Schlumberger
	\item most cards implement GlobalPlatform specifications for vendor-independent management
	\begin{itemize}
		\item super constrained, weird subset of Java
		\item special on-card VM (not normal JVM)
		\item special CAP format (not normal JAR)
		\item Idea: Portability of Cardlets (card applications)
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Java SIM Cards}
\begin{itemize}
	\item There is no functional requirement for a SIM/USIM/ISIM to be a java card
	\item In reality, most SIM cards probably are Java Cards these days
	\item Portability is the main driver here
	\item Operators want to share same applications over multiple vendors/generations of cards
	\item 3GPP and ETSI specify Java APIs / packages available specifically on Java SIM cards
\end{itemize}
\end{frame}

\begin{frame}{SIM Toolkit (STK) / Card Application Toolkit (CAT)}
\begin{itemize}
	\item Ability by card to offer applications with UI/menu on the phone
	\item New APDUs/Instructions
	\begin{itemize}
		\item TERMINAL PROFILE
		\item ENVELOPE
		\item FETCH
		\item TERMINAL RESPONSE
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Proactive SIM}
\begin{itemize}
	\item SIM cards are "slave" in the 7816 interface
	\item All actions are triggered by the phone, card can only respond
	\item Proactive SIM works around this restriction
	\item Piggy-backs proactive commands to card responses
	\item Phone can be requested to poll the SIM if it has some proactive commands pending
	\item Phone can be requested to provide event notifications
\end{itemize}
\end{frame}

\begin{frame}{OTA (Over-The-Air)}
\begin{itemize}
	\item Ability for operator to transparently communicate with SIM card in the field
	\item Based on Proactive SIM
	\item Can use different transport channels, such as
	\begin{itemize}
		\item SMS-PP (normal SMS as you know it)
		\item SMS-CB (bulk update of cards via cell broadcast)
		\item USSD (Release 7)
		\item BIP (via CSD, GPRS): ETSI TS 102 223 / TS 102 127
		\item now also HTTPS (Release 9)
	\end{itemize}
	\item Cryptographic security mechanisms specified, but detailed use up to operator
	\begin{itemize}
		\item Message Authentication (optional)
		\item Message Encryption (optional)
		\item Replay Protection (optional)
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Remote File Management (RFM)}
\begin{itemize}
	\item Introduced in Release 6
	\item Common use case of OTA
	\item Allows remote read / update of files in file system
	\item Example: Change of preferred/forbidden roaming operator list
	\item Example (ancient): Backup of phonebook at operator
\end{itemize}
\end{frame}

\begin{frame}{Remote Application Management (RAM)}
\begin{itemize}
	\item Introduced in Release 6
	\item Common use case of OTA
	\item Allows remote installation / removal of applications on card
	\item Example: New multi-IMSI application (MVNOs)
	\item Example: New STK applications
\end{itemize}
\end{frame}

\begin{frame}{OTA over HTTPs}
\begin{itemize}
	\item 4G and beyond don't natively support SMS-PP, USSD, ...
	\item In Release 9, OTA over HTTPs is first introduced
	\item References to GlobalPlatform 2.2 Amd B + ETSI TS 102 226
	\item Uses HTTP as per RFC 2616
	\item Uses PSK-TLS as per RFC4279, RFC4785, RFC5487
	\begin{itemize}
		\item TLS 1.0 / 1.1: TLS\_PSK\_WITH\_3DES\_EDE\_CBC\_SHA
		\item TLS 1.0 / 1.1: TLS\_PSK\_WITH\_AES\_128\_CBC\_SHA
		\item TLS 1.0 / 1.1: TLS\_PSK\_WITH\_NULL\_SHA (RFC4785)
		\item TLS 1.2: TLS\_PSK\_WITH\_AES\_128\_CBC\_SHA256 (RFC5487)
		\item TLS 1.2: TLS\_PSK\_WITH\_NULL\_SHA256 (RFC5487)
	\end{itemize}
	\item IP and TCP socket terminated in phone, only TCP payload handled by card
\end{itemize}
\end{frame}

\begin{frame}{OTA over HTTPs}
\begin{itemize}
	\item Card acts as HTTP client performing HTTP POST
	\item TLS payload is remote APDU format of ETSI TS 102 226
	\item additional HTTP headers
	\begin{itemize}
		\item X-Admin-Targeted-Application
		\item X-Admin-Next-URI
		\item X-Admin-Protocol: globalplatform-remote-admin/1.0
		\item X-Admin-From
		\item X-Admin-Script-Status
		\item X-Admin-Resume
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{S@T}
\begin{itemize}
	\item a strange beast specified outside of ETSI/3GPP
	\item allows SIM toolkit applications without writing Java or native applications
	\item special byte code format interpreted by S@T browser
	\item to me, one of those WTF? technologies
\end{itemize}
\end{frame}

\begin{frame}{GSMA eSIM}
\begin{itemize}
	\item system for remote provisioning of {\em profiles} to SIM
	\item allows change of operator / identity without replacement of physical card
	\item main use case is non-removable / soldered SIM chip (MFF2)
	\item also available from some operators in classic smart card size
	\item main relevant spec is GSMA SGP.22
	\item based around PKI between operators, all parties approved by GSMA
\end{itemize}
\end{frame}



\begin{frame}{The CCC event SIM cards}
\begin{figure}
	\centering
	\includegraphics[width=50mm]{32c3-sim-front.jpg}
	\includegraphics[width=50mm]{32c3-sim-back.jpg}
\end{figure}
\begin{itemize}
	\item are Java SIM + USIM cards
	\item support OTA, RAM, RFM (via SMS-PP and maybe BIP, not HTTPS)
	\item you can get the ADM PIN and OTA keys from the event GSM team
	\item a "hello world" Java applet and tools for installation are provided (thanks to shadytel + Dieter Spaar)
	\item identities and key data can be modified using Osmocom pySim software
\end{itemize}
\end{frame}

%\begin{frame}{The evoluation of form factors}
	%\includegraphics{sim_card_formats.png}
%\end{frame}

\begin{frame}{Further Reading (hyperlinked)}
\begin{itemize}
	\item \href{https://simalliance.org/wp-content/uploads/2017/01/MobileConnectSteppingStones_FINAL_.pdf}{SIM alliance stepping stones}
	\item \href{https://osmocom.org/projects/simtrace2/wiki}{SIMtrace2 wiki}
	\item \href{https://simjacker.com/downloads/technicalpapers/AdaptiveMobile_Security_Simjacker_Technical_Paper_v1.01.pdf}{Simjacker vulnerability}
	\item \href{https://opensource.srlabs.de/projects/simtester/wiki}{SRLabs SIMtester}
	\item for historians
	\begin{itemize}
		\item \href{http://ftp.ccc.de/software/gsm/SIM_sim.zip}{CCC SIM simulator in Turbo C}
		\item \href{http://ftp.ccc.de/software/gsm/gsm_hack.tar.gz}{CCC sim clone / D2 Pirat}
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Thanks}
Thanks for your attention.

	You have a General Public License to ask questions now :)
\end{frame}

\end{document}
