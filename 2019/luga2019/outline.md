
[die Letzten] 20 Jahre als FOSS-Entwickler

* persönlicher Werdegang
 * angeblich das stehen gelernt, um an Knöpfe der Stereoanlage ranzukommen
 * mit 3 Jahren die erste [eigene] Schreibmaschine
 * ab ~1991 (12 Jahre) mit in Datennetzen (Z-Netz, FIDO, ..)
 * ab ~1994 im Kommunikationsnetz Franken e.V.
 * zur Schulzeit aushilfsweise Sysadmin in Deutschlands 1. Internet Cafe "Falken's Maze"
  * es war ursprünglich ein "Online-Bistro": 4 Rechner mit Modems + Gebührenzähler zur BBS-Einwahl
 * Berufsschule IV in Fürth ans Internet gebracht
 * späte 90er: Linux-Kurse in Nürnberg gegeben (KNF, VHS, ...)
 * 1997 erster Besuch eines CCC-Kongresses (damals noch im HAKP/Berlin)
 * kein Abi (Gymnasium hätte verpflichtend Kunstunterricht gehabt)
 * kein Studium
 * jede Menge firmen, die Mitte u.v.a. Ende der 90er Leute suchen, die sich mit Internet auskennen
  * das hiess damals: IP, Router aufsetzen, Modems, ISDN, Standleitungen, Frame Relay, Mailserver, DNS
 * schon unter 18 erste Aufträge ("schwarz", selbständigkeit ginge ja nur mit Vormundschaftsgericht)
 * seit 18. Lebensjahr selbständig tätig: Entwicklung, Beratung, Security, Reverse Engineering, ...

* Was hat das mit FOSS zu tun?
 * Bis 1994 nur DOS-User
 * Intensiver Nutzer von Freeware + Shareware (wie z.b. CrossPoint)
 * Ein paar kleine Shareware-Programme in Turbo Pascal fuer DOS
 * Man konnte TCP/IP, PPP, SMTP, POP3, FTP, ... unter DOS sprechen: KA9Q NOS mit ISDN packet treiber!
 * Ab 1994/95 erste Schritte in Linux (über KNF, obwohl dort eher BSD verbreitet)
  * viele Monate Installation fehlgeschlagen: Bootdisk hatte nur SCSI-Treiber, ich hatte IDE-Platte :{
 * Sozialisierung in der GNU/Linux community, Verständnis für Freie Software als Philosophie
 * Erste eigene patches + contributions z.B. gegen smail, pppd/pptpd, ...
 * "dayjob" war v.a. mit Entwicklung proprietärer Software (elektronische Dokumentenarchivierung bei Bank)
  * wir haben dort Linux kernel 2.3.99 produktiv verwendet.  Keiner durfte es Linux nennen, es war immer nur UNIX genannt, weil das den Leuten ein begriff war ;)
 * FOSS-Haacking war reine Freizeitaktivität, "just for fun"
 * 1998: Ausprobieren des 2.3er kernels (neues firewalling 'netfilter/iptables')
  * hat kein conntrack/NAT fuer IRC!
 * Italienurlaub 1998: IRC conntrack + NAT helper fuer netfilter
 * "dayjob" bringt viel Geld ein, aber ist technisch eher wenig attraktiv
 * 2000: Angebot, bei "Conectiva Linux" in Brasilien anzufangen
  * 100% Free Software hacking, all day long
  * Vertrag: Du bekommst USD X/Monat, mach was Du willst, aber alles wird GPLv2 Lizenziert. Und sag uns wasDu machst.
  * erstmal aus der "Schusslinie" des deutschen Wehrdienstes
  * ~6 Monate spaeter: dot-com-bubble blast
  * freiwillige Rückreise nach DE ("Ihr koennt von meinem Gehalt 2 Brasilianer weiterzahlen, ich gehe")
 * dayjob: freiberufliche Tätigkeit als Linux-Kernel-Entwickler
 * 2002: Umzug nach Berlin (nicht nur, aber auch wegen CCC)
 * 2006: Open Hardware + FOSS RFID: OpenPCD, OpenPICC, librfid, libmrtd
 * 2007: "Chief Architect: System Level" fuer FOSS-Smartphone bei OpenMoko in Taiwan
 * 2008: Open Source Liaison bei VIA Technologies (Taiwan)
 * 2008: Open Source GSM: Erste Anfaenge von bs11-abis/bsc-hack -> OpenBSC
 * 2009: Consulting/Training der Linux-Kernel-Entwickler bei Samsung System LSI (SoC sparte)
 * Geld wird mit Linux-Entwicklung+Consulting verdient, Zeit wird in FOSS GSM investiert
 * 2010: OsmocomBB (GSM Protocol Stack + Firmware fuer TI Calypso basierte Telefone
 * 2011: Mitgründer sysmocom GmbH (professional services + products around Osmocom)
 * ...
 * 2019: 
 * immernoch 90% meiner Zeit mit Entwicklung [meist] systemnaher Freier Software in C unter Linux oder Open
   Source Hardware (OSHW) bzw. Device-Firmware befasst

FIXME: gpl-violations.org
FIXME: Nachrichten aus der Zeit korrelieren

 * thanks to
  * Kommunikationsnetz Franken e.V.
  * Chaos Computer Club e.V. + CCC Berlin e.V.
  * Rusty Russell (original netfilter/iptables author)
  * 


* Veränderung der FOSS-Szene
 * Konferenzen
  * früher kleiner, technischer, nicht-kommerzieller
  * oft an Unix User Groups angegliedert
  * oft in Universitateen stat teuren Konferenzzentren
  * z.B. Linux-Kongress, UKUUG, ...:
  * heute: Quasi-Monopolismus durch Linux Foundation
 * Firmen
  * Linux war der Underdog.
  * Early Adopters waren viel im Deutschen KMU + Mittelstand
  * Heute: Grosskonzerne, v.a. Silicon Valley, Cloud.
 * Recht
  * Früher: Entwickler behalten Copyright (RedHat Verträge USA); Verträge wie meiner bei Conectiva
  * Heute: Neun Monate Vertragsverhandlungen mit Rechtsabteilung eines Grosskonzerns.  Und die hat anscheinend weniger Ahnung von FOSS als ich :/
 * Einsatzbereiche
  * Früher: Server + Developer Workstations
  * Heute: Embedded + Cloud.
   * gefühlt wird Grossteil der Entwicklungen am Kernel nur für Cloud. bzw. hyperskalare Umgebungen
     entwickelt.  Jede Menge Infrastruktur, mit der "Otto-Normal-User/Entwickler" nie in Beruehrung kommt.
 * Level
  * Früher natürlich mehr C-Entwicklung, es gab ja noch kein Java, Python, PHP, Ruby, Rust, Go, ...
  * Früher mehr native "Systemprogramme", heute sehr Web-Dominiert
 * Komplexität
  * Komplexität der Technologie nimmt stetig zu
  * zu wenige Leute wissen noch, was *wirklich* auf Prozessor, Bus, ... passiert.
  * es gibt auch immer weniger brauchbare Literatur über neue Hardwaretechnologien :/
 * Fragmentierung
  * weiterer Scope von FOSS-Software und Technologie insgesamt
  * grosse Zersplitterung nach Programmiersprachen, Frameworks, IDEs
  * Kurios: Früher waren es weniger Leute, aber ich habe mich mehr Teil einer Community gefuehlt als heute.
 * Lizenzen
  * Gefuehlt / Behauptet: Früher mehr copyleft (GPL, ...) heute mehr permissive (Apache, BSD, MIT, ...)
  * Realitaet ist eher: Viele neue Projekte v.a. im Web-Bereich sind permissive licensed. Deswegen wird
    copyleft-Software nicht weniger.
  * grosesr Fan von AGPLv3 (Hauptlizenz von Osmocom).  Wird leider oft nur als Hebel fuer dual-licensing
    verwendet ("kauft andere Lizenz von uns")
  * Massiver Widerstand gegen copyleft aus bestimmten Kreisen im Silicon Valley. Viele Standardverträge
    scliessen copyleft aus. 
