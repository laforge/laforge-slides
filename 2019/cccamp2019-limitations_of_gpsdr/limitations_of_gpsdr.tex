\documentclass[11pt]{beamer}
\usetheme{default}
%\setbeamertemplate{frametitle}{}
\newenvironment{myline}
    %{\usebeamerfont{frametitle}\usebeamercolor[fg]{frametitle}\vfill\centering}
    {\usebeamerfont{frametitle}\vfill\centering}
    {\par\vfill}

\usetheme{Warsaw}
\usecolortheme{whale}

\title{Limitations of General Purpose SDR}
%\subtitle{Subtitle}
\author{Harald~Welte}
\date[August 2019, CCCamp2019]{Chaos Communication Camp 2019}
\institute{osmocom.org}


\begin{document}

\begin{frame}
\titlepage
\end{frame}


\begin{frame}{Outline}
	\tableofcontents[hideallsubsections]
\end{frame}


\begin{frame}{About the speaker}
\begin{itemize}
	\item Free Software + OSHW developer for more than 20 years
	\item Used to work on the Linux kernel from 1999-2009
	\item By coincidence among the first people enforcing the GNU GPL in court
	\item Since 2009 developing FOSS in cellular communications (Osmocom)
	\item Living and working in Berlin, Germany.
\end{itemize}
\end{frame}


\section{Introduction: GP-SDR Everywhere}

\begin{frame}{SDR vs. General-Purpose SDR}
\begin{itemize}
	\item virtually any radio of the last 10+ years is SDR
	\item mobile phones, cordless phones, DVB receivers, etc.
	\item most of them perform essential signal processing steps in software
	\begin{itemize}
		\item Inside DSP cores with proprietary firmware
		\item Inside FPGA cores with proprietary bitstream
	\end{itemize}
	\item on the other hand there are devices like USRP, LimeSDR, HackRF, BladeRF, ...
	\begin{itemize}
		\item Only up/down-conversion, ADC/DAC (and possibly decimation) in hardware
		\item All actual signal processing happening in code on general-purpose CPU (x86, ARM)
	\end{itemize}
	\item to differentiate the latter from the former, I call them {\em General Purpose SDR (GP-SDR)} devices
\end{itemize}
\end{frame}


\begin{frame}{History of key GP-SDR projects}
Timeline
\begin{itemize}
	\item 2001: {\em GNURadio} software project launched by Eric Blossom, funded by John Gilmore
	\item 2004: Matt Ettus launches the {\em USRP}
	\item 2012: Steve Markgraf releases {\em rtl-sdr}
	\item 2013: Nuand launches {\em bladeRF} Kickstarter
	\item 2014: Michael Ossman launches {\em HackRF One} Kickstarter
	\item 2017: Lime Micro launches {\em LimeSDR} on crowdsupply
\end{itemize}
\end{frame}

\begin{frame}{Accessibility of GP-SDR}
\begin{itemize}
	\item Over the last decade-or-so, GP-SDR hardware becomes widely available
	\item ultra-low-cost devices mostly for receive only
	\item medium to high cost devices for receive + transmit
	\item many people from a software and/or ITSEC background start to play with it
	\item learning curve is steep if you don't have an RF and/or electronics background
	\item quite some FOSS projects around, not many of them easy-to-use
	\item still many systems/protocols/technologies without any (or highly incomplete) FOSS
\end{itemize}
\end{frame}


\begin{frame}{Accessibility of GP-SDR}
\begin{itemize}
	\item It's great that any technology, including GP-SDR is becoming more accessible
	\item in reality though, many people have misconceptions about he complexity involved
	\item sadly, just buying a random SDR and installing some FOSS is *not* going to enable you to run a cellular base station
	\item particularly true for applications like cellular technologies (GSM, GPRS, UMTS, LTE, NR, ...) 
	\item Why is that?
\end{itemize}
\end{frame}

\section{Limitations}

\subsection{Clock}

\begin{frame}{Clock Accuracy}
\begin{itemize}
	\item Many radio standards require high clock accuracy (e.g. 30 ppb = parts per billion)
	\item Most GP-SDR devices have standard crystal (20 ppm = parts per million)
	\begin{itemize}
		\item That's about a factor 1000 (ppm vs. ppb) worse
	\end{itemize}
	\item Some have TCXO with 280 ppb stability, still not sufficient for reliable, spec-compliant operation of cellular systems
	\item Operating 2G/3G/4G base stations without proper clock is bound to be unreliable and show all
		kinds of failure patterns, including phones not reliably even detecting the base station
\end{itemize}
\end{frame}

\begin{frame}{Clock Stability}
\begin{itemize}
	\item It's not as easy as calibrating your clock once, but ...
	\item also clock drift over time due to temperature, aging.
	\item Result: Calibrating your clock against a reference once is insufficient, you'd need to
		re-calibrate very often if you work with an inaccurate clock
	\item In receive-only and particularly in non-realtime/post-processing use cases, you can have
		software track clock drift to compensate
	\item For reliable, real-world performance, you need a proper clock reference
\end{itemize}
\end{frame}

\begin{frame}{Clock Phase Noise / Jitter}
\begin{itemize}
	\item Frequency accuracy and drift are not the only relevant characteristics
	\item Clock phase noise in the analog domain (or clock jitter in the digital domain) causes all kinds of distortions across your Rx and/or Tx signal chains
	\item This is particularly critical on some devices that don't have PLLs with very effective loop filters between the external clock reference clock and the ADC/DAC sample clock (LimeSDR-mini)
\end{itemize}
\end{frame}

\begin{frame}{Clock reference options}
\begin{itemize}
	\item OCXO (Ovenized Crystal Oscillator)
	\begin{itemize}
		\item Need calibration against more stable reference typically about 6-12 months and then keep the clock stable enough until next calibration
	\end{itemize}
\item GPS-DO (GPS Disciplined Oscillator)
	\begin{itemize}
		\item typically use an OCXO or VCTCXO which is disciplined / steered by the clock reference generated by a GPS receiver (1PPS signal)
		\item Extremely stable, but require permanent GPS antenna / coverage
	\end{itemize}
	\item Rubidium Oscillator
	\begin{itemize}
		\item Very stable, much longer than OCXOs
		\item No external reference/antenna
		\item Rather large and power-hungry
		\item Inexpensive 2nd hand on eBay
	\end{itemize}
	\item (Chip Scale) Atomic Clocks
	\begin{itemize}
		\item Too expensive for most use cases, even professional use cases
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Clock reference parameters}
If you want to provide an external reference to your GP-SDR, you have to make
sure it is compatible in the following parameters
\begin{itemize}
	\item Frequency (some SDRs use 10MHz, others 30.72MHz, LimeSDR-mini 40MHz)
	\item Sine or Square
	\item Voltage
\end{itemize}
Also: Never forget that almost all SDRs require you to actively enable/select the external reference using some software/driver parameter!
\end{frame}


\subsection{Misc}

\begin{frame}{Full-Duplex operation}
\begin{itemize}
	\item Most cellular base stations transmit and receive simultaneously (on different frequencies)
	\item This requires a full-duplex capable SDR!
\end{itemize}
\end{frame}

\begin{frame}{Timestamped Commands}
\begin{itemize}
	\item In many systems, particularly those baed on TDMA (Time Division Multiple Access) like GSM and LTE, it is not sufficient to simply transmit a stream of samples at any undetermined time, but you need to be able to deterministically instruct the SDR to transmit a given burst of samples at a specified value of the ADC clock count.
	\item Similarly, received samples must be timestamped at the ADC sample clock
	\item This feature needs support in the SDR hardware/gateware, firmware, host software and throughout the driver stack
\end{itemize}
\end{frame}

\begin{frame}{Sync between Rx and Tx}
\begin{itemize}
	\item In full-duplex systems, Receive and Transmit typically must be synchronized, i.e. the software must make the Rx and Tx streams align around the same time frames
	\item The reference point for this is *not* the time of ADC input or DAC output which is where the samples are timestamped in the digital domain
	\item Rather, the analog group delay of the entire chain between Antenna and ADC (Rx) or DAC and Antenna (Tx) must be compensated for.  This value must be determined experimentally for each and every board.  And it changes if you attach more external components to your Rx/Tx chains.
	\item This value is board/product specific, even if you use the same driver (UHD, SoapySDR) and it must be compensated for in software but the application, outside of the drivers 
\end{itemize}
\end{frame}

\begin{frame}{Vendor Drivers (1/2)}
\begin{itemize}
	\item You're buying hardware, but a large amount of the product is software
	\begin{itemize}
		\item device firmware
		\item FPGA logic
		\item host drivers
	\end{itemize}
	\item Software quality is often unfortunately much worse than hardware quality
	\item Every vendor driver exposes different API (which is often not stable across releases)
\end{itemize}
\end{frame}

\begin{frame}{Vendor Drivers (2/2)}
\begin{itemize}
	\item Market politics prevent all vendor settling on common drivers/interface
	\begin{itemize}
		\item UHD is maintained by Ettus Labs (now NI), who won't merge drivers for competitors
		\item this situation is unheard of in other areas. Imagine a Linux kernel sub-system
			maintained by an Intel engineer refusing to merge AMD drivers or vice-versa ?!?
	\end{itemize}
	\item Additional abstraction layers on top of vendor drivers (SoapySDR, ..) often have to generalize and you may loose capability to control low-level aspects of the device
\end{itemize}
\end{frame}

\begin{frame}{Underruns / Overruns}
\begin{itemize}
	\item Each SDR contains an ADC/DAC which produces/consumes a constant amount of samples per second
	\item The entire computing system attached to that ADC/DAC must be able to continuously produce (Tx) and consume (Rx) those streams
	\item In reality, most elements (userspace, kernel, USB stack, USB host controller, USB device
		controller, firmware, ...) operate on buffers of some size
	\item Scheduling of CPU resources and I/O bandwidth must occur often enough to guarantee processing of
		all samples at any time
	\item Linux kernel real-time scheduling (SCHED\_RR) can be of assistance
	\item Beware of dynamic power management systems such as CPU voltaage/frequency scaling
	\item Beware of BIOS / System Management Mode code executed at hardware events (attaching monitors, power supplies, docking stations, ...)
\end{itemize}
\end{frame}


\subsection{Transmitter}

\begin{frame}{Tx Gain settings}
\begin{itemize}
	\item After the DAC, there are typically several gain stages, e.g.
	\begin{itemize}
		\item before the mixer/upconverter, 
		\item inherent to mixer/upconverter, 
		\item between mixer/upconverter and filters, ...
	\end{itemize}
	\item It's typically up to the user (application software) to configure all those gain stages of one
		specific GP-SDR model in a way that their gain is most linear and doesn't introduce any
		distortions due to operating the gain stages too close to their limit where they become
		non-linear
	\item This is often a lot of manual work involving radio standard specific measurement devices and
		fiddling with the various gain values until you hit a sweet spot
	\item Unfortunately, this often depends on your signal level as well as frequency/band
\end{itemize}
\end{frame}

\begin{frame}{Tx Power level}
\begin{itemize}
	\item RF output power of GP-SDR is typically rather low (around 0 dBm to +10 dBm)
	\item insufficient for any real-world operation beyond your lab desk
	\item amplification is needed
	\item Why not simply attach a power amplifier (from eBay)?
	\begin{itemize}
		\item output signal often contains significant harmonics or other unwanted components
		\item amplifier will amplify all of that together with intended signal
		\item can cause massive interference at other frequencies
	\end{itemize}
	\item Solution: Proper filtering before and possibly also after the PA!
\end{itemize}
\end{frame}


\begin{frame}{Tx Power Amplifiers}
\begin{itemize}
	\item Amplifiers need to be *highly* linear
	\item Unfortunately, most real-world devices, particularly those affordable
	\item There's also a trade-off between efficiency and linearity
	\item Signals with high PAPR (Peak-to-Average Power Ratio) get distorted a lot
	\item Commercial Base Stations perform digital, adaptive pre-distortion to compensate for amplifier
		non-linearities, whose computational complexity is significant (large DSP/FGPA/ASIC)
\end{itemize}
\end{frame}


\begin{frame}{Tx Power level calibration}
\begin{itemize}
	\item In any cellular standard, you operate with well-defined, absolute transmit power levels
	\item Any GP-SDR device is completely uncalibrated and will give you different output levels at each
		frequency.
	\item If you get +4 dBm at one frequency, and you change the frequency/channel, you might get -5 dBm on another
	\item well-defined / known / calibrated  transmit power levels are mandatory for cell coverage planning and preventing/reducing interference with intra-frequency neighbors
\end{itemize}
\end{frame}

\subsection{Receiver}

\begin{frame}{Rx: Harmonics}
\begin{itemize}
	\item A typical SDR frontend is a wide open mixer
	\item If you want to receive at 900 MHz, it will also receive at 1.8GHz
	\item You need an analog band filter between Antenna and Rx input
	\item Ideally, it should cover only the band of interest
\end{itemize}
\end{frame}

\begin{frame}{Rx: Dynamic Range}
\begin{itemize}
	\item A typical SDR frontend is a wide open receiver
	\item Any energy on any frequency (near or far) will pass to the input LNA or even to the mixer input,
		or possibly even down to the ADC
	\item Any energy that is not your wanted to-be-received channel
	\begin{itemize}
		\item might de-sensitize your receiver as you must make sure to not set any gain too high that makes any of those unwanted signals clip
		\item might limit your dynamic range as you must make sure the strongest of the unwanted signals doesn't exceed the dynamic range of your ADC
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Rx: Power Level}
\begin{itemize}
	\item Cellular systems are specified to use receivers that are calibrated at least to something like +/- 2dB, meaning that any received signal arrives with a calibrated absolute receive signal strength information.
	\item GP-SDR is not calibrated to anything.  You might get indications of {\em dB Full Scale} but then again, like in the Tx case, all the frequency-dependent analog gains are not compensated or accounted for.
\end{itemize}
\end{frame}

\section{Summary}

\begin{frame}{Summary}
\begin{itemize}
	\item This is not to scare you away from SDR!
	\item By all means, do get into this exciting technology
	\item Receive-only use cases are much easier
	\item Just don't expect magical results overnight
	\item Particularly don't expect to use it in any kind of real-world deployment
	\item Getting a GP-SDR is likely not the biggest investment; be prepared to also
	\begin{itemize}
		\item to get some measurement equipment like spectrum analyzer
		\item to get a variety of accessories, including GPS-DO, filters, ...
		\item to invest more time than you might have intended
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Thanks}
Thanks for your attention.

	You have a General Public License to ask questions now :)
\end{frame}

\end{document}
