\documentclass[aspectratio=169,11pt]{beamer}
\usetheme{default}
%\setbeamertemplate{frametitle}{}
\newenvironment{myline}
    %{\usebeamerfont{frametitle}\usebeamercolor[fg]{frametitle}\vfill\centering}
    {\usebeamerfont{frametitle}\vfill\centering}
    {\par\vfill}

% ensure the page number is printed in front of the author name in the footer 
\newcommand*\oldmacro{}
\let\oldmacro\insertshortauthor% save previous definition
\renewcommand*\insertshortauthor{%
  \leftskip=.3cm% before the author could be a plus1fill ...
  \insertframenumber\,/\,\inserttotalframenumber\hfill(CC-BY-SA)\hfill\oldmacro}

\usepackage[pdf]{graphviz}
\usetheme{Warsaw}
\usecolortheme{whale}

\title{Cellular Base Station Technology}
%\subtitle{Subtitle}
\author{Harald~Welte~laforge@gnumonks.org}
\date[September 2019, CCCB]{September 2019, CCCB Datengarten}
\institute{osmocom.org / sysmocom.de}


\begin{document}

\begin{frame}
\titlepage
\end{frame}


\begin{frame}{Outline}
	\tableofcontents[hideallsubsections]
\end{frame}


\begin{frame}{About the speaker}
\begin{itemize}
	\item Free Software + OSHW developer for more than 20 years
	\item Used to work on the Linux kernel from 1999-2009
	\item By coincidence among the first people enforcing the GNU GPL in court
	\item Since 2009 developing FOSS in cellular communications (Osmocom)
	\item Living and working in Berlin, Germany.
\end{itemize}
\end{frame}


\section{Introduction}

\begin{frame}{What is a Cellular Base station?}
\begin{columns}
	\column{0.38\linewidth}
	\centering
	\includegraphics[width=50mm]{gsm-tower.jpg}
	\column{0.58\linewidth}
	\begin{itemize}
		\item transmits and receives signals from/to mobile phones
		\item converts wireless signals to wired signals
		\item sits between the {\em air interface} and {\em back-haul}
		\item is the most visible part of cellular networks
	\end{itemize}
\end{columns}
\end{frame}

\begin{frame}{The 3GPP Specification point-of-view: 2G}
\includegraphics[width=100mm]{GSM_structures.png}

{\tiny Image credits: tsaitgaist via Wikipedia}
\end{frame}


\begin{frame}{The 3GPP Specification point-of-view: 3G}
\includegraphics[width=100mm]{UMTS_structures.png}

{\tiny Image credits: tsaitgaist via Wikipedia}
\end{frame}

\begin{frame}{The 3GPP Specification point-of-view}
What do we learn from this?
\begin{itemize}
	\pause
	\item The telecom world loves acronyms
	\pause
	\item Specifications deal with functional / logical network elements
	\item Cellular network contains lots of elements
	\item Today, we only want to look at real-world base stations
\end{itemize}
\end{frame}

\begin{frame}{Terminology across cellular generations}
\begin{table}
\begin{tabular}{c | c | c | c | c}
	Generation & Name & Base Station & Back-haul & Next element \\
\hline \hline
	2G & GSM/GPRS & BTS & Abis & BSC \\
	3G & UMTS & NodeB & Iub & RNC \\
	4G & LTE & eNodeB & S1 & MME + SGW \\
	5G & NR & gNodeB & N2 + N3 & AMF + UPF
\end{tabular}
\end{table}
\end{frame}

\begin{frame}{Site vs. Cell}
\begin{description}
	\item[Site] A single tower and associated equipment
	\begin{itemize}
		\item could in theory be omnidirectional
		\item in reality almost always sectorized
		\item classic setup is three-sector site (120 degree per sector)
	\end{itemize}
	\item[Cell] A logical cell in one cellular network generation
	\begin{itemize}
		\item typically illuminated by one (set of) antenna
	\end{itemize}
\end{description}

\begin{itemize}
	\item Result: Single site often has 9 cells
	\item three sectors for each of 2G, 3G and 4G
\end{itemize}
\end{frame}

\begin{frame}{Components of a cellular base station}
\begin{itemize}
	\item Tower/Pole (civil engineering part)
	\item Antenna
	\item Coaxial Cable
	\item Actual Base Station Electronics
	\item Back-haul connection to the rest of the network
	\item Power Supply / Environment (Fans, AC, UPS, ...)
\end{itemize}
\end{frame}


\begin{frame}{Simplified Rx/Tx chain}
\begin{itemize}
	\item Simplified Receiver chain:
\digraph[scale=0.38]{rxsimple}{
	rankdir=LR;
	Antenna -> Duplexer -> RF_Filter -> LNA -> Mixer -> BB_Filter -> ADC -> PHY -> L2_L3
}
	\item Simplified Transmitter chain:
\digraph[scale=0.38]{txsimple}{
	rankdir=RL;
	L2_L3 -> PHY -> DAC -> BB_Filter -> Mixer -> PA -> RF_Filter -> Duplexer -> Antenna;
}
\end{itemize}
	Reality is more complex in many cases (circulator, active predistortion, rx diversity, ...)
\end{frame}


\begin{frame}{Even more Simplified Rx/Tx chain}
\begin{itemize}
	\item Even more simplified Receiver chain:
\digraph[scale=0.45]{rxsimple2}{
	rankdir=LR;
	Antenna -> Mixer [label=RF];
	Mixer -> ADC [label="Analog Baseband"];
	ADC -> PHY [label="Digital Baseband"];
	PHY -> L2_L3 [label="Primitives"];
}
	\item Even more simplified Transmitter chain:
\digraph[scale=0.45]{txsimple2}{
	rankdir=RL;
	L2_L3 -> PHY [label="Primitives"];
	PHY -> DAC [label="Digital Baseband"];
	DAC -> Mixer [label="Analog Baseband"];
	Mixer -> Antenna [label="RF"];
}
\end{itemize}
\end{frame}

\section{Evolution of Cell Sites}

\subsection{Classic Cell Sites}

\begin{frame}{Classic Cell Site (year 2000)}
\begin{columns}
	\column{0.28\linewidth}
	\centering
	\includegraphics[width=37mm]{RBS2206.jpg}
	\column{0.70\linewidth}
	The traditional way of building cell sites:
	\begin{itemize}
		\item (multiple) large racks full of equipment
		\item installed in [air conditioned] shelters
		\item all active electronics on ground level
		\item long lines of coaxial cable up the tower
		\item only passive element (antenna) up tower
		\item half of transmitted power lost in cable
	\end{itemize}
{\tiny Image: Timur V. Voronkov via Wikimedia Commons (CC-BY-SA)}
\end{columns}
\end{frame}

\begin{frame}{Slightly less Classic Cell Site}
\begin{columns}
	\column{0.35\linewidth}
	\centering
	\includegraphics[width=49mm]{nokia_flexi.jpeg}
	\column{0.65\linewidth}
	The fist step of logical evolution:
	\begin{itemize}
		\item equipment becomes smaller (partial rack)
		\item no strict need for large shelter anymore
		\item all active electronics on ground level
		\item long lines of coaxial cable up the tower
		\item only passive element (antenna) up tower
		\item half of transmitted power lost in cable
	\end{itemize}
	Equipment gets smaller, less power hungry and dissipates less heat
{\tiny Image: Peter Schmidt @33dBm}
\end{columns}
\end{frame}

\begin{frame}{Coaxial Cables...}
Why don't we like long coaxial cables
\begin{itemize}
	\item good cabling is 1/2" to 1" in diameter and costs a lot
	\item installation is more like plumbing than cabling
	\item looses lots of energy over length of tower; compensated by
	\begin{itemize}
		\item downlink: more PA; waste of energy; causs more heat dissipation
		\item uplink: tower-mounted amplifier (TMA)
	\end{itemize}
	\item higher frequencies have even more losses (and we went from 900 MHz to 1800 MHz to 2100 MHz to 2600 MHz)
	\item more bands mean more coaxial cables in parallel
\end{itemize}
\end{frame}


\begin{frame}{Towards Remote Radio Heads}
So why not do he logical thing and ...
\begin{itemize}
	\pause
	\item Generate the RF closer to the antenna?
\end{itemize}
Answer:
\begin{itemize}
	\item Requires much more compact radios
	\item Requires passive cooling
	\item Difficult installation (heavy)
	\item Environmental protection (sun, rain, temperature cycles)
	\item Hard to service / replace
\end{itemize}
\end{frame}

\subsection{(Remote) Radio Heads}

\begin{frame}{(Remote) Radio Heads}
Solution: Instead of moving all equipment up the tower,
\begin{itemize}
	\item Move only the Analog parts of the chain up
	\item Transport digital samples up/down the tower
	\item Base Station split in two parts:
	\begin{itemize}
		\item Baseband processing ({\em digital unit})
		\item Radio processing ({\em radio unit})
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Base Station split with Radio Heads}
\begin{itemize}
	\item Incredibly Simplified Receiver chain:
\digraph[scale=0.42]{rxsimple2split}{
	rankdir=LR;
	Antenna -> Mixer [label=RF];
	subgraph cluster_0 {
		label="Radio Head";
		Mixer -> ADC [label="Analog Baseband"];
	}
	ADC -> PHY [label="Digital Baseband Samples"];
	subgraph cluster_1 {
		label="Baseband Unit";
		PHY -> L2_L3 [label="Primitives"];
	}
}
	\item Incredibly Simplified Transmitter chain:
\digraph[scale=0.42]{txsimple2split}{
	rankdir=RL;
	subgraph cluster_0 {
		label="Baseband Unit";
		L2_L3 -> PHY [label="Primitives"];
	}
	subgraph cluster_1 {
		label="Radio Head";
		PHY -> DAC [label="Digital Baseband Samples"];
		DAC -> Mixer [label="Analog Baseband"];
	}
	Mixer -> Antenna [label="RF"];
}
\end{itemize}
\end{frame}

\begin{frame}{Cell Sites with (Remote) Radio Heads}
\includegraphics[width=100mm]{antennas-and-rrus.jpg}
\end{frame}

\begin{frame}{Cell Sites with (Remote) Radio Heads}
\includegraphics[width=100mm]{cellular-tower-2172041_1920.jpg}
\end{frame}

\begin{frame}{Cell Sites with (Remote) Radio Heads}
\includegraphics[width=85mm]{lots-of-radioheads.jpeg}

{\tiny Image: Peter Schmidt @33dBm}
\end{frame}

\begin{frame}{New term: front-haul}
\begin{itemize}
	\item {\em back-haul} is the connection between cell and core
	\item {\em front-haul} is the newly-introduced term for the link between radio head and baseband unit
	\item physical medium
	\begin{itemize}
		\item typically fiber-optic
		\item copper only if radio next to baseband unit
	\end{itemize}
	\item physical layer
	\begin{itemize}
		\item OBSAI (Open Base Station Architecture Initiative)
		\begin{itemize}
			\item Started in 2002 by Hyundai, LG, Nokia, Samsung, ZTE
			\item Mostly obsolete now
		\end{itemize}
		\item CPRI (Common Public Radio Interface)
		\begin{itemize}
			\item Ericsson, Huawei, NEC, Alcatel-Lucent
			\item more adoption particularly in recent years
		\end{itemize}
		\item eCPRI showing up on the horizon
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{from fiber-based front-haul to C-RAN}
As digital baseband samples are transmitted over fiber optics
\begin{itemize}
	\item can cover distances way above height of the tower
	\item single-mode transceivers allow for dozens of kilometers
	\item allows for cell sites without any shelter or rack
	\item leads to some people proclaiming {\em cloud-RAN} or {\em centralized RAN}
	\begin{itemize}
		\item don't distribute baseband compute power in the field
		\item bring all your baseband samples into the cloud
		\item perform CPU-intensive baseband function in data center
	\end{itemize}
	\item bit rates are high. A single LTE 2x2 MIMO carrier at 20MHz needs 2Gbps CPRI bandwidth
	\begin{itemize}
		\item site with 3 sectors and multiple carriers exceeds 10Gbps
	\end{itemize}
	\item latency constraints are biggest limiting factor
\end{itemize}
\end{frame}

%%\section{Antennas}

\begin{frame}{Antennas}
\begin{itemize}
	\item You learned some antenna basics
	\item You think about an omnidirectional dipole
	\item Almost no cellular base station antenna is like that
	\item Complexity of those antennas has grown significantly
\end{itemize}
\end{frame}

\begin{frame}{Vertical polarization vs. X-Pol}
\begin{itemize}
	\item Nominally, cellular signals are emitted in vertical polarization
	\item Industry has moved to two radiators at +45 / -45 degrees polarization
	\item This apparently gives polarization gain, as signals reflected (by buildings) don't arrive in
		vertical polarization
	\item Isolation between radiators typically 20..30dB, allowing use cases like
	\begin{itemize}
		\item operating two transmitters without combiner
		\item operating Rx + Tx without duplexer
		\item diversity reception within one antenna (polarization diversity)
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Single-Band vs. Multiple Bands}
\begin{itemize}
	\item So you rolled out a GSM network in 900 MHz
	\begin{itemize}
		\item then added more GSM on 1800 MHz
		\item then added 3G on 2100 MHz, ...
	\end{itemize}
	\item Do you add one new set of three sector antennas per band?
	\begin{itemize}
		\item space and weight constraints on tower
		\item they may affect each others' radiation pattersn
	\end{itemize}
	\item Industry responds with multi-band antennas
\end{itemize}
\end{frame}

\begin{frame}{Electrical Tilt}
\begin{itemize}
	\item For RF planning, you want to determine where your cell physically ends
	\item Tilting antennas downwards means RF signals emitted eventually will hit the ground
	\item Adjusting the network by climing up the tower and mechanically adjusting tilt is cumbersome
	\item Industry responds with {\em Electrical Tilt}
	\item Rods are controlled by motors leading to {\em Remote Electrical Tilt (RET)}
\end{itemize}
\end{frame}

\begin{frame}{MIMO}
\begin{itemize}
	\item MIMO means Multiple-In / Multiple-Out
	\item uses spatial diversity to establish multiple signals between different antennas
	\item 2x2 MIMO is standard with LTE today
	\item 5G / New Radio specified for massive MIMO (32-64 antennas in base station!)
\end{itemize}
\end{frame}

\begin{frame}{Antennas with many ports}
\includegraphics[width=80mm]{multiport-antenna.jpg}
\end{frame}

\begin{frame}{Where will it end?}
\includegraphics[width=115mm]{kathrein_hepta.png}
\end{frame}

\subsection{Antenna Integrated Radio}

\begin{frame}{Further integration}
\begin{itemize}
	\item the radio head has moved up the tower
	\item coaxial cables are shorter than ever
	\item ... but we have more and more of them
	\item So what do we do?
	\pause
	\item Integrate radio head inside antenna!
\end{itemize}
\end{frame}

\begin{frame}{Antenna Integrated Radio}
\begin{columns}
	\column{0.28\linewidth}
	\centering
	\includegraphics[width=36mm]{RAS.jpg}
	\column{0.70\linewidth}
	\begin{itemize}
		\item Systems like {\em Nokia RAS} / {\em Ericsson AIR}
		\item Radio heads completely integrated with antenna
		\item no coaxial cable at all
		\item CPRI over fiber directly into the antenna
		\item Everything Great? New problems
		\begin{itemize}
			\item enormous weight not suitable everywhere
			\item complicated measurements (field technicians)
		\end{itemize}
	\end{itemize}
\end{columns}
\end{frame}


\section{back-haul, hardware, software}

\subsection{Evolution of cellular back-haul}

\begin{frame}{Classic 2G back-haul}
\begin{itemize}
	\item 2G (GSM) was specified while ISDN was hot
	\item back-haul of GSM BTS is done via E1/T1 (ISDN PRI)
	\item E1 has 30 usable timeslots of 64kBps each
	\begin{itemize}
		\item use one for signaling (A-bis RSL + OML)
		\item use one quarter (16kBps) sub-slot for each voice call
	\end{itemize}
	\item While GSM is still deployed today, 3GPP never specified any other transport
	\item Every vendor came up with their own proprietary kludge on how to carry Abis over IP
\end{itemize}
\end{frame}

\begin{frame}{Classic 3G back-haul}
\begin{itemize}
	\item 3G (UMTS) was specified when ATM was the next hot thing
	\item back-haul of NodeB is done via ATM
	\item in reality, often Inverse ATM Multiplex (ATM over 4xE1 ISDN)
	\item 3GPP at least later adapted specs for IP based transport
	\begin{itemize}
		\item Every 20ms voice codec frame split over three different UDP packets. yay!
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{4G back-haul}
\begin{itemize}
	\item 4G is first 3GPP cellular technology transported over IP from day one
	\item Therefore, no exotic physical layers
	\item Ethernet in most cases
	\item Problem: Where do we get clock from?
	\begin{itemize}
		\item ISDN/E1/ATM always provided clock reference
		\item Ethernet doesn't provide clock reference
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{IP-based back-haul and base station clocking}
\begin{itemize}
	\item cellular base stations need super stable clock reference
	\begin{itemize}
		\item requirement of 30 ppb is almost 1000 times more accurate than crystal
		\item even ovenized crystals (OCXOs) not long-term stable enough
	\end{itemize}
	\item in the post-ISDN/PDH/SDH days, pick your poison:
	\begin{itemize}
		\item go for a GPS-DO and create a single point of failure, or
		\item use Synchronous Ethernet and loose the advantage of low-cost COTS Ethernet Switches, or
		\item use IEEE PTP and hope your switches don't introduce too much jitter, or
		\item let your base stations hammer your NTP server and pray
	\end{itemize}
\end{itemize}
\end{frame}

\subsection{Base Station Electronics}

\begin{frame}{Base Station Electronics: Baseband}
\begin{itemize}
	\item Typically some multi-core DSP
	\begin{itemize}
		\item e.g. TI Keystone2 (eight 64bit 1.2GHz DSPs)
		\item built-in coprocessors (FFT, crypto, Turbo Decoder, Viterbi)
		\item built-in CPRI/OBSAI Controller
		\item four ARM Cortex A-15 for L2/L3 processing
	\end{itemize}
	\item Often also FPGAs + vendor-specific ASICs
	\begin{itemize}
		\item Ericsson big on ASICs
		\item proprietary ASICs/SoCs with 10.5 billion transistors
		\item that's comparable to Apple A12X / Huawei Kirin 990!
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Base Station Electronics: Radiohead}
\begin{itemize}
	\item Some RFIC (typically ADI)
	\begin{itemize}
		\item ADC + DAC
		\item up/downconversion (mixer)
		\item on-chip filters
	\end{itemize}
	\item Power Amplifier
	\begin{itemize}
		\item typically 2 stages of drivers + final PA
	\end{itemize}
	\item Circulator
	\begin{itemize}
		\item protect PA from power reflected back from antenna
	\end{itemize}
	\item Cavity Duplexer
	\item [Digital] [Adaptive] Pre-distortion
	\begin{itemize}
		\item Ensure Linear PA even for high-PAPR signals
	\end{itemize}
\end{itemize}
\end{frame}


\subsection{Base Station Software}

\begin{frame}{Base Station Software}
\begin{itemize}
	\item Don't expect too many familiar things here
	\item decades of proprietary development by large corporations
	\item Enea OSE (Operating System Embedded) popular with Ericsson + Nokia
	\begin{itemize}
		\item proprietary microkernel with custom-everything including filesystems
	\end{itemize}
	\item vxworks found in some equipment like Huawei radioheads
	\item Linux found mostly only in small cells, inheriting software from femtocells
\end{itemize}
\end{frame}

\begin{frame}{Further Reading}
\begin{itemize}
	\item \url{http://cpri.info/}
	\item FlexiWCDMA teardown: \url{https://www.youtube.com/watch?v=d5xT4p9FXIw}
	\item Ericsson RBS6000 teardown: \url{https://www.youtube.com/watch?v=qO127zY3voE}
\end{itemize}
\end{frame}

\begin{frame}{Thanks}
Thanks for your attention.

	You have a General Public License to ask questions now :)
\end{frame}

\end{document}
