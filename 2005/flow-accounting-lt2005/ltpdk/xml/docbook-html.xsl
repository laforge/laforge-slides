<?xml version="1.0" encoding="ISO-8859-1"?> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html" doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN" doctype-system="http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd" encoding="ISO-8859-1" indent="yes"/> 

<xsl:param name="lang">de</xsl:param>
<xsl:param name="i18n.xml" select="document('i18n.xml')"/>

<!-- DocBook Variables -->

<xsl:variable name="text.toc" select="($i18n.xml//i18n[@name='text.toc'])[1]/@value"/>
<xsl:variable name="text.lot" select="($i18n.xml//i18n[@name='text.lot'])[1]/@value"/>
<xsl:variable name="text.warning" select="($i18n.xml//i18n[@name='text.warning'])[1]/@value"/>
<xsl:variable name="text.caution" select="($i18n.xml//i18n[@name='text.caution'])[1]/@value"/>
<xsl:variable name="text.important" select="($i18n.xml//i18n[@name='text.important'])[1]/@value"/>
<xsl:variable name="text.tip" select="($i18n.xml//i18n[@name='text.tip'])[1]/@value"/>
<xsl:variable name="text.note" select="($i18n.xml//i18n[@name='text.note'])[1]/@value"/>

<!-- ****************************************************************** -->
<!-- * DocBook Templates                                              * -->
<!-- ****************************************************************** -->

<xsl:template match="/">
	<xsl:apply-templates/>
</xsl:template>

<!-- ****************************************************************** -->
<!-- * Template book                                                  * -->
<!-- ****************************************************************** -->
<xsl:template match="article|book">
	<xsl:apply-templates select="titlepage"/>
	<xsl:apply-templates select="toc" mode="create.toc"/>
	<!--<xsl:apply-templates select="lot"/>-->
	<!--<xsl:apply-templates select="dedication"/>-->
	<!--<xsl:apply-templates select="preface"/>-->
	<xsl:apply-templates/>
	<!--<xsl:apply-templates select="appendix"/>-->
	<!--<xsl:apply-templates select="bibliography"/>-->
	<!--<xsl:apply-templates select="glossary"/>-->
	<!--<xsl:apply-templates select="index"/>-->
	<!--<xsl:apply-templates select="colophon"/>-->
</xsl:template>

<!-- ****************************************************************** -->
<!-- * Templates for logical divisions                                * -->
<!-- ****************************************************************** -->
<xsl:template match="chapter|section|sect1|sect2|sect3|sect4|sect5">
	<div class="{name()}">
		<xsl:if test="name()='chapter'">
			<h2 class="{name()}">
				<xsl:apply-templates select="title" mode="create.chapter.title"/>
			</h2>
		</xsl:if>
		<xsl:if test="not(name()='chapter')">
			<h3 class="{name()}">
				<xsl:apply-templates select="title" mode="create.title"/>
			</h3>
		</xsl:if>
		<xsl:if test="ancestor::*/toc or ./toc">
			<xsl:if test="section|reference|sect1">
				<table class="toc">
					<tr class="toc">
						<td class="toc">
							<b class="toc">
								<font class="toc"><xsl:value-of select="$text.toc"/></font>
							</b>
							<xsl:apply-templates select="section|reference|sect1" mode="create.toc"/>
						</td>
					</tr>
				</table>
			</xsl:if>
		</xsl:if>
		<!--<xsl:apply-templates select="abstract"/>-->
		<xsl:choose>
			<xsl:when test="name()='chapter'">
				<xsl:apply-templates/>
			</xsl:when>
			<xsl:when test="name()='section'">
				<xsl:apply-templates/>
			</xsl:when>
			<xsl:when test="name()='sect1'">
				<xsl:apply-templates/>
			</xsl:when>
			<xsl:when test="name()='sect2'">
				<xsl:apply-templates/>
			</xsl:when>
			<xsl:when test="name()='sect3'">
				<xsl:apply-templates/>
			</xsl:when>
			<xsl:when test="name()='sect4'">
				<xsl:apply-templates/>
			</xsl:when>
			<xsl:when test="name()='sect5'">
				<xsl:apply-templates/>
			</xsl:when>
		</xsl:choose>
	</div>
</xsl:template>

<xsl:template match="title" mode="create.title">
	<xsl:if test="../@id">
		<a>
			<xsl:attribute name="name">
				<xsl:value-of select="../@id"/>
			</xsl:attribute>
			<xsl:apply-templates/>
		</a>
	</xsl:if>
	<xsl:if test="not(../@id)">
		<xsl:apply-templates/>
	</xsl:if>
</xsl:template>

<xsl:template match="title" mode="create.chapter.title">
	<xsl:variable name="title"><xsl:value-of select="."/></xsl:variable>
	<xsl:for-each select="../../chapter">
		<xsl:if test="title=$title">
			<xsl:number format="1. "/>
		</xsl:if>
	</xsl:for-each>
	<xsl:if test="../@id">
		<a>
			<xsl:attribute name="name">
				<xsl:value-of select="../@id"/>
			</xsl:attribute>
			<xsl:apply-templates/>
		</a>
	</xsl:if>
	<xsl:if test="not(../@id)">
		<xsl:apply-templates/>
	</xsl:if>
</xsl:template>

<!-- ****************************************************************** -->
<!-- * Templates for creating the TOC                                 * -->
<!-- ****************************************************************** -->
<xsl:template name="toc" match="toc" mode="create.toc">
	<xsl:if test="../chapter[@id]|../section[@id]|../reference[@id]|../sect1[@id]">
		<div class="toc">
			<b class="toc">
				<font class="toc"><xsl:value-of select="$text.toc"/></font>
			</b>
			<dl>
				<xsl:apply-templates select="../chapter[@id]|../section[@id]|../reference[@id]|../sect1[@id]" mode="create.toc"/>
			</dl>
		</div>
	</xsl:if>
</xsl:template>

<xsl:template match="toc" mode="create.chapter.toc">
	<xsl:apply-templates select="../section|../reference|../sect1" mode="create.toc"/>
</xsl:template>

<xsl:template match="title" mode="create.toc">
	<a>
		<xsl:attribute name="class">content</xsl:attribute>
		<xsl:attribute name="href">
		  <xsl:value-of select="concat('#',../@id)"/>
		</xsl:attribute>
		<xsl:value-of select="."/>
	</a>
</xsl:template>

<xsl:template match="chapter" mode="create.toc">
	<dt>
		<xsl:number format="1. "/>
		<xsl:apply-templates select="title" mode="create.toc"/>
		<xsl:if test="section[@id]|sect1[@id]">
			<dd>
				<dl>
					<xsl:apply-templates select="sect1|section" mode="create.toc"/>
				</dl>
			</dd>
		</xsl:if>
	</dt>
</xsl:template>

<xsl:template match="section|sect1|sect2|sect3|sect4" mode="create.toc">
	<xsl:variable name="next">
		<xsl:choose>
			<xsl:when test="name()='section'">section</xsl:when>
			<xsl:when test="name()='sect1'">sect2</xsl:when>
			<xsl:when test="name()='sect2'">sect3</xsl:when>
			<xsl:when test="name()='sect3'">sect4</xsl:when>
		</xsl:choose>
	</xsl:variable>
	<dt>
		<xsl:apply-templates select="title" mode="create.toc"/>
		<xsl:if test="concat($next,'[@id]')">
			<dd>
				<dl>
					<xsl:choose>
						<xsl:when test="name()='section'">
							<xsl:apply-templates select="section[@id]" mode="create.toc"/>
						</xsl:when>
						<xsl:when test="name()='sect2'">
							<xsl:apply-templates select="sect2[@id]" mode="create.toc"/>
						</xsl:when>
						<xsl:when test="name()='sect3'">
							<xsl:apply-templates select="sect3[@id]" mode="create.toc"/>
						</xsl:when>
						<xsl:when test="name()='sect4'">
							<xsl:apply-templates select="sect4[@id]" mode="create.toc"/>
						</xsl:when>
					</xsl:choose>
				</dl>
			</dd>
		</xsl:if>
	</dt>
</xsl:template>

<!-- ****************************************************************** -->
<!-- * Templates for Para                                             * -->
<!-- ****************************************************************** -->
<xsl:template name="para" match="para|simpara">
	<p>
		<xsl:call-template name="add.class.by.id"/>
		<xsl:apply-templates/>
	</p>
</xsl:template>

<xsl:template match="formalpara">
	<xsl:if test="title">
		<h3 class="section"><xsl:apply-templates select="title" mode="create.title"/></h3>
	</xsl:if>
	<p><xsl:apply-templates/></p>
</xsl:template>

<xsl:template match="para|simpara" mode="create.toc">
	<xsl:call-template name="add.class.by.id"/>
	<xsl:apply-templates/>
</xsl:template>

<!-- ****************************************************************** -->
<!-- * Template for emphasis et al.                                   * -->
<!-- ****************************************************************** -->
<xsl:template match="emphasis">
	<em class="{name()}"><xsl:apply-templates/></em>
</xsl:template>

<xsl:template match="subscript">
	<sub class="{name()}"><xsl:apply-templates/></sub>
</xsl:template>

<xsl:template match="superscript">
	<sup class="{name()}"><xsl:apply-templates/></sup>
</xsl:template>

<xsl:template match="acronym">
	<acronym class="{name()}"><xsl:apply-templates/></acronym>
</xsl:template>

<xsl:template match="abbrev">
	<abbr class="{name()}"><xsl:apply-templates/></abbr>
</xsl:template>

<xsl:template match="wordasword">
	<em class="{name()}"><xsl:apply-templates/></em>
</xsl:template>

<xsl:template match="quote">
	<xsl:text>"</xsl:text>
	<xsl:apply-templates/>
	<xsl:text>"</xsl:text>
</xsl:template>

<xsl:template match="filename">
	<em class="{name()}"><xsl:apply-templates/></em>
</xsl:template>

<xsl:template match="accel">
	<u class="{name()}"><xsl:apply-templates/></u>
</xsl:template>

<xsl:template match="guimenu">
	<em class="{name()}"><xsl:apply-templates/></em>
</xsl:template>

<xsl:template match="guimenuitem">
	<em class="{name()}"><xsl:apply-templates/></em>
</xsl:template>

<xsl:template match="action">
	<em class="{name()}"><xsl:apply-templates/></em>
</xsl:template>

<!-- ****************************************************************** -->
<!-- * Template for creating Blockquotes                              * -->
<!-- ****************************************************************** -->
<xsl:template match="blockquote">
	<div class="{name()}">
		<blockquote>
			<xsl:apply-templates/>
		</blockquote>
		<xsl:apply-templates select="attribution" mode="create.attribution"/>
	</div>
</xsl:template>

<xsl:template match="attribution" mode="create.attribution">
	<div class="{name()}" align="right">--<xsl:apply-templates/></div>
</xsl:template>

<xsl:template match="attribution"/>

<!-- ***************************************************************** -->
<!-- * Template for creating Lists                                   * -->
<!-- ***************************************************************** -->
<xsl:template match="itemizedlist">
	<div class="{name()}">
		<ul class="{name()}">
			<xsl:apply-templates/>
		</ul>
	</div>
</xsl:template>

<xsl:template match="orderedlist">
	<div class="{name()}">
		<ol class="{name()}">
			<xsl:variable name="numeration">
				<xsl:choose>
					<xsl:when test="@numeration='loweralpha'">a</xsl:when>
					<xsl:when test="@numeration='lowerroman'">i</xsl:when>
					<xsl:when test="@numeration='upperalpha'">A</xsl:when>
					<xsl:when test="@numeration='apperroman'">i</xsl:when>
					<xsl:otherwise></xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:if test="not($numeration='')">
					<xsl:attribute name="type">
						<xsl:value-of select="$numeration"/>
					</xsl:attribute>
			</xsl:if>
						<xsl:message>Numeration: <xsl:call-template name="orderedlist.find.numeration"/></xsl:message>
			<xsl:if test="@continuation">
				<xsl:attribute name="start">
					<xsl:call-template name="orderedlist.find.numeration"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates/>
		</ol>
	</div>
</xsl:template>

<xsl:template name="orderedlist.find.numeration">
	<xsl:param name="count">1</xsl:param>
	<xsl:choose>
		<xsl:when test="@continuation='continues'">
			<xsl:for-each select="preceding::orderedlist[1]">
				<!--<xsl:if test="not(name()='orderedlist')">-->
					<xsl:call-template name="orderedlist.find.numeration">
						<xsl:with-param name="count">
							<xsl:value-of select="count(listitem)+$count"/>
						</xsl:with-param>
					</xsl:call-template>
				<!--</xsl:if>-->
			</xsl:for-each>
		</xsl:when>
		<xsl:otherwise><xsl:value-of select="$count"/></xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="listitem">
	<li class="{name()}">
		<xsl:apply-templates/>
	</li>
</xsl:template>

<xsl:template match="listitem" mode="varlistentry">
		<dd class="{name()}">
			<xsl:apply-templates/>
		</dd>
   </xsl:template>

   <xsl:template match="variablelist">
		<xsl:if test="title">
			<h3 class="{name()}"><xsl:apply-templates select="title" mode="create.title"/></h3>
		</xsl:if>
		<div class="{name()}">
			<dl class="{name()}">
				<xsl:apply-templates/>
			</dl>
		</div>
   </xsl:template>

   <xsl:template match="varlistentry">
		<dt class="{name()}">
			<xsl:apply-templates select="term"/>
			<xsl:apply-templates select="listitem" mode="varlistentry"/>
		</dt>
   </xsl:template>

   <xsl:template match="term">
		<xsl:if test="not(position()=1)">
			<xsl:text>, </xsl:text>
		</xsl:if>
		<xsl:apply-templates/>
   </xsl:template>

	<!-- ***************************************************************** -->
	<!-- * Templates for FAQs                                            * -->
	<!-- ***************************************************************** -->
   <xsl:template match="qandaset">
		<!--
		<table border="1" class="toc" width="100%">
			<tr>
				<td>
		-->
					<xsl:apply-templates select="qandadiv|qandaentry|question" mode="create.toc"/>
		<!--
				</td>
			</tr>
		</table>
		-->
		<xsl:apply-templates/>
   </xsl:template>
	
   <xsl:template match="qandadiv">
		<a id="{generate-id()}"/>
		<h3 class="{title}">
			<xsl:number format="1. " level = "multiple" />
			<xsl:apply-templates select="title" mode="create.title"/>
		</h3>
		<!--
		<table border="1" class="toc">
			<tr>
				<td>
		-->
					<xsl:apply-templates select="qandadiv|qandaentry|question" mode="create.toc"/>
		<!--
				</td>
			</tr>
		</table>
		-->
		<xsl:if test="ancestor::qandadiv[1]">
			<xsl:apply-templates/>
		</xsl:if>
		<xsl:if test="not(ancestor::qandadiv)">
			<table class="{name}" border="0" margin="0" marginsep="0">
				<xsl:apply-templates/>
			</table>
		</xsl:if>
   </xsl:template>
	
   <xsl:template match="qandadiv" mode="create.toc">
		<dt>
			<a>
				<xsl:attribute name="class">content</xsl:attribute>
				<xsl:attribute name="href">
					<xsl:value-of select="concat('#', generate-id(parent::*))"/>
				</xsl:attribute>
				<xsl:number format="1. " count="//qandadiv|//qandaentry" level = "multiple" />
				<xsl:apply-templates select="title" mode="create.title"/>
			</a>
			<dd>
				<dl>
					<xsl:apply-templates select="qandadiv|qandaentry|question" mode="create.toc"/>
				</dl>
			</dd>
		</dt>
   </xsl:template>
	
   <xsl:template match="qandaentry">
		<a id="{generate-id()}"/>
		<tr class="question">
			<td><b><xsl:number format="1.1." count="//qandadiv|//qandaentry" level = "multiple" /></b></td>
			<td align="left" valign="top"><xsl:apply-templates select="question"/></td>
		</tr>
		<tr class="answer">
			<td/>
			<td align="left" valign="top"><xsl:apply-templates select="answer"/></td>
		</tr>
   </xsl:template>
	
   <xsl:template match="qandaentry" mode="create.toc">
		<dt>
			<a>
				<xsl:attribute name="class">content</xsl:attribute>
				<xsl:attribute name="href">
					<xsl:value-of select="concat('#', generate-id())"/>
				</xsl:attribute>
				<xsl:number format="1.1. " count="//qandadiv|//qandaentry" level = "multiple" />
					<xsl:apply-templates select="question[1]" mode="create.toc"/>
			</a>
		</dt>
   </xsl:template>
	
   <xsl:template match="question">
		<xsl:apply-templates/>
   </xsl:template>
	
   <xsl:template match="question" mode="create.toc" >
		<xsl:apply-templates mode="create.toc"/>
   </xsl:template>
	
   <xsl:template match="answer">
		<xsl:apply-templates/>
   </xsl:template>
	
	<!-- ***************************************************************** -->
	<!-- * Templates for Program Listings                                * -->
	<!-- ***************************************************************** -->
   <xsl:template match="programlisting|screen">
		<div class="{name()}">
			<table class="{name()}">
				<tr class="{name()}">
					<td class="{name()}">
						<pre class="{name()}"><xsl:attribute name="width"><xsl:value-of select="@width"/></xsl:attribute><xsl:apply-templates/></pre>
					</td>
				</tr>
			</table>
		</div>
   </xsl:template>

   <xsl:template match="computeroutput|prompt">
		<pre class="name()">
			<code class="name()">
				<xsl:apply-templates/>
			</code>
		</pre>
   </xsl:template>

	<!-- ***************************************************************** -->
   <!-- * Templates for creating Adresses                               * -->
	<!-- ***************************************************************** -->
	<xsl:template match="address">
		<table class="address" border="0">
			<tr class="address">
				<td class="address">
					<xsl:apply-templates/>
				</td>
			</tr>
		</table>
   </xsl:template>

	<xsl:template name="personname" match="personname">
		<br>
			<xsl:call-template name="add.class.by.id"/>
		</br>
		<xsl:apply-templates select="honorific|text()"/>
		<xsl:if test="firstname"><xsl:text>&#160;</xsl:text></xsl:if>
		<xsl:apply-templates select="firstname"/>
		<xsl:if test="othername"><xsl:text>&#160;</xsl:text></xsl:if>
	   <xsl:apply-templates select="othername"/>
		<xsl:if test="surname"><xsl:text>&#160;</xsl:text></xsl:if>
	   <xsl:apply-templates select="surname"/>
		<xsl:if test="lineage"><xsl:text>&#160;</xsl:text></xsl:if>
	   <xsl:apply-templates select="lineage"/>
   </xsl:template>

	<xsl:template match="honorific|firstname|othername|surname|lineage">
		<xsl:apply-templates/>
   </xsl:template>

	<xsl:template match="street">
		<br>
			<xsl:call-template name="add.class.by.id"/>
		</br>
		<xsl:apply-templates/>
   </xsl:template>

	<xsl:template match="country">
		<br>
			<xsl:call-template name="add.class.by.id"/>
		</br>
		<xsl:apply-templates/>
   </xsl:template>

	<xsl:template match="phone">
		<br>
			<xsl:call-template name="add.class.by.id"/>
		</br>
		<xsl:apply-templates/>
   </xsl:template>

	<xsl:template match="fax">
		<br>
			<xsl:call-template name="add.class.by.id"/>
		</br>
		<xsl:apply-templates/>
   </xsl:template>

	<xsl:template match="email">
		<br>
			<xsl:call-template name="add.class.by.id"/>
		</br>
		<xsl:apply-templates/>
   </xsl:template>

	<xsl:template match="city|state|postcode">
		<xsl:if test="(ancestor::address/descendant::city|ancestor::address/descendant::state|ancestor::address/descendant::postcode)[1] = .">
			<br>
				<xsl:call-template name="add.class.by.id"/>
			</br>
		</xsl:if>
		<xsl:apply-templates/>
		<xsl:choose>
			<xsl:when test="(ancestor::address/descendant::city|ancestor::address/descendant::state|ancestor::address/descendant::postcode)[last()] = .">
			</xsl:when>
			<xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
		</xsl:choose>
   </xsl:template>

	<!-- ****************************************************************** -->
   <!-- * Templates for Function Stuff                                   * -->
	<!-- ****************************************************************** -->
	<xsl:template match="funcsynopsis">
		<div class="{name()}">
			<table class="{name()}">
				<tr class="{name()}">
					<pre class="{name()}">
						<xsl:apply-templates select="funcsynopsisinfo"/>
						<xsl:apply-templates select="funcprototype"/>
					</pre>
				</tr>
			</table>
		</div>
   </xsl:template>

   <xsl:template match="funcsynopsisinfo">
		<div class="{name()}">
			<xsl:apply-templates/>
		</div>
   </xsl:template>

   <xsl:template match="funcprototype">
		<code class="{name()}">
			<xsl:apply-templates select="funcdef"/>
			<xsl:if test="paramdef">
				<xsl:text>(</xsl:text>
					<xsl:apply-templates select="paramdef/parameter" mode="create.function.signature"/>
				<xsl:text>);</xsl:text>
			</xsl:if>
			<xsl:apply-templates select="void|varargs|paramdef"/>
		</code>
   </xsl:template>

	<xsl:template match="parameter" mode="create.function.signature">
		<xsl:if test="not(position()=1)">
			<xsl:text>, </xsl:text>
		</xsl:if>
		<i class="{name()}">
			<xsl:apply-templates/>
		</i>
   </xsl:template>

	<xsl:template match="paramdef">
		<br/>
		<xsl:apply-templates select="text()|funcparams|parameter|type"/>
   </xsl:template>

	<xsl:template match="funcparams">
		<xsl:text>(</xsl:text>
			<i class="{name()}">
				<xsl:apply-templates/>
			</i>
		<xsl:text>)</xsl:text>
   </xsl:template>

	<xsl:template match="parameter">
		<i class="{name()}">
			<xsl:apply-templates/>
		</i>
		<xsl:text>; </xsl:text>
   </xsl:template>

	<xsl:template match="funcdef">
		<xsl:value-of select="normalize-space(text())"/>
		<xsl:apply-templates select="normalize-space(type)"/>
		<xsl:text>&#160;</xsl:text>
		<xsl:apply-templates select="function"/>
   </xsl:template>

	<xsl:template match="void">
		<xsl:text>(void);</xsl:text>
	</xsl:template>

	<xsl:template match="varargs">
		<xsl:text>(...);</xsl:text>
	</xsl:template>

	<xsl:template match="type">
		<xsl:apply-templates/>
	</xsl:template>

	<xsl:template match="function">
		<b class="{name()}">
			<xsl:apply-templates/>
		</b>
	</xsl:template>

	<xsl:template match="varname|constant|symbol|returnvalue">
		<xsl:apply-templates/>
	</xsl:template>

	<!-- ****************************************************************** -->
   <!-- * Templates for Copyright and Trademarks                         * -->
	<!-- ****************************************************************** -->
   <xsl:template match="copyright" mode="create.title">
      <xsl:text>Copyright � </xsl:text>
      <xsl:value-of select="year"/>
      <xsl:text> </xsl:text>
      <xsl:value-of select="holder"/>
   </xsl:template>

   <xsl:template match="copyright"/>

   <xsl:template match="trademark|productname">
		<xsl:choose>
			<xsl:when test="@class='copyright'">
				<xsl:text>� </xsl:text>
				<xsl:apply-templates/>
			</xsl:when>
			<xsl:when test="@class='registered' or (not(@class) and name()='productname')">
				<xsl:apply-templates/>
				<xsl:text>�</xsl:text>
			</xsl:when>
			<xsl:when test="@class='service'">
			</xsl:when>
			<xsl:when test="@class='trade' or (not(@class) and name()='trademark')">
				<xsl:apply-templates/>
				<sup>TM</sup>
			</xsl:when>
		</xsl:choose>
   </xsl:template>

	<!-- ****************************************************************** -->
   <!-- * Templates for Graphics                                         * -->
	<!-- ****************************************************************** -->
   <xsl:template match="mediaobject|inlinemediaobject">
		<xsl:apply-templates/>
   </xsl:template>

   <xsl:template match="screenshot">
		<xsl:apply-templates/>
   </xsl:template>

	<!-- ****************************************************************** -->
   <!-- * Templates for Linking                                          * -->
	<!-- ****************************************************************** -->
   <xsl:template name="link" match="link">
		<a class="link">
			<xsl:attribute name="href">
				<xsl:variable name="linkend"><xsl:value-of select="@linkend"/></xsl:variable>
				<xsl:choose>
					<xsl:when test="//*[@id=$linkend and position()=1]">
						<xsl:value-of select="concat('#',$linkend)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$linkend"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:if test=".=''">
				<xsl:variable name="endterm"><xsl:value-of select="@endterm"/></xsl:variable>
				<xsl:apply-templates select="//*[@id=$endterm and position()=1]" mode="create.title"/>
			</xsl:if>
			<xsl:apply-templates/>
		</a>
   </xsl:template>

   <xsl:template match="ulink">
		<a class="ulink">
			<xsl:attribute name="href">
				<xsl:value-of select="@url"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test=".=''">
					<xsl:value-of select="@url"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates/>
				</xsl:otherwise>
			</xsl:choose>
		</a>
   </xsl:template>

	<!-- ****************************************************************** -->
   <!-- * Templates for Events                                           * -->
	<!-- ****************************************************************** -->
   <xsl:template match="caution|note|important|tip|warning">
		<p>
			<div>
				<xsl:attribute name="class">
					<xsl:value-of select="name()"/>
				</xsl:attribute>
				<table>
					<xsl:attribute name="class">
						<xsl:value-of select="name()"/>
					</xsl:attribute>
					<tr>
						<xsl:attribute name="class">
							<xsl:value-of select="name()"/>
						</xsl:attribute>
						<td valign="top" align="center">
							<b>
								<xsl:attribute name="class">
									<xsl:value-of select="name()"/>
								</xsl:attribute>
								<xsl:choose>
									<xsl:when test="name()='caution'">
										<xsl:value-of select="$text.caution"/>
									</xsl:when>
									<xsl:when test="name()='note'">
										<xsl:value-of select="$text.note"/>
									</xsl:when>
									<xsl:when test="name()='important'">
										<xsl:value-of select="$text.important"/>
									</xsl:when>
									<xsl:when test="name()='tip'">
										<xsl:value-of select="$text.tip"/>
									</xsl:when>
									<xsl:when test="name()='warning'">
										<xsl:value-of select="$text.warning"/>
									</xsl:when>
								</xsl:choose>
							</b>
						</td>
						<td valign="top" align="left">
							<xsl:attribute name="class">
								<xsl:value-of select="name()"/>
							</xsl:attribute>
							<xsl:apply-templates/>
						</td>
					</tr>
				</table>
			</div>
		</p>
   </xsl:template>
	
	<!-- ****************************************************************** -->
   <!-- * Templates for Graphics                                         * -->
	<!-- ****************************************************************** -->

	<xsl:template match="mediaobject">
		<xsl:apply-templates select="objectinfo[1]"/>
		<xsl:apply-templates select="videoobject|audioobject|imageobject|textobject"/>
		<xsl:apply-templates select="caption[1]"/>
	</xsl:template>

	<xsl:template match="imageobject">
		<xsl:apply-templates select="objectinfo[1]"/>
		<xsl:apply-templates select="imagedata[1]"/>
	</xsl:template>
	
	<xsl:template match="imagedata">
		<img border="0">
			<xsl:attribute name="src">
				<xsl:value-of select="@fileref"/>
			</xsl:attribute>
			<xsl:if test="@width">
				<xsl:attribute name="width">
					<xsl:value-of select="@width"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="@height">
				<xsl:attribute name="height">
					<xsl:value-of select="@height"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="@scale">
				<xsl:attribute name="scale">
					<xsl:value-of select="@scale"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="@align">
				<xsl:attribute name="align">
					<xsl:value-of select="@align"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="@valign">
				<xsl:attribute name="valign">
					<xsl:value-of select="@valign"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="../../textobject/textdata">
				<xsl:attribute name="alt">
					<xsl:value-of select="../../textobject/textdata"/>
				</xsl:attribute>
			</xsl:if>
		</img>
	</xsl:template>
	
	<xsl:template match="textobject">
		<xsl:apply-templates select="objectinfo[1]"/>
		<xsl:apply-templates select="phrase|textdata|itemizedlist|orderedlist"/>
	</xsl:template>
	
	<xsl:template match="textdata">
		<xsl:apply-templates select="text()"/>
	</xsl:template>
	
	<!-- ****************************************************************** -->
   <!-- * Templates for Tables                                           * -->
	<!-- ****************************************************************** -->
	
	<xsl:template match="table">
		<xsl:if test="title">
			<h3 class="{name()}"><xsl:apply-templates select="title" mode="create.title"/></h3>
		</xsl:if>
		<xsl:apply-templates select="textobject|mediaobject|tgroup"/>
	</xsl:template>

	<xsl:template match="tgroup">
		<table>
			<xsl:attribute name="class">
				<xsl:choose>
					<xsl:when test="ancestor::table/@label">
						<xsl:value-of select="ancestor::table/@label"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>table</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="border">
				<xsl:choose>
					<xsl:when test="ancestor::table[@frame = 'none']">
						<xsl:text>0</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>1</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:apply-templates select="thead"/>
			<xsl:apply-templates select="tbody"/>
			<xsl:apply-templates select="tfoot"/>
		</table>
	</xsl:template>

	<xsl:template match="tbody|thead|tfoot">
		<xsl:param name="table.colspec"/>
		<xsl:param name="table.spanspec"/>
		<xsl:apply-templates select="row"/>
	</xsl:template>

	<xsl:template match="row">
		<xsl:param name="table.colspec"/>
		<xsl:param name="table.spanspec"/>
		<tr>
			<xsl:attribute name="class">
				<xsl:choose>
					<xsl:when test="ancestor::table/@label">
						<xsl:value-of select="ancestor::table/@label"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>row</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:apply-templates select="entry"/>
		</tr>
	</xsl:template>

	<xsl:template match="entry">
		<td>
			<xsl:attribute name="class">
				<xsl:choose>
					<xsl:when test="ancestor::table/@label">
						<xsl:value-of select="ancestor::table/@label"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>entry</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:if test="@namest">
				<xsl:variable name="colname.namest" select="@namest"/>
				<xsl:variable name="colname.nameend" select="@nameend"/>
				<xsl:variable name="namest">
					<xsl:choose>
						<xsl:when test="ancestor::tgroup/colspec[@colname = $colname.namest]/@colnum">
							<xsl:value-of select="ancestor::tgroup/colspec[@colname = $colname.namest]/@colnum"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:for-each select="ancestor::tgroup/colspec">
								<xsl:if test="@colname = $colname.namest">
									<xsl:value-of select="position()"/>
								</xsl:if>
							</xsl:for-each>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="nameend">
					<xsl:choose>
						<xsl:when test="ancestor::tgroup/colspec[@colname = $colname.nameend]/@colnum">
							<xsl:value-of select="ancestor::tgroup/colspec[@colname = $colname.nameend]/@colnum"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:for-each select="ancestor::tgroup/colspec">
								<xsl:if test="@colname = $colname.nameend">
									<xsl:value-of select="position()"/>
								</xsl:if>
							</xsl:for-each>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:attribute name="colspan">
					<xsl:value-of select="$nameend - $namest +1"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="@morerows">
				<xsl:attribute name="rowspan">
					<xsl:value-of select="1 + @morerows"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="@align">
				<xsl:attribute name="align">
					<xsl:value-of select="@align"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="@valign">
				<xsl:attribute name="valign">
					<xsl:value-of select="@valign"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates/>
		</td>
	</xsl:template>

	<!-- ***************************************************************** -->
   <!-- * Templates for Tags that have to be ignored                    * -->
	<!-- ***************************************************************** -->
   <xsl:template match="beginpage"/>
   <xsl:template match="remark"/>
   <xsl:template match="comment"/>
   <xsl:template match="title"/>
   <xsl:template match="subtitle"/>
   <xsl:template match="titleabbrev"/>
   <xsl:template match="appendixinfo"/>
   <xsl:template match="articleinfo"/>
   <xsl:template match="bibliographyinfo"/>
   <xsl:template match="blockinfo"/>
   <xsl:template match="bookinfo"/>
   <xsl:template match="chapterinfo"/>
   <xsl:template match="classsynopsisinfo"/>
   <xsl:template match="glossaryinfo"/>
   <xsl:template match="indexinfo"/>
   <xsl:template match="msginfo"/>
   <xsl:template match="objectinfo"/>
   <xsl:template match="partinfo"/>
   <xsl:template match="prefaceinfo"/>
   <xsl:template match="refentryinfo"/>
   <xsl:template match="referenceinfo"/>
   <xsl:template match="refmiscinfo"/>
   <xsl:template match="refsect1info"/>
   <xsl:template match="refsect2info"/>
   <xsl:template match="refsect3info"/>
   <xsl:template match="refsectioninfo"/>
   <xsl:template match="refsynopsisdivinfo"/>
   <xsl:template match="relesaseinfo"/>
   <xsl:template match="screeninfo"/>
   <xsl:template match="sect1info"/>
   <xsl:template match="sect2info"/>
   <xsl:template match="sect3info"/>
   <xsl:template match="sect4info"/>
   <xsl:template match="sect5info"/>
   <xsl:template match="sectioninfo"/>
   <xsl:template match="setindexinfo"/>
   <xsl:template match="setinfo"/>
   <xsl:template match="sidebarinfo"/>

	<!-- ****************************************************************** -->
   <!-- * Library Templates                                              * -->
	<!-- ****************************************************************** -->

	<xsl:template name="add.class.by.id">
		<xsl:if test="@id">
			<xsl:attribute name="class">
				<xsl:value-of select="@id"/>
			</xsl:attribute>
		</xsl:if>
	</xsl:template>
	
	<xsl:template name="transpose.spaces">
		<xsl:param name="string"></xsl:param>
		<xsl:choose>
			<xsl:when test="contains($string,' ')">
				<xsl:value-of select="substring-before($string,' ')"/>
				<xsl:text>&#160;</xsl:text>
				<xsl:call-template name="transpose.spaces">
					<xsl:with-param name="string">
						<xsl:value-of select="substring-after($string,' ')"/>
					</xsl:with-param>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$string"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="strip.leading.spaces">
		<xsl:param name="string"></xsl:param>
<xsl:message>Calling strip.leading.spaces</xsl:message>
<xsl:message>Value:<xsl:value-of select="$string"/></xsl:message>
<xsl:message>Starts-with:<xsl:value-of select="substring($string,1,1)"/></xsl:message>
		<xsl:choose>
			<xsl:when test="substring($string,1,1)='�'">
				<xsl:call-template name="strip.leading.spaces">
					<xsl:with-param name="string">
						<xsl:value-of select="substring($string,2,string-length($string)-1)"/>
					</xsl:with-param>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$string"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="perform-test">
		<xsl:call-template name="transpose.spaces">
			<xsl:with-param name="string">Oliver    Zendel ist muede</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="strip.ending.spaces">
		<xsl:param name="string"></xsl:param>
<xsl:message>Calling strip.ending.spaces</xsl:message>
<xsl:message>Value:<xsl:value-of select="$string"/></xsl:message>
<xsl:message>End-with: '<xsl:value-of select="substring($string,string-length($string),1)"/>'</xsl:message>
		<xsl:choose>
			<xsl:when test="substring($string,string-length($string),1)='	'">
<xsl:message>Found Tab</xsl:message>
				<xsl:call-template name="strip.ending.spaces">
					<xsl:with-param name="string">
						<xsl:value-of select="substring($string,1,string-length($string)-1)"/>
					</xsl:with-param>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="substring($string,string-length($string),1)=' '">
<xsl:message>Found Space</xsl:message>
				<xsl:call-template name="strip.ending.spaces">
					<xsl:with-param name="string">
						<xsl:value-of select="substring($string,1,string-length($string)-1)"/>
					</xsl:with-param>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
<xsl:message>Found Noting</xsl:message>
				<xsl:value-of select="$string"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="perform-test">
		<xsl:call-template name="transpose.spaces">
			<xsl:with-param name="string">Oliver    Zendel ist muede</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

<!--	
   <xsl:template match="legalnotice">
      <xsl:if test="//artinfo[1]|//bookinfo[1]">
         <xsl:for-each select="//artinfo[1]|//bookinfo[1]">
            <p><xsl:apply-templates select="descendant-or-self::copyright"/></p>
         </xsl:for-each>
      </xsl:if>
      <xsl:if test="not(//artinfo[1]|//bookinfo[1])">
         <xsl:if test="//copyright[1]">
            <p><xsl:apply-templates select="descendant-or-self::copyright"/></p>
         </xsl:if>
      </xsl:if>
      <p><xsl:apply-templates/></p>
   </xsl:template>

   <xsl:template match="releaseinfo|abstract">
      <p><xsl:apply-templates/></p>
   </xsl:template>



   <xsl:template match="authorgroup">
      <xsl:apply-templates select="author"/>
   </xsl:template>

   <xsl:template match="author">
      <div class="author">
         <h3 class="author">
            <xsl:value-of select="honorific"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="firstname"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="othername"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="surname"/>
         </h3>
      </div>
   </xsl:template>

   <xsl:template name="titlepage">
      <div class="titlepage">
         <a name="top"/>
         <h1 class="title">
            <xsl:value-of select="//artinfo[1]/title|//bookinfo[1]/title|//capterinfo[1]/title"/>
         </h1>
         <xsl:for-each select="bookinfo|artinfo"> 
            <xsl:apply-templates select="author|authorgroup"/>
            <xsl:apply-templates select="releaseinfo"/>
            <xsl:apply-templates select="legalnotice"/>
            <xsl:apply-templates select="abstract"/>
         </xsl:for-each>
         <hr width="100%"/>
      </div>
   </xsl:template>

   <xsl:template match="activeboxset"/>
   <xsl:template match="artinfo"/>
   <xsl:template match="chapterinfo"/>
   <xsl:template match="bookinfo"/>

-->
	<xsl:template match="*">
		<xsl:message>
			<xsl:text>Warning: Tag </xsl:text>
			<xsl:value-of select="name()"/> 
			<xsl:text> not supportet</xsl:text>
		</xsl:message>
	</xsl:template>
</xsl:stylesheet>
