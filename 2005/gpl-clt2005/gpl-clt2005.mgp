%include "default.mgp"
%default 1 bgrad
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
%nodefault
%back "blue"

%center
%size 7


Enforcing the GNU GPL
Copyright helps Copyleft


%center
%size 4
by

Harald Welte <laforge@gnumonks.org>



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
GNU GPL - Copyright helps Copyleft
Contents

	About the speaker
	The GNU GPL Revisited
	GPL Violations
	Past GPL Enforcement
	Typical case timeline
	Success so far
	What we've learned
	Problems encountered
	Future outlook
	Thanks

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page 
GNU GPL - Copyright helps Copyleft
Introduction


Who is speaking to you?
		an independent Free Software developer
		who earns his living off Free Software since 1997
		who is one of the authors of the Linux kernel firewall system called netfilter/iptables
		who IS NOT A LAWYER, although this presentation is the result of dealing almost a year with lawyers on the subject of the GPL

Why is he speaking to you?
		he thinks there is too much confusion about copyright and free software licenses. Even Red Hat CEO Matt Szulik stated in an interview that RedHat puts investments into 'public domain' :(

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
GNU GPL - Copyright helps Copyleft
Disclaimer

Legal Disclaimer

		All information presented here is provided on an as-is basis
		There is no warranty for correctness of legal information
		The author is not a lawyer
		This does not comprise legal advise
		The authors' experience is limited to German copyright law

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
The GNU GPL Revisited
What is copyrightable?

	The GNU GPL is a copyright license, and thus only covers copyrighted works
	Not everything is copyrightable (German: Schoepfungshoehe)
		Small bugfixes are not copyrightable (similar to typo-fixes in a book)
		As soon as the programmer has a choice in the implementation, there is significant indication of a copyrightable work
		Choice in algorithm, not in formal representation
	Apparently, the level for copyrightable works is relatively low


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
The GNU GPL Revisited
Terminology

	Public Domain
		concept where copyright holder abandons all rights
		same legal status as works where author has died 70 years ago (German: Gemeinfreie Werke)
	Freeware
		object code, free of cost. No source code
	Shareware
		proprietary "Try and Buy" model for object code.
	Cardware/Beerware/...
		Freeware that encourages users to send payment in kind

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
The GNU GPL Revisited
Terminology

	Free Software
		source code freely distributed
		must allow redistribution, modification, non-discriminatory use
		mostly defined by Free Software Foundation
	Open Source
		source code freely distributed
		must allow redistribution, modification, non-discriminatory use
		defined in the "Open Source Definition" by OSI

	The rest of this document will refer to Free and Open Source Software as FOSS.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
The GNU GPL Revisited
The GNU GPL Revisited

Revisiting the GNU General Public License

	Regulates distribution of copyrighted code, not usage
	Allows distribution of source code and modified source code
		The license itself is mentioned
		A copy of the license accompanies every copy
	Allows distribution of binaries or modified binaries, if
		The license itself is mentioned
		A copy of the license accompanies every copy
		The complete source code is either included with the copy (alternatively a written offer to send the source code on request to any 3rd party)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
The GNU GPL Revisited
Complete Source Code

%size 3
"... complete source code means all the source code for all modules it contains, plus any associated interface definition files, plus the scripts used to control compilation and installation of the executable."
	Our interpretation of this is:
		Source Code
		Makefiles
		Tools for generating the firmware binary from the source
			(even if they are technically no 'scripts')
	General Rule:
		Intent of License is to enable user to run modified versions of the program.  They need to be enabled to do so.
		Result: Signing binaries and only accepting signed versions without providing a signature key is not acceptable!


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
The GNU GPL Revisited
Derivative Works

	What is a derivative work?
		Not dependent on any particular kind of technology (static/dynamic linking, dlopen, whatever)
		Even while the modification can itself be a copyrightable work, the combination with GPL-licensed code is subject to GPL.
	No precendent in Germany so far
		As soon as code is written for a specific non-standard API (such as the iptables plugin API), there is significant indication for a derivative work
		This position has been successfully enforced out-of-court with two Vendors so far (iptables modules/plugins).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
The GNU GPL Revisited
Derivative Works

	Position of my lawyer:
		In-kernel proprietary code (binary kernel modules) are hard to claim GPL compliant
		Case-by-case analysis required, especially when drivers/filesystems are ported from other OS's.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
The GNU GPL Revisited
Collected Works

%size 3
"... it is not the intent .. to claim rights or contest your rights to work written entirely by you; rather, the intent is to exercise the right to control the distribution of derivative or collective works ..."
%size 3
"... mere aggregation of another work ... with the program on a volume of a storage or distribution medium does not bring the other work und the scope of this license"

	GPL allows "mere aggregation"
		like a general-porpose GNU/Linux distribution (SuSE, Red Hat, ...)

	GPL disallows "collective works"
		legal grey area
		tends to depend a lot on jurisdiction
		no precendent so far


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
The GNU GPL Revisited
Non-Public modifications

	Non-Public modifications
		A common misconception is that if you develop code within a corporation, and the code never leaves this corporation, you don't have to ship the source code.
		However, at least German law would count every distribution beyound a number of close colleague as distribution.  
		Therefore, if you don't go for '3a' and include the source code together with the binary, you have to distribute the source code to any third party.
		Also, as soon as you hand code between two companies, or between a company and a consultant, the code has been distributed.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
The GNU GPL Revisited
GPL Violations

	When do I violate the license
		when one ore more of the obligations are not fulfilled

	What risk do I take if I violate the license?
		the GPL automatically revokes any usage right
		any copyright holder can obtain a preliminary injunction banning distribution of the infringing product

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
GNU GPL - Copyright helps Copyleft
Past GPL enforcement

Past GPL enforcement

		GPL violations are nothing new, as GPL licensed software is nothing new.
		However, the recent GNU/Linux hype made GPL licensed software used more often
		The FSF enforces GPL violations of code on which they hold the copyright
			silently, without public notice
			in lengthy negotiations

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
GNU GPL - Copyright helps Copyleft
The Linksys case


	During 2003 the "Linksys" case drew a lot of attention
		Linksys was selling 802.11 WLAN Acces Ponts / Routers
		Lots of GPL licensed software embedded in the device (included Linux, uClibc, busybox, iptables, ...)
		FSF led alliance took the usual "quiet" approach 
		Linksys bought itself a lot of time
		Some source code was released two months later
		About four months later, full GPL compliance was achieved

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
GNU GPL - Copyright helps Copyleft
The Linksys case

	Some developers didn't agree with this approach
		not enough publicity
		violators don't loose anything by first not complying and wait for the FSF
		four months delay is too much for low product lifecycles in WLAN world
	The netfilter/iptables project started to do their own enforcement in more cases that were coming up

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
GNU GPL - Copyright helps Copyleft
Enforcement case timeline

	In chronological order
		some user sends us a note he found our code somewhere
		reverse engineering of firmware images
		sending the infringing organization a warning notice
		wait for them to sign a statement to cease and desist
		if no statement is signed
			contract technical expert to do a study
			apply for a preliminary injunction
		if statement was signed
			try to work out the details 
			grace period for boxes in stock possible
			try to indicate that a donation would be good PR

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
GNU GPL - Copyright helps Copyleft
Sucess so far

	Success so far
		amicable agreements with a number (25+) of companies
			sdome of which made significant donations to charitable organizations of the free software community
		preliminary injunction against Sitecom, Sitecom also lost appeals case 
		court decision of munich district court in Sitecom appeals case
		a second preliminary injunction against one of Germanys largest technology firms
		more settled cases (not public yet)
		negotiating in more cases
		public awareness 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
GNU GPL - Copyright helps Copyleft
Cases so far (1/2)


		Allnet GmbH
		Siemens AG
		Fujitsu-Siemens Computers GmbH
		Axis A.B.
		Securepoint GmbH
		U.S.Robotics Germany GmbH
		Netgear GmbH
		Belkin Compnents GmbH
		Asus GmbH
		Gateprotect GmbH
		Sitecom GmbH / B.V.
		TomTom B.V.
		Gigabyte Technologies GmbH
		D-Link GmbH

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
GNU GPL - Copyright helps Copyleft
Cases so far (2/2)


		Sun Deutschland GmbH
		Open-E GmbH
		Siemens AG (second case)
		Deutsche Telekom AG
		Hitachi Inc.
		Tecom Inc.
		ARP Datacon GmbH
		Conceptronic B.V.

		some more not public yet

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
GNU GPL - Copyright helps Copyleft
What we've learned


	Copyleft-style licenses can be enforced!
	A lot of companies don't take Free Software licenses seriously
		Even corporations with large legal departments who should know
		Reasons unclear, probably the financial risk of infringement was considered less than the expected gains
	The FUD spread about "GPL not holding up in court" has disappeared



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
GNU GPL - Copyright helps Copyleft
Future GPL Enforcement


	GPL Enforcement
		remains an important issue for Free Software
		will start to happen within the court more often
		has to be made public in order to raise awareness
		will probably happen within some form of organization

	What about Copylefted Content (Creative Commons)
		probably just a matter of time until CC-licensed works of art are infringed

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
GNU GPL - Copyright helps Copyleft
Problems of GPL Enforcement

	Problems
		distributed copyright
			is an important safeguard
			can make enforcement difficult, since copyright traditionally doesn't know cases with thousands of copyright holders
			distribution of damages extremely difficult
		the legal issue of having to do reverse engineering in order to prove copyright infringement(!)
		only the copyright holder (in most cases the author) can do it
		users discovering GPL'd software need to communicate those issues to all entitled parties (copyright holders)
		infringers obfuscating and/or encrypting fres software as disguise

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
GNU GPL - Copyright helps Copyleft
gpl-violations.org

	The http://www.gpl-violations.org/ project was started
		as a platform wher users can report alleged violations
		to verify those violations and inform all copyright holders
		to inform the public about ongoing enforcement efforts
	
	At the moment, project is only backed by the author
		more volunteers needed to investigate all cases
		something like 170 reported (alleged) violations up to day

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
GNU GPL - Copyright helps Copyleft
Make later enforcement easy

	Practical rules for proof by reverse engineering
		Don't fix typos in error messages and symbol names
		Leave obscure error messages like 'Rusty needs more caffeine'
		Make binary contain string of copyright message, not only source
	Practical rules for potential damages claims
		Use revision control system
		Document source of each copyrightable contribution
			Name+Email address in CVS commit message
		Consider something like FSFE FLA (Fiduciary License Agreement)
		Make sure that employers are fine with contributions of their employees
	If you find out about violation
		Don't make it public (has to be new/urgent for injunctive relief)
		Contact lawyer immediately to send wanrning notice

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
GNU GPL - Copyright helps Copyleft
Thanks

	Thanks to
		KNF
			for first bringing me in contact with linux in 1994
		Astaro AG
			for sponsoring most of my netfilter work
		Free Software Foundation
			for the GNU Project 
			for the GNU General Public License
		Dr. Till Jaeger
			for handling my legal cases

%size 3
	The slides of this presentation are available at http://www.gnumonks.org/

	Further reading:
%size 3
	The http://www.gpl-violations.org/ project
%size 3
	The Free Software foundation http://www.fsf.org/, http://www.fsf-europe.org/
%size 3
	The GNU Project http://www.gnu.org/
%size 3
	The netfilter homepage http://www.netfilter.org/
%%	http://management.itmanagersjournal.com/management/04/05/31/1733229.shtml?tid=85&tid=4


