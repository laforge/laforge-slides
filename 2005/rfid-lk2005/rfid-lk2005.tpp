--author Harald Welte <laforge@gnumonks.org>
--title RFID, Biometric Passports and Linux
--date 13 Oct 2005
Starting in November, the German federal government will be issuing epectronic passports with RFID interface.  

This presentation covers technical background about the RFID technology, the ICAO MRTD specification, and the authors' efforts to develop a free software stack to use Linux to communicate with those passports.
--footer This presentation is made with tpp http://synflood.at/tpp.html

--newpage
--footer RFID, Biometric Passports and Linux
--header Overview
Introduction into RFID
	What is RFID
	Components of RFID System
	Protocols and Standards
	Security ISsues
librfid - A free software RFID stack
	Data Structures
	Protocol Stack
	Interaction with OpenCT
ePassports - Electronic Passports
	Organization of Data
	Security Features
	PKI
	Crypto Algorithms
	Security Threats

--newpage
--footer RFID, Biometric Passports and Linux
--header Introduction into RFID
Definition of term RFID
	Radio Frequency IDentification
	
RFID is one of the recent buzzwords in lots of industries, such as
	transportation
	retail sector
	governments

Like most buzzwords, it's not very clearly defined.  There is no such thing as "the RFID System. There are lots of different Systems, some standardized, most proprietary.  Each of them uses it's own frequency, modulation, encoding and protocol combination.  Often, systems of multiple vendors can not be used interchangibly.

--newpage
--footer RFID, Biometric Passports and Linux
--header Components of an RFID system
Tag (Transponder)
	Serial Number Tags
		Replacement for EAN/UPC Barcodes
	WORM Tags
		Can be written once by Issuer
	Read/Write Tags
		Can be re-written many times
	Read/Write Tags with "passive" security
		Have state-machine based crypto for access control
	Cryptographic smartcards with RF Interface
		Like other crypto smartcards, just with RF interface

--newpage
--footer RFID, Biometric Passports and Linux
--header Reader
Readers (Coupling Device)
	Readers are always called readers, even if they can write ;)
	Usually connected to a host computer via RS-323, USB or alike
	Unfortunately no standard, for API, Hardware and/or Protocol :(
	Most applications are written to vendor-provided device-specific API's
	One exception: Readers for Smartcards with RF-Interface (use PC/SC)

--newpage
--footer RFID, Biometric Passports and Linux
--header RF Interface
The RF interface is the key attribute of any RFID system.
Parameters that determine the RF interface are
	frequency
	modulation
	operational principle

--newpage
--footer RFID, Biometric Passports and Linux
--header RF Interface
Magnetic Coupling
	used by many of todays RFID deployment
	rely on the magnetic coupling ("transformer") principle
	Tag/Transponder has a coil antenna to pick up RF-Field of Reader
	Power for Tag/Transponder is drawn from the magnetic field
	Common systems use 125kHz (old) or 13.56MHz (current)
	Operational range often small, since high magnetic field strengh needed

--newpage
--footer RFID, Biometric Passports and Linux
--header RF Interface
Backscatter
	Used by many RFID systems under current development
	Operate typically in UHF range (868 to 956 MHz)
	Use electric field of the reader, employ backscatter modulation
	Higher operational range (within tens of metres)

Surface Accoustic Wave
	SAW tags use low-power microwave radio signals
	Tag/Transponder converts it to untrasonic signals (piezo crystal)
	Variations of the reflected signal used to provide a unique number

--newpage
--footer RFID, Biometric Passports and Linux
--header Protocols and Standards
Apart from the various vendor proprietary protocols, there are some ISO standards
ISO 11784 / 11785
	Identification of Animals
	134.2kHz, magnetic coupling, load modulation, 4191 bps
ISO 14223
	Extension of 11784/11785 and allows for more data
ISO 10536
	"close coupling" smart cards, range up to 1cm
	Inductive or capacitive coupling at 4.9152MHz
	Never attained any significant market share
ISO 18000 series
	Current development of international "Auto-ID" standard
	Includes operation on 13.56MHz, 2.4GHz, 868/956MHz
	Not yet deployed

--newpage
--footer RFID, Biometric Passports and Linux
--header Protocols and Standards
ISO 14443
	"proximity coupling ID cards"
	Range of up to 10cm
	Two variants: 14443-A and 14443-B
	Both use 13.56MHz, but different parameters (see paper for details)
	Specifies physical layer, link-layer (anticollision)
	Specifies an optional transport level protocol (ISO 14443-4)
	Speed up to 848kbits/sec

ISO 15693
	"vicinity coupling", range up to 1m
	Like ISO 14443, operates on 13.56MHz, magnetic coupling
	Data rate 1.65kbits/sec or 26.48kbits/sec
	Because of long distance, very little power
	Therefore only used for passive tags

--newpage
--footer RFID, Biometric Passports and Linux
--header Closer look on Readers
There's a variety of readers for the 13.56MHz world
Usually they all use one of the (small number of) available ASIC's
Reader ASIC's integrate analog and digital part and have standard bus interface
End-User Reader products contain such an ASIC plus a microcontroller

Active Readers
	e.g. "Philips Pegoda"
	Run the RFID protocol stack on the microcontroller

Passive Readers
	e.g. "Omnikey CardMan 5121"
	Run the RFID protocol on the host system

Passive readers obviously provide higher flexibility and are cheaper.

--newpage
--footer RFID, Biometric Passports and Linux
--header Security Issues
Eavesdropping
	Channel from reader to tag can be easily sniffed (even > 10m)
	Channel from tag to reader is difficult (Author has managed 3m)

Denial of Service
	Anti-collision mechanism used to distinguish between multiple tags
	Using a "fake tag" you can create Denial of Service
	Products such as "blocker tags" have already been presented

Authenticity/Confidentiality
	None of the existing standards offers any kind of crypto
	Standards-compliant systems like passports use crypto at layer 5
	Lots of proprietary "closed algorithm" vendor products with questionable security


--newpage
--footer RFID, Biometric Passports and Linux
--header librfid - A Free Software RFID stack
The librfid project intends to provide a free software reader-side implementation of common RFID protocols such as ISO 14443 and ISO 15693

Various abstraction layers and plugin interface allows for later addition of new protocols an readers.

Optionally integrates with OpenCT.

--newpage
--footer RFID, Biometric Passports and Linux
--header librfid - A Free Software RFID stack
struct rfid_asic
	Contains all routines for a specific reader asic
	Currently only Philips CL RC 632 supported
struct rfid_asic_transport
	A transport that gives access to the ASIC registers
struct rfid_reader
	A container for rfid_asic and rfid_asic_transport
struct rfid_layer2
	An anticollision protocol such as ISO 14443-3A/B
struct rfid_protocol
	A transport protocol suhc as ISO 14443-4

--newpage
--footer RFID, Biometric Passports and Linux
--header librfid - A Free Software RFID stack

Typical Protocol Stack
	rfid_protocol_stack
	CM5121 Reader
	CL RC632 ASIC
	PC_to_RDR_Escape transport
	USB-CCID driver of OpenCT
	libusb

--newpage
--footer RFID, Biometric Passports and Linux
--header librfid - A Free Software RFID stack
Application Interface

Native API
	librfid-specific API
	quite low-level
	requires application to know a lot about the stack

OpenCT, PC/SC, CT-API
	OpenCT integration provides PC/SC and CT-API for crypto smarcards
	Is currently under development

--newpage
--footer RFID, Biometric Passports and Linux
--header Electronic Passports
Electronic Passports (ePassports) are officially called MRTD
MRTD: Machine Readable Travel Document
Specifications by ICAO (International Civil Aviation Organization)
Basic idea 
	store passport data and additional biometrics on Transponder
	alternate storage methods such as 2D barcodes covered, too
	common standard for interoperability
	some features required, others optional (up to issuing country)

--newpage
--footer RFID, Biometric Passports and Linux
--header Electronic Passports
Organization of Data
	According to LDS (Logical Data Structure) specification
	Data is stored in DG (Data Groups)
		DG1: MRZ information (mandatory)
		DG2: Portrait Image + Biometric template (mandatory)
		DG3-4: fingerprints, iris image (optional)
	EF.SOD: Security Object Data (cryptographic signatures)
	EF.COM: Lists with Data Groups Exist

--newpage
--footer RFID, Biometric Passports and Linux
--header Electronic Passports
Security Features
	Randomization of Serial Number
	Passive Authentication (mandatory)
		Inspection System verifies signature of DG's
	Active Authentication (optional)
		Verifies that chip has not been substituted
	Basic Access Control (optional, implemented in .de passports)
		Denies Access to the chip until inspection system is authorized
		Authorization is performed by deriving keys from MRZ
	Extended Access Control (optional)
		Prevents unauthorized access to additional bimetrics
		Similar to Basic Access Control, but different keys

--newpage
--footer RFID, Biometric Passports and Linux
--header Electronic Passports
Public Key Hierarchy
	X.509 Certificates
	Every country operates it's own CA
	Document signer keys derived from CA root
	Document signer public keys are distributed publicly via ICAO PKD
	Everyone can verify

--newpage
--footer RFID, Biometric Passports and Linux
--header libmrtd - Free Software library for MRTD's
libmtrd provides functions for
	reading out and decoding data on MRTD
	verifying data stored on MRTD
	cryptograpy compliant with MRTD specs
		basic access control
		passive authentication
		extended access control (planned)

--newpage
--footer RFID, Biometric Passports and Linux
--header libmrtd - Free Software library for MRTD's
API towards the lower level (transport)
	PC/SC (to work with readers/drivers other than librfid)
	native librfid API
API towards the application
	not really finished yet, lots of flux

--newpage
--footer RFID, Biometric Passports and Linux
--header libmrtd - Free Software library for MRTD's
libmrtd status
	parsing functions for LDS
	parsing functions for DG1
	parsing functions for DG2 (CBEFF)
	basic access control
	still very early alpha stage
	contributors welcome
	no program
