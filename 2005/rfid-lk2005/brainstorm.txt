- RFID confusion
	- passive 1-bit tags
	- ro/rw memory
	- state machines for permission checking
	- contactless smartcards (processor chip cards, ...)

- processor chip cards
	- typically 8bit micrprocessor (e.g. 8051 based)
	- MMU (!)
	- typical configuration: 1k RAM, 32k Flash, 70k mask ROM


- ISO 14443 "proximity cards"
	- 14443-1 (physical specification)
		- 13.56 MHz
	- 14443-2 (radio interface)
		- power transmission from reder (PCD) to card (PICC)
			- 13.56MHz +- 7kHz
			- field strength 1.5A/m to 7.5A/m (rms)
		- channel from reader to card
			- Type A: carrier modulation with 100% ASK, modified miller code
			- Type B: carrier modulation with 10% ASK, NRZ-L code
		- channel from card to reader
			- Type A: load modulation on subcarrier fc/16, OOK, manchester code
			- Type B: load modulation on subcarrier fc/16, BPSK, NRZ-L code
	- 14443-3 (anti collision)
		-


- biometric passport
	- technical view
		- iso 7816-4 command based processor chip card
		- iso 14443-{1,2,3,4} based RFID interface
