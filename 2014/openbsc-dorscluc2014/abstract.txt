Free Software for GSM networks

During its 25 year history, Free Software has ventured in many areas of
computing, such as TCP/IP networks, Internet servers, personal computers,
laptops, desktop computers, embedded devices, and so on.

However, there are other areas of computing that - until very recently - have
not yet seen any Free Software.  One prime example is cellular telephony
networks.   More than 3 billion subscribers use GSM cellular phones around the
world.  All components in the public GSM networks are proprietary
both on the network side and on the telephon side.

The cellular networks consist of components like base stations, telephone
switches, all running proprietary software.

The cellular phones - even those running Free Software based operating systems
liek Android - have a separate computer called "baseband processor" that
interacts with the GSM network and runs proprietary software.

Since 2009, projects like OpenBTS, OpenBSC and OsmocomBB have been created to
change this.  They all implement components of a GSM network as Free Software.

Harald Welte is the founder of OpenBSC and OsmocomBB.  He will discuss the
proprietary nature of the GSM world, the progress of Free Software in GSM
and how the GSM related Free Software projects can be used in research
and production.
