Retronetworking divf - yate, dhdi-trunkdev and more
===================================================
:revealjsdir: /home/laforge/projects/git/reveal.js
:revealjs_width: 1918
:revealjs_height: 1070
:revealjs_transition: none
//:revealjs_theme: night
//:revealjs_theme: beige
//:revealjs_theme: solarized


:author:      Harald Welte <laforge@gnumonks.org>
:copyright:   2023 by Harald Welte (License: CC-BY-SA)
:backend:     slidy
:max-width:   45em

== Who am I?

* Harald "LaF0rge" Welte
* Recovering Linux Kernel hacker (netfilter/iptables)
* Founder of Osmocom (Open Source Mobile Communications)
** tons of FOSS projects around mobile comms
* Active participant of CCC Camps and congresses since 1999
* Dayjob: Implementing cellular protocols across all layers
* Hobby: Research on data communications from the 1970s to 1990
** *Osmocom retronetworking* project was also operating ISDN + POTS at CCC Camp 2023


== Retronetworking

* many people enjoy *retrocomputing*
** focus is on historical computers (real or emulated)
* but what's a computer without communications technology?
* *retronetworking* is just like *retrocomputing*
** focus is on networking / telecommunications

== Why

* because we can
* relevance of communication technology to computing
** (at least) since the 1980s, home computers / PCs used acoustic couplers and modems
* equipment (user side, network side) readily available
** NICs, Modems, ISDN-TA, Video Telephones, Fax Machines
* personal reasons:
** I mostly was a user *back in the day*
** now I'm a developer with decades of experience
** no chance back then to play with / operate the network side

== Why (more serious reasons)

* our society becomes more and more technology-driven
* preserving [knowledge about] historical technology is important to preserve our *cultural history*
** thanks to Software Heritage Project, even software is now considered *immaterial cultural heritage* by
UNESCO
* today we still have a chance of collecting and/or documenting *early digital* communications technology
* window of opportunity is closing
** equipment harder to find once it is all scrapped
** software for management / configuration often even harder
** people involved in creating it are passing away

== OCTOI Network

image::octoi-network.png[width=1800]

== Joining OCTOI Network

* get a PBX with E1/PRI/S2M uplink
* get an icE1usb device + embedded Linux (rpi, beaglebone, nanopi, ...)
** icE1usb available with considerable community discount
* connect your system via the internet to the OCTOI hub
* interact with hosted services and/or other users

== How to get in touch?

* join our forums at https://discourse.osmocom.org/
* join our IRC channel `#retronetworking` on `libera.chat`
* join our (almost) monthly teleconference `RetroNetCall`
* contact me privately if you have stuff to donate
** e-mail: `laforge@gnumonks.org`
** Fediverse: `@LaF0rge@chaos.social`

== Further Reading

* https://osmocom.org/projects/retronetworking/wiki
* https://osmocom.org/projects/octoi/wiki
* https://projects.osmocom.org/projects/retronetworking/wiki/RetroNetCall


[.questions]
=== !

[.bubbles]
=== !

[.hands]
=== !


== EOF

End of File
