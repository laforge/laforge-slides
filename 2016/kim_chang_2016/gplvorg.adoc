The past and the future of Free Software License Violations
===========================================================
:author:	Harald Welte <laforge@gpl-violations.org>
#:copyright:	sysmocom - s.f.m.c. GmbH (License: CC-BY-SA)
:backend:	slidy
:max-width:	45em



== About the speaker

* A _deeply_ technical person, IANAL.
* Started as FOSS sysadmin in the mid-1990ies
* Network security expert, electronics engineer, software developer.
* Former Linux Kernel developer from 1999 on
* Former head of netfilter core team
* Founder of gpl-violations.org
* Recipient of FSF Award for the Advancement of Free Software
* Recipient of Google/O'Reilly Open Source Award
* Now fully immersed in implementing cellular (GSM/3G) protocol stacks
  under the Osmocom.org project (mostly AGPLv3)

== My personal journey into _the communities_

The culture in which we grow up defines our values. For me:

* BBS communities (FIDO, Z-Netz, ...) and UseNet @ age 12
* programming DOS shareware in TurboPascal @ age 13
** Didn't know about Free Software yet. My apologies!
* switched to GNU/Linux before Windows 95, never looked back
** learning about Free Software, GNU, copyleft, the GPL
* from 1994 on, helped building a non-for-profit ISP
** started to write + contribute patches against software we used there
* from 1999 onwards: netfilter/iptables, the Linux 2.3/2.4 packet filter

[role="incremental"]
=> all of the above were communities of enthusiasts

[role="incremental"]
* open to anyone
* information and code was shared freely, to mutual benefit


== Linux and license compliance

* Until around 2000, Linux was still the niche of the nerds
** the Long-bearded gurus used a *real* UNIX instead
** the rest of the world was trapped in Microsoft-land

[role="incremental"]
* GPL violations on the Linux kernel were not known to me until about 2002
* First news about GPL violations made me very upset
** the industry ignored our culture, rules and norms
** they took what we had created and did not give back
** as companies didn't react to friendly reminders, I started legal action
** gpl-violations.org was started, first legal case in 2003
** enforcement in hundreds of cases, most of them out of court
** prevailed in several German court cases, 100% success rate



== The past of FS license enforcement

For those not around to witness it:

* early work by the FSF (until 2004?)
** entirely out of court
* gpl-violations.org (2003-2011)
** started by a Linux Kernel developer (yours truly)
* Software Freedom Conservancy (2006-current)
** doing excellent work on behalf of many projects since


== gpl-violations.org early history

* device makers stared to use embedded Linux in WiFi routers
* vendors did not get into compliance
* some frustration existed with FSFs back then very tolerant approach
  of pushing for compliance at Linksys
* further companies were infringing, triggering me as one of the many
  copyright holders to pursue independent legal action against product
  vendors in Germany


== gpl-violations.org later history

Fast-Forward 8 years. Results:

* more than two hundred enforcements in total
** some of them didn't even reach any legal claims
** most of them were settled out of court
** some very few actually had to go to court
* created some of the first precedent in terms of GPL enforcement in
  court, both in Germany and world-wide
* not a single case lost

== gpl-violations.org dormancy

* While doing netfilter work as dayjob, there still was time to do
  compliance work in spare time
* Increasingly difficult when I got involved with OpenMoko in Taiwan
  (2007-2009)
* Impossible to find time while I started + bootstrapped my new
  company sysmocom from 2011 onwards
* Big loss to the project when Armijn left in 2012

Result: No gpl-violations.org activity in years. Project became
dormant.

== gpl-violations.org dormancy

* I've never been particularly sad about the dormancy
* We did some pioneering and hugely successful work in GPL
  enforcement, creating ripples throughout the technology industry.
* The FSFE legal network got started as a forum for related topics
* Other people (e.g. SFC) started to do enforcement
* So I didn't think it's a loss if I focus on other areas for an
  undefined amount of time

Still, it is a pity that it was too much tied to me personally,
and there was no structure and no team that could continue the work.

Let's learn from that...

== Resurrection, Step 1 (Q4/2015)

* brought historic content of gpl-violations.org back online
* occasional blog post about GPL related topics again
* getting more exposure in FOSS legal community again
* reporting about VMware case (in which I'm not legally involved, but
  which I very much support)


== Resurrection, Step 2 (2016)

* establishing a legal body for new gpl-violations.org activities
** put project on more shoulders
** less dependency on me personally
** taking legal action as natural person didn't allow others to get
   involved to larger extent due to associated personal risk
* I wanted to have it established before LLW, but schedule slipped.
  Plan is to definitely complete this within Q2/2016.


== gpl-violations.org e.V.

* structure of a German "eingetragener Verein" (e.V.)
* membership-based entity, where FOSS developers can become members
* members can (but do not have to) sign fiduciary license agreement to
  enable gpl-violations.org e.V. to enforce license on their behalf
* any enforcement will be done in compliance with the principles of
  community-oriented enforcement as published by SFC+FSF
* is not going to be charitable due to increased tax/legal risk
** financial structure and usage of funds will be published to avoid
   any claims regarding misappropriation of funds


== How is this different to SFC?

* Jurisdiction / Geographic Scope
** SFC is primarily active in the US (so far?)
** gpl-violations.org would be primarily active in Germany, maybe EU
* There's no shortage of violations to enforce, i.e. room for many
  more people or entities doing active enforcement
* Very narrow focus on copyleft license enforcement, no other services

Apart from that, in terms of goals and actual enforcement work, not
all that different.  At last not that it is planned.


== Isn't more enforcement harmful?

* there is some feeling that more enforcement scares people away from
  FOSS
* I think it matters a lot about the _style_ of enforcement. We need
  more evidence of people caring about licenses and doing enforcement
  in a proper and respected way; compliance-centric and within a
  generally accepted common sense.
* I also think license enforcement is required to make new (corporate)
  players in the FOSS world comply, and to continuously encourage and
  increase motivations for companies to be compliant
* Last, but not least: License enforcement is also happening in
  proprietary software, so it's not a specific issue of FOSS, so let's
  not over-dramatize it.


== Actual enforcement process

* will probably not look any different from the past
* reports of GPL violations by the community at large
* technical investigation + establishing legal evidence
* sending warning notice to company, requesting cease + desist
* resolving the issue hopefully out of court
* going to court whenever it is really necessary

== Taking a step back

[role="incremental"]
* companies start to work on/with Linux without following
  collaborative development model.  Their management is free to
** ignore the decades-old requests by the community
** ignore requests by their own engineers to contribute
* community upset, because management did *not* enable, allow or require
** FOSS development to be done in the regular, collaborative process
** their engineers to contribute
* gpl-violations.org uses the legal vehicle of copyright enforcement
** senior management cannot ignore legal threats, we got their attention!
* Result: they ask their lawyers what needs to be done to comply to
  the absolute minimum _legally_ required to not get in trouble
** they do still not follow the collaborative development process

== The cultural impedance mis-match

Surprise: FOSS is about collaborative development

[role="incremental"]
* participation on mailing lists
* developing code in public repositories
* using fine grained commits
* to **jointly develop software**
* it is **not about procrastinating over legal issues**
* FOSS developers _really_ want **collaboration, not license compliance**
** GPL is just a legal hack to ensure the bare absolute minimum of adherence to the FOSS culture
** it suffers from impedance mismatch between what can be done under copyright law, and not what is _actually_ the goal in terms of a development model
** focusing _just_ on legal compliance with the license indicates a lack of understanding
* **GPL compliance should not be driven only by lawyers!**

== Cultural Differences

[role="incremental"]
* exist between every set of two cultures
* think of _Western_ vs. _Asian_ culture
* westerners (_farang/gaijin/laowei_) are considered rude, if they
[role="incremental"]
** stick chopsticks in a rice bowl anywhere in Asia
** have loud phone conversations on a Japanese train
** want to split a restaurant bill in China
** decline to accept Soju offered by their Korean host
** use a Buddha statues head as decoration in Thailand
* Being European and coming to Asia likely causes me to make mistakes
due to the _cultural differences_.
* those mistakes may cause people to be upset with me. _How could I
not know?_  Couldn't I at least inform myself before travelling?
* This is not so different from an electronics or proprietary software
company first engaging with FOSS

== Outlook

* get over with formalities of establishment
* get initial group of members to sign up
* establish and tune the related processes
* get started with some actual enforcement

== Thanks

* to Armijn Hemel for helping me all those years in the past at
  gpl-violations.org
* to Till Jaeger and his team at JBB for all their legal help
* to FSFE for their great work far beyond the Legal Network

You now have a license to ask questions ;)
