Linux, Community, License Compliance
====================================
:author:	Harald Welte <laforge@gnumonks.org>
:copyright:	Harald Welte (License: CC-BY-SA)
:backend:	slidy
:max-width:	45em
//:data-uri:
//:icons:


== Who am I and why am I here?

[role="incremental"]
* Former Linux kernel developer (mostly netfilter/iptables)
* as technical as it can get. Not a lawyer.
* have had many, many other lives, including:
** helping an (ARM) SoC maker to understand mainline development process
** security research + ethical hacking @ German CCC
** Open Hardware + FOSS firmware/software RFID reader
** electronics + software development for the first _100% FOSS_ smartphone Openmoko
** 2008 onwards: OpenBSC, Osmocom: FOSS implementation of telecom protocol stacks for GSM/GPRS/EDGE/UMTS infrastructure
** 2011 onwards: running a small company in Berlin doing FOSS based cellular infrastructure
* but also: Legal enforcement of the GNU GPL on the Linux kernel
* I'm here to share my personal perspective on License compliance


== My personal journey into _the communities_

The culture in which we grow up defines our values. For me:

* BBS communities (FIDO, Z-Netz, ...) and UseNet @ age 12
* programming DOS shareware in TurboPascal @ age 13
** Didn't know about Free Software yet. My apologies!
* switched to GNU/Linux before Windows 95, never looked back
** learning about Free Software, GNU, copyleft, the GPL
* from 1994 on, helped building a non-for-profit ISP
** started to write + contribute patches against software we used there
* from 1999 onwards: netfilter/iptables, the Linux 2.3/2.4 packet filter

[role="incremental"]
=> all of the above were communities of enthusiasts

[role="incremental"]
* open to anyone
* information and code was shared freely, to mutual benefit


== Linux and license compliance

* Until around 2000, Linux was still the niche of the nerds
** the Long-bearded gurus used a *real* UNIX instead
** the rest of the world was trapped in Microsoft-land

[role="incremental"]
* GPL violations on the Linux kernel were not known to me until about 2002
* First news about GPL violations made me very upset
** the industry ignored our culture, rules and norms
** they took what we had created and did not give back
** as companies didn't react to friendly reminders, I started legal action
** gpl-violations.org was started, first legal case in 2003
** enforcement in hundreds of cases, most of them out of court
** prevailed in several German court cases, 100% success rate


== Technical GPL enforcement

In the active phase of gpl-violations.org, we would

[role="incremental"]
* browse new product announcements, vendor web sites for suspicious-looking products
* go into electronics stores and make test purchases
* disassemble the hardware
* reverse-engineer serial console, JTAG
* dump flash via JTAG or hot-air-rework and offline flash dumping
* manually unpack the (often proprietary) firmware image formats
* search for strings/symbols of Linux kernel code that I hold copyright on
* As this is the technical part, it can actually be quite enjoyable.
* Buying new gadgets and probing test-points for UART/JTAG definitely
  more enjoyable and rewarding than Sudoku for me ;)


== Legal GPL enforcement

After technical analysis is complete, the legal battle starts

[role="incremental"]
* explaining technical evidence to your lawyer
* reviewing legal briefs of both parties
* spending lots of time trying to teach corporate legal departments
  what you have learned as a teenager growing up with FOSS
* makes you **even more frustrated/upset**, as this costs time
** not only do they insult the community and its culture
** they now also keep me from writing more code by being hostile or ignorant
** and they force me to take legal risks

[role="incremental"]
Starts all over again with each new vendor, department within
the vendor, or at least in every new market Linux gets introduced :(

== Taking a step back

[role="incremental"]
* companies start to work on/with Linux without following
  collaborative development model.  Their management is free to
** ignore the decades-old requests by the community
** ignore requests by their own engineers to contribute
* community upset, because management did *not* enable, allow or require
** FOSS development to be done in the regular, collaborative process
** their engineers to contribute
* gpl-violations.org uses the legal vehicle of copyright enforcement
** senior management cannot ignore legal threats, we got their attention!
* Result: they ask their lawyers what needs to be done to comply to
  the absolute minimum _legally_ required to not get in trouble
** they do still not follow the collaborative development process


== The cultural impedance mis-match

Surprise: FOSS is about collaborative development

[role="incremental"]
* participation on mailing lists
* developing code in public repositories
* using fine grained commits
* to **jointly develop software**
* it is **not about procrastinating over legal issues**
* FOSS developers _really_ want **collaboration, not license compliance**
** GPL is just a legal hack to ensure the bare absolute minimum of adherence to the FOSS culture
** it suffers from impedance mismatch between what can be done under copyright law, and not what is _actually_ the goal in terms of a development model
** focusing _just_ on legal compliance with the license indicates a lack of understanding
* **GPL compliance should be driven by engineering, not legal!**


== Cultural Differences

[role="incremental"]
* exist between every set of two cultures
* think of _Western_ vs. _Asian_ culture
* westerners (_farang/gaijin/laowei_) are considered rude, if they
[role="incremental"]
** stick chopsticks in a rice bowl anywhere in Asia
** have loud phone conversations on a Japanese train
** want to split a restaurant bill in China
** decline to accept Soju offered by their Korean host
** use a Buddha statues head as decoration in Thailand
* Being European and coming to Asia likely causes me to make mistakes
due to the _cultural differences_.
* those mistakes may cause people to be upset with me. _How could I
not know?_  Couldn't I at least inform myself before travelling?
* This is not so different from an electronics or proprietary software
company first engaging with FOSS


== License Compliance in 2016?

[role="incremental"]
* those parts of the IT industry exposed to
  (embedded) Linux for a longer time make more of an effort to comply
  _with legal requirements only_
** establishing the required release + business processes
** FOSS + proprietary tools for aiding license compliance
** Legal Network by FSFE with hundreds of legal experts
* license compliance is driven by fear of legal threats, not by
  understanding + following collaborative development models :(
* Treated similar to compliance with environmental standards, regulatory requirements, etc.

[role="incremental"]
=> Bringing back the Western vs. Asian cultural analogy:

[role="incremental"]
* Our _farang/gaijin/laowei_ now complies with local laws by not
  bringing restricted items (medication, too long pocket knives) into
  Asia which might be legal at his home (legal compliance)
* He still often ignores the local culture and social norms, and is
  perceived by some of the locals as disrespectful or rude at times
  (doesn't cause legal risks)


== Summary

[role="incremental"]
* legal-to-the-letter compliance has significantly improved over the
  last 15 years
* awareness that license compliance is mandatory is widely present
* collaborative FOSS development model is becoming more frequent
* however, some industry players, particularly those doing FOSS for a shorter
  time still think FOSS is a one-way road that enables them to profit
  on the work of others while keeping their code private / out-of-tree
** Sure, you can have a marriage that caters exclusively to the needs of one of the people involved
*** But will it be a sustainable long-term relationship?
*** Or will it just be a short affair?
* we need to shift the focus from _legal-centric GPL compliance_ to
  _engineering-centric collaborative development_


== The End

Thanks for your attention.

* You have a license to raise questions now !
