#!/usr/bin/perl -w

$preface = q{\newcount\olsmaster
\olsmaster=1

\documentclass[twocolumn,twoside,12pt]{combine}
\usepackage{ols}
\ifpdf
\usepackage[pdftex]{epsfig}
\else
\usepackage{epsfig}
\fi
\usepackage{rotating}

% Other packages that authors have used...
\usepackage[modulo]{lineno}
\usepackage{alltt}

\usepackage[T1]{fontenc}
\ifpdf
\usepackage[pdftex]{graphicx}
\else
\usepackage{graphicx}
\fi
\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage{csty}

\usepackage{enumerate}
\usepackage{geometry}
%%%%% html breaks 'combine' rather badly
% \usepackage{html} 
%%% hyperref is nearly as bad
% \usepackage{hyperref}
\usepackage{isolatin1}
\usepackage{latexsym}
\usepackage{longtable}
\usepackage{multicol}
\usepackage{cprog}
\usepackage{float}
\usepackage{supertabular}
\usepackage{textcomp}
\usepackage{times}
\usepackage{url}
\usepackage{usenix}
\usepackage{wrapfig}
\usepackage{fancyvrb}

\title{{\em\normalsize Reprinted from the}\\\\
Proceedings of the\\\\
Linux Symposium}
\author{\vspace{4in}}
\date{July 21th--24th, 2004\\\\
 Ottawa, Ontario\\\\
 Canada}

% make room for "OLS2004...pagenumber" header
\setlength{\topmargin}{-0.5in}
\setlength{\headheight}{0.2in}
\setlength{\headsep}{0.3in}

\thispagestyle{empty}
\pagestyle{empty}
%%%%%%%%%%%%%%%%% DOC STARTS HERE %%%%%%%%%%%%%%%%%%%%
\begin{document}
\pagestyle{empty}
\thispagestyle{empty}


%%%%%%%%%%%%%% TITLE PAGE %%%%%%%%%%%%%%%%%%%
\twocolumn[\pagestyle{empty}\thispagestyle{empty} \maketitle ]


%%%%%%%%%%%%%%%%%%%%% CREDITS PAGE %%%%%%%%%%%%%%%%%%
\thispagestyle{empty}
\twocolumn
\thispagestyle{empty}
%%% \thispagestyle{empty}
\begin{minipage}[t][0.95\textheight]{\textwidth}
\thispagestyle{empty}

\vspace{2cm}

\textbf{{\Large Conference Organizers}}

\vspace{5mm}
\begin{large}
\hspace*{0.5in}Andrew J.\ Hutton, \textit{Steamballoon, Inc.}\\\\
\hspace*{0.5in}Stephanie Donovan, \textit{Linux Symposium}\\\\
\hspace*{0.5in}C.\ Craig Ross, \textit{Linux Symposium}
\end{large}

\vspace{1cm}
\textbf{{\Large Review Committee}}

\vspace{5mm}
\begin{large}
\hspace*{0.5in}Jes Sorensen, \textit{Wild Open Source, Inc.}\\\\
\hspace*{0.5in}Matt Domsch, \textit{Dell}\\\\
\hspace*{0.5in}Gerrit Huizenga, \textit{IBM}\\\\
\hspace*{0.5in}Matthew Wilcox, \textit{Hewlett-Packard}\\\\
\hspace*{0.5in}Dirk Hohndel, \textit{Intel}\\\\
\hspace*{0.5in}Val Henson, \textit{Sun Microsystems}\\\\
\hspace*{0.5in}Jamal Hadi Salimi, \textit{Znyx}\\\\
\hspace*{0.5in}Andrew Hutton, \textit{Steamballoon, Inc.}
\end{large}

\vspace{1cm}

\textbf{{\Large Proceedings Formatting Team}}

\vspace{5mm}
\begin{large}
\hspace*{0.5in}John W.\ Lockhart, \textit{Red Hat, Inc.}\\\\
\end{large}


% \vspace*{\fill}

\begin{center}
\vspace{2.5in}
Authors retain copyright to all submitted papers, but have granted 
unlimited redistribution rights to all as a condition of submission.
\end{center} 
\end{minipage}

%%%%%%%% PAGE HEADINGS DEFINITIONS %%%%%%%%%%%%%%%%%%%%
\pagestyle{myheadings}
\markboth{~~{\textbullet}~~Linux Symposium\ ~\hrulefill\ }{\ \hrulefill\ Linux Symposium 2004~~{\textbullet}~~}

%%%%%%%%%%%%%%% PAPERS BEGIN HERE %%%%%%%%%%%%%%%%%%%%%%%%%%
};

###### THIS IS THE SORT OF STUFF THAT MAKES UP THE MIDDLE...
### \setcounter{page}{176}
### \begin{papers}
### \coltocauthor{Kai Germaschewski} % { University of Iowa}
### \coltoctitle{Kernel configuration and building in Linux 2.5}
### \label{art16}
### \import{kbuild}

$ender = q{\end{papers}
\clearpage
\end{document}
};

# print "hey, the quotes worked\n";
@authors = ( );
@titles = ( );
@imports = ( );
@pagenos = ( );

open(AUTHORS, "grep coltocauthor ./MasterOLS.tex|") or die "nogrep";
while (defined($ln = <AUTHORS>)) {
  chomp $ln;
  push(@authors, $ln);
}
close(AUTHORS);

open(TITLES, "grep coltoctitle ./MasterOLS.tex|") or die "nogrep2";
while (defined($ln = <TITLES>)) {
  chomp $ln;
  push @titles, $ln;
}
close(TITLES);

open(IMPORTS, "grep import ./MasterOLS.tex|") or die "nogrep3";
while (defined($ln = <IMPORTS>)) {
  chomp $ln;
  push @imports, $ln;
}
close(IMPORTS);

open(PAGENO, "grep coltocauthor ./MasterOLS-2side.toc|") or die "nogrep4";
while (defined($ln = <PAGENO>)) {
  chomp $ln;
  push @pagenos, $ln;
}
close(PAGENO);

$i = 0;
foreach my $author (@authors) {
  if (!defined($author) or !$author) {
     print STDERR "Hey, undef author!\n";
     exit 1;
  }
  $title  = $titles[$i];
  $import = $imports[$i];
  $pageno = $pagenos[$i];
  if ($author =~ /coltocauthor{(.*)}/) {
     $a = $1;
     if ($a =~ /(\S+ )*(\S+)/) {
        $lastName = $2;
        $fileName = "Reprint-${lastName}-OLS2004.tex";
        $fileName =~ s/\'//;
        ### \setcounter{page}{176}
        ### \begin{papers}
        if ($pageno =~ /{.*}{(\d+)}$/) {
            open(OUT, ">$fileName") or die "cannot open $fileName";
            $p = $1 - 1;
            # print "$fileName\n";
            print OUT "$preface\n";
            print OUT '\setcounter{page}{' . $p . '}', "\n";
            print OUT '\begin{papers}[\clearpage]', "\n";
            print OUT "$author\n";
            print OUT "$title\n";
            print OUT "$import\n\n";
            print OUT "$ender\n";
            close(OUT);
            # print "    pageno: $p\n\n";
        } else {
          print STDERR "Hey, no pageno: $pageno\n";
        }
     }
     # print STDERR "Got author: $a\n  lastname: $lastName\n";
  } else {
     print STDERR "Hey, no author here: $author\n";
  }
  $i++;
}


