#!/usr/bin/perl

# An abject hack, but produces something that can be tidied up by hand.

$in = 0;
print "<html>\n<title></title>\n<body>\n<table>";
while (defined($ln = <STDIN>)) {
    chomp $ln;
    next if ($ln =~ /^\s*$/);
    next if ($ln =~ /^\s*%/);

    if ($ln =~ /coltocauthor{(.*?)}/) {
        $foo = $1;
        $foo =~ s/\\//g;
	print "<tr>\n  <td>$foo</td>\n";
	$in = 1;
    }
    if ($ln =~ /coltoctitle{(.*?)}/) {
	$title = $1;
    }
    if ($ln =~ /import{(.*)}/) {
	$fyle = $1;
	if ($fyle ne 'missing') {
	    print "  <td><a href=\"${fyle}.pdf\">${title}</a></td>\n</tr>\n";
	} else {
	    print "  <td>${title}</td>\n</tr>\n";
	}
	$in = 0;
    }
}
print "</table>\n</body>\n</html>\n";

