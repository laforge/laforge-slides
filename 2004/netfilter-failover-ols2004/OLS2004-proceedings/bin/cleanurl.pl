#!/usr/bin/perl -w
my $ln;


while (defined($ln = <STDIN>)) {
    chomp $ln;
    next if ($ln =~m/^\s*>linuxsymposium\s+/);
    next if (($ln =~ m/^\s*Content\s+/) && ($ln =~ m/\s+Related\s*$/));
    next if (($ln =~ m|^\s*Register/Login\s+|) && ($ln =~ m/\s+Venue\s*$/));
    next if (($ln =~ m/^\s*Paper\s+/) && ($ln =~ m/\s+Travel\s*$/));

    if (($ln =~ m/^\s*Presentations\s+/) && ($ln =~ m/\s+FAQ\s*$/)) {
	$ln =~ s/^\s*Presentations\s+//;
	$ln =~ s/\s+FAQ\s*$//;
    }
    $ln =~ s/^\s*Tutorials\s+//;
    if (($ln =~ m|^\s*BOFS/Meetings\s+|) && ($ln =~ m/\s+Archives\s*$/)) {
	$ln =~ s|^\s*BOFS/Meetings\s+||;
	$ln =~ s/\s+Archives\s*$//;
    }
    $ln =~ s/^\s*Sponsors\s+//;
    if (($ln =~ m/^\s*Contacts\s+/) && ($ln =~ m/\s+Photos\s*$/)) {
	$ln =~ s/^\s*Contacts\s+//;
	$ln =~ s/\s+Photos\s*$//;
    }
    if (($ln =~ m/^\s*Information\s+/) && ($ln =~ m/\s+200\d\s*$/)) {
	$ln =~ s/^\s*Information\s+//;
	$ln =~ s/\s+200\d\s*$//;
    }
    if (($ln =~ m/^\s*Home\s+/) && ($ln =~ m/\s+200\d\s*$/)) {
	$ln =~ s/^\s*Home\s+//;
	$ln =~ s/\s+200\d\s*$//;
    }
    $ln =~ s/^\s+200\d\s*$//;
    $ln =~ s/^\s+199\d\s*$//;
    $ln =~ s/\s+200\d\s*$//;
    $ln =~ s/\s+199\d\s*$//;
    $ln =~ s/\s+Proceedings\s*$//;
    $ln =~ s/\s+Valid\sXHTML\s*$//;
    $ln =~ s/\s+1\.0!\s*$//;
    $ln =~ s/^\s+//g;
    $ln =~ s/\s+$//g;

    print '% ' if ($ln =~ m/^http:/i);
    print "$ln\n";

    #  if (($ln =~ m/^\s*\s+/) && ($ln =~ m/\s+\s*$/));
    #  if (($ln =~ m/^\s*\s+/) && ($ln =~ m/\s+\s*$/));
    
    # next if (($ln =~ m/^\s*\s+/) && ($ln =~ m/\s+\s*$/));
    # next if (($ln =~ m/^\s*\s+/) && ($ln =~ m/\s+\s*$/));
    # next if (($ln =~ m/^\s*\s+/) && ($ln =~ m/\s+\s*$/));
    # next if (($ln =~ m/^\s*\s+/) && ($ln =~ m/\s+\s*$/));
    # next if (($ln =~ m/^\s*\s+/) && ($ln =~ m/\s+\s*$/));
    # next if (($ln =~ m/^\s*\s+/) && ($ln =~ m/\s+\s*$/));
    # next if (($ln =~ m/^\s*\s+/) && ($ln =~ m/\s+\s*$/));
    # next if (($ln =~ m/^\s*\s+/) && ($ln =~ m/\s+\s*$/));
    # next if ($ln =~ m/\s+\s*$/);
    # next if ($ln =~ m//);

}
