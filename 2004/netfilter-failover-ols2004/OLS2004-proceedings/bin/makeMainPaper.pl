#!/usr/bin/perl -w

#
# Creates all papers with minimal content.  Caution: clobbers existing papers!
# Run from top-level directory (GCC2004 or OLS2004), and relies on the
# Master.tex file being correct.
#
# It's probably easier to make a copy of TEMPLATES/Blank.tex and edit
# it by hand than to mess with this script.  But it's included anyway,
# just in case it's handy.

open(IN,'<MasterOLS.tex') || die 'Cannot open MasterOLS.tex';
my $inEntry = 0;
my $author = '';
my $instit = '';
my $title = '';
my $dirName = '';
my @hrefs;
my $paperCount = 0;
my @keyNames = qw(__TITLE__ __AUTHOR__ __INSTITUTION__ __EMAIL__ __ABSTRACT__);

while (defined($ln = <IN>)) {
    chomp $ln;
    next if ($ln =~ m/^\s*%*\s*$/); # skip blanks/comments
    if (($ln =~ m/coltocauthor/) || (1 == $inEntry)) {
	if ($ln =~ m/coltocauthor{(.*?)}\s+%\s+(.*)/) {
	    $author = $1;
	    $instit = $2;
	    $inEntry = 1;
	    $instit =~ s/\s+$//g;
	} elsif ($ln =~ m/coltoctitle{(.*?)}/) {
	    $title = $1;
	} elsif ($ln =~ m/import{([^}]+)}/) {
	    $dirName = $1;
	    push @hrefs, {
		__AUTHOR__      => $author, 
		__INSTITUTION__ => $instit, 
		__TITLE__       => $title, 
		__EMAIL__       => 'your@email.address',
		dirName         => $dirName,
		__ABSTRACT__    => ($dirName . '-abstract')};
	    $author = '';
	    $instit = '';
	    $title = '';
	    $dirName = '';
	    $inEntry = 0;
        } elsif ($ln =~ m/label{gccart/) {
	    $paperCount++;
	}
    }
}
close(IN);

print "found: count: $paperCount with ", (1 + $#hrefs), " entries\n";
my $i = 1;
for my $h (@hrefs) {
    my $paper = $h->{dirName} . '/' . $h->{dirName} . '.tex';
    printf "%02d: %s: %s | %s | %s | %s\n", $i, $paper,
       $h->{__AUTHOR__}, 
       $h->{__INSTITUTION__}, 
       $h->{__TITLE__}, 
       $h->{dirName};
    $i++;

    open(OUT, ">$paper") || die "Cannot open $paper for writing";
    open(IN, '<TEMPLATES/Blank.tex') || die 'Cannot open TEMPLATES/Blank.tex for reading';
    while (defined($ln = <IN>)) {
	chomp $ln;
	for my $r (@keyNames) {
	    my $v = $h->{$r};
	    $ln =~ s/$r/$v/;
	}
	print OUT "$ln\n";
    }    
    print OUT "\n";
    close(IN);
    close(OUT);
}



