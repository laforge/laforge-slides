#!/usr/bin/perl -w

open(IN,'<all.txt') || die 'cannot open all.txt for read';
my @recs;
my $ref;
# http://www.linuxsymposium.org/2004/view_abstract.php?content_key=24
my $aurl='http://linuxsymposium.org/2004/view_abstract.php?content_key=';
my %fields=(A=>1,I=>1,T=>1,E=>1,N=>1,);
my @keyNames = qw(__TITLE__ __AUTHOR__ __INSTITUTION__ __EMAIL__ __ABSTRACT__);

sub newrec {
    my $r = {};
    for my $i qw(A I T E dname abstract) {
	$r->{$i} = '';
    }
    $r->{'N'} = 0;
    for my $i (@keyNames) {
	$r->{$i} = 'BUG';
    }
    return $r;
}
sub doItem {
    my ($r, $l) = @_;
    my ($f, $rest) = split /=/,$l;
    die "bad record: $l" if (!exists($fields{$f}));
    $rest =~ s/\s+$//g;
    $rest =~ s/^none$/~/;
    $r->{$f} = $rest;
    if ($f eq 'A') {
	my @fname = split /\s+/,lc($rest);
	my $aname = pop(@fname);
	$aname =~ s/[[:punct:]]//g;
	$r->{dname} = $aname;
	$r->{__AUTHOR__} = $r->{A};
	$r->{__ABSTRACT__} = ($r->{dname} . '-abstract');
    } elsif ($f eq 'N') {
	$r->{abstract} = ($aurl . $rest);
    } elsif ($f eq 'T') {
	$r->{__TITLE__} = $r->{T};
    } elsif ($f eq 'I') {
	$r->{__INSTITUTION__} = $r->{I};
    } elsif ($f eq 'E') {
	$r->{__EMAIL__} = $r->{E};
    }
}

$ref = newrec();
while (defined($ln = <IN>)) {
    chomp $ln;
    if ($ln =~ m/^\s*$/) {
	push @recs, $ref if ($ref);
	$ref = newrec();
    } else {
	doItem($ref, $ln);
    }
}
close(IN);

# print "got $#recs records\n";
my $cnt = 1;
open(OUT,'>Authors.tex') || die "cannot open Authors.tex for write";
open(CLEAN,'>Cleanup.sh') || die "cannot open Cleanup.sh for write";
print CLEAN '#!/bin/bash', "\n\n";
for my $r (@recs) {
    print OUT '% email=';
    print OUT $r->{E}, "\n";
    print OUT '% url=', $r->{abstract}, "\n";

    print OUT '\coltocauthor{';
    $r->{A} =~ s/\./.\\/g;
    print OUT $r->{A};
    print OUT '}', "\n";

    print OUT '\coltoctitle{';
    print OUT $r->{T};
    print OUT '}', "\n";

    print OUT '\label{';
    printf OUT "art%02d", $cnt;
    print OUT '}', "\n";

    print OUT '\import{';
    print OUT $r->{dname};
    print OUT '}', "\n";
    print OUT "\n";
    print CLEAN 'rm -rf ', $r->{dname}, "\n";

    my $paper = $r->{dname} . '/' . $r->{dname} . '.tex';
    my $setup = $r->{dname} . '/setup.sh';
    if (! -d $r->{dname}) {
	mkdir($r->{dname});
	if (! -f $paper) {
	    open(OWT, ">$paper") || die "Cannot open $paper for writing";
	    open(INN, '<TEMPLATES/Blank.tex') || die 'Cannot open TEMPLATES/Blank.tex for reading';
	    my $ln;
	    while (defined($ln = <INN>)) {
		chomp $ln;
		for my $x (@keyNames) {
		    my $v = $r->{$x};

		    $ln =~ s/$x/$v/;
		}
		print OWT "$ln\n";
	    }    
	    print OWT "\n";
	    close(INN);
	    close(OWT);
	}
	if (! -f $setup) {
	    open(OWT, ">$setup") || die "Cannot open $setup for writing";
	    print OWT '#!/bin/bash', "\n\n";
	    for my $i qw(ols-fonts.tex ols.sty zrl.sty) {
		print OWT 'ln -s ../TEMPLATES/', $i, ' . || /bin/true', "\n";
	    }
	    print OWT 'cat ../TEMPLATES/ProtoMake | sed -e ', "'s/TOP=/TOP=",
	    $r->{dname}, "/' > Makefile\n";
	    print OWT 'echo -n ', "'%'", '> ', "'", $r->{__ABSTRACT__}, ".tex'", "\n";
	    print OWT "links -dump '", $r->{abstract}, "' | ../bin/cleanurl.pl >> ",
	              $r->{__ABSTRACT__}, '.tex', "\n";
	    close(OWT);
	}
    }
    $cnt++;
}
close(OUT);
close(CLEAN);
