\documentclass[twocolumn,12pt]{article}
\usepackage{ols}
\ifpdf
\usepackage[pdftex]{epsfig}
\else
\usepackage{epsfig}
\fi
\input{ols-fonts}

\begin{document}

% Required: Do not print the date.
\date{}

\title{Formatting Tips and Tricks: \\
  {\normalsize Some potentially helpful examples}}

\author{
John W.\ Lockhart \\
{\em Red Hat, Inc.}\\
{\tt\normalsize lockhart@\{oco.net,redhat.com\}}\\
\and
Optional Second Author\\
{\em Second Institution}\\
{\tt\normalsize another@address.for.email.com}\\
} % end author section

\maketitle

% Required: Suppress page numbers on title page
\thispagestyle{empty}

\section*{Abstract}
This example paper contains tips and tricks to ensure that what you
write is what appears in the \textit{Proceedings} with as little
editing as possible.  The most important parts are at the end; please
read them.

If you are new to {\LaTeX}, please read this paper in its entirety,
and check out its source and any other \texttt{.tex} files in the
\texttt{\small EXAMPLE} directory.  

If you have a paper from OLS-2002 or the 2003 Linux Symposium or GCC
Summit, and would like to crib from its final formatting, please drop
me a note and I'll be happy to send along the edited source.
Likewise, if you would like a copy of the final edited form of this
year's source, just let me know.

The tree was created based on the information on the conference
website.  If you don't have a subdirectory, create one along the same
lines.  Blank materials are in the \texttt{\small TEMPLATES}
directory; \texttt{ProtoMake} and \texttt{Blank.tex} are probably the
most interesting files.  Likewise, if your Abstract was available when
I looked, it has been included.  Feel free to edit it; it's just there
to get you started and to provide an example of how to properly
include files should you need to.

 
\section{Simple Formatting Tricks}

\LaTeX\ is just a fancy markup language\ldots \textit{most} of the
time.

Some of the more common font and layout conventions follow:
\begin{itemize}
\item \texttt{texttt} produces \texttt{typewriter} style.
\item \texttt{textit} produces \textit{italics}.
\item \texttt{textbf} produces \textbf{boldface}.
\item \texttt{textsc} produces \textsc{small caps}.
\item \texttt{\textit{Font}} \textbf{\textsc{styles}} can be
      \textit{\textbf{combined}}\footnote{Often eye-breakingly. Restraint is Good.}
\end{itemize}

Paragraphs
   can  be      awfully messy
in the source, and even
% what, a comment?
have comments interspersed.  Be careful with % unintentional
percent signs---75\% of the time you'll accidentally comment out the
rest of the text on the line.

Unescaped dollar signs will put you into math mode, so be likewise
careful.  Of course, that's sometimes exactly where you \textit{want}
to be.

Tildes do not produce tildes in \LaTeX ---think instead of
\textsc{html}'s \texttt{\&nbsp;} and you'll get the picture.  Instead,
you can use \texttt{{\textbackslash}{\~{}}\{\}} or
\texttt{{\textbackslash}textasciitilde} to produce a tilde.  
Table~\ref{lockhart-tab1} provides a list of characters that require
special handling.  Note that tables may ``float''---that is, {\LaTeX}
might move your table to a place where it all fits on a single page,
rather than putting it exactly where you have included it in your
source. 
%
% that's 
%    \~{} 
% or
%    \textasciitilde
% for a tilde (without all the extra typesetting).
% Escape anything but a backslash by using a backslash.  Backslash
% itself is \textbackslash (as seen above).

\begin{table}[!th]
\centering
\begin{small}
\begin{tabular}[b]{c|c|p{2.3cm}}
Char & Command & Otherwise \\
\hline
% #
\# & \texttt{{\textbackslash}\#} & argument number \tabularnewline
\hline
% $
\$  & \texttt{{\textbackslash}\$} & toggle math mode \tabularnewline
\hline
% %
\%  & \texttt{{\textbackslash}\%} & comment: ignore rest of line \tabularnewline
\hline
% &
\&  & \texttt{{\textbackslash}\&} & tabstop \tabularnewline
\hline
% _
\_  & \texttt{{\textbackslash}{\_}} & subscript in math mode \tabularnewline
\hline
% {
\{ & \texttt{{\textbackslash}\{} & open environment \tabularnewline
\hline
% }
\} & \texttt{{\textbackslash}\}} & close environment \tabularnewline
\hline
% ~
{\~{}}       & \texttt{{\textbackslash}{\~{}}\{\}} & non-breaking space \tabularnewline
{\textasciitilde}       & \texttt{{\textbackslash}textasciitilde} & non-breaking space \tabularnewline
\hline
% \
{\textbackslash} & \texttt{{\textbackslash}textbackslash} & begin command \tabularnewline
\end{tabular}
\end{small}
\caption{{\LaTeX} characters that require special handling}
\label{lockhart-tab1}
\end{table}

\subsection{New Macros}\label{lockhart-newmacros}

A number of macros based on the \texttt{url} package have been created
for this year.  They are:
\begin{itemize}
\item \ident{ident} -- intended for identifiers,
  \texttt{{\textbackslash}ident\{some\_text\}} sets the text in
  \texttt{tt} and may break the line at any punctuation.  Spaces are deleted.
\item \ident{lident} -- intended for long identifiers, this works the
  same as \ident{ident}, but sets the text in a smaller font.
\item \ident{code} -- intended for short excerpts of code, this works
  like \ident{ident}, except that spaces are preserved.  Lines are not
  broken on spaces.
\item \ident{lcode} -- intended for longer excerpts of code, this works
  like \ident{code}, except that text is set in a smaller font.  This
  probably does not work correctly for multi-line code fragments;
  consider using the \texttt{cprog} package for that.
\item \ident{brcode} -- intended for excerpts of source code, this works
  like \ident{code}, except that line breaks may occur at spaces.
\item \ident{lbrcode} -- intended for excerpts of source code, this works
  like \ident{brcode}, except that text is set in a smaller font.
\end{itemize}

Examples are shown in Table~\ref{lockhart-macro-examples}.

\begin{table*}[tb]
\begin{itemize}
\item \verb|\ident{a_long_identifier}| --- this example in turn yields \ident{a_long_identifier}

\item \texttt{{\textbackslash}lident|an\_even\_lon ger\_identifier|} --- this
  in turn
  yields \lident|an_even_lon ger_identifier|

\item \verb|\lcode{int un_useful(int *a) { return *a; }}| --- this
  yields
  \lcode{int un_useful(int *a) { return *a; }}

\item \verb|\lbrcode{int un_useful(int *a) { return *a; }}| --- this
  yields
  \lbrcode{int un_useful(int *a) { return *a; }}

\end{itemize}
\caption{Examples of New Macros}
\label{lockhart-macro-examples}
\end{table*}

\section{Typesetting conventions}

You shouldn't have to worry too much here, but I'll illustrate a few
things.

Quotation marks, both `single' and ``double,'' look good in body text,
while other \texttt{"styles"} might look better for other uses.  Note
that when you're typesetting for a compiler, punctuation goes outside
the \texttt{"quotation marks",} but punctuation is placed
\textit{inside} the quotation marks for ``narrative.''

There are multiple flavors of dashes---the em dash, the en--dash, the
oft-used hyphen, and the minus sign (math mode: $2x - 3$).

\subsection{Choices for uniformity}

For source code, we have chosen the common style of not beginning a
line with a comma.  The compiler doesn't care, but keeping the printed
page consistent between papers is useful.

Identifiers may need to be split between lines, so we use a typewriter font
and mark up the string appropriately:
\texttt{sys\_\linebreak[0]sched\_\linebreak[0]yield()} or
\texttt{\small A\_\linebreak[0]REALLY\_\linebreak[0]LONG\_\linebreak[0]IDENTIFIER\_\linebreak[0]THAT\_\linebreak[0]NEEDS\_\linebreak[0]TO\_\linebreak[0]BE\_\linebreak[0]THIS\_\linebreak[0]LONG}
would be good examples\footnote{Alternatively, see the macros in
Section~\ref{lockhart-newmacros}.}.  To tell {\LaTeX} that an unhyphenated line
break is okay if required, just use \texttt{{\textbackslash}linebreak[0]}.

\subsection{Points of English}

A few nitpicks:
\begin{enumerate}
\item \textit{it's} is a macro which expands to \textit{it is}.  It
      has no other meaning. 
\item \textit{its} is possessive.
\item Items in a series are:  \textit{a}, \textit{b}, and \textit{c}.  
      Never \textit{a}, \textit{b} and \textit{c}.  This rule makes it
      much simpler when you must use complex values of (for example)
      \textit{b}.  For truly long constructs, you may use a semicolon
      as a delimiter rather than a comma.
\item Some phrases should be hyphenated---for instance, when you're
  using an adjective to modify another adjective, or a noun that
  appears before another.  A high-performance system; a win-win
  situation; a high-level loop transformation; a slow-moving train,
  but a slowly moving car; that sort of thing.  Most of the time,
  people will still be able to parse the results easily if the sentence isn't
  perfect. 
\item Be happy, know your homonyms.  There, they're, their.  To, two,
      too.  Your, you're.  And so forth.  Spelling checkers show their
      limitations on this\ldots
\end{enumerate}

Of course, proofreading is a wonderful thing, and every bit of it you
(or any guinea pigs you can persuade) do is a Good Thing.  I'll
correct what I notice, but I have only two eyes and there's a lot of
margin-crunching formatting to be done.  There are certain
times, often with non-native speakers, where I'm not clear on the
meaning.  If I catch something like that in time, I'll ask; if not,
chances are that I'll keep my hands off of the section in question so
as not to insert a woefully incorrect meaning. 

\section{Tools}

It helps to have the following installed on your system:
\begin{itemize}
\item \textbf{\tt tetex}.  The most common \TeX\ package for Linux.  
\item \textbf{\tt transfig}.  Graphics in \texttt{.fig} format,
  useful for figures.
\item \textbf{\tt dia}.  Also useful for figures.
\item \textbf{\tt ImageMagick}.  Great for photographs and graphics
  manipulation \& conversion.
\item \textbf{\tt xpdf} or \textbf{\tt acroread} for viewing PDF files.
\item Utilites often found in {\tt tetex}, but which your distribution
  may have packaged separately:  \texttt{xdvi}, \texttt{dvips},
  \texttt{pdflatex}. 
\item \textbf{\tt ghostscript} for handling Postscript.
\end{itemize}

\section{Examples}

Some examples from previous conferences have been included
in this package; hopefully they'll be useful in handling code
examples.  Reducing everything to \texttt{footnotesize} or setting it
\texttt{verbatim} won't magically make it fit on the page, alas.  Have
a look in the \texttt{EXAMPLE} directory to find these items:
\begin{itemize}
\item {\raggedright \texttt{\small bibli\-og\-raphy.tex}, \texttt{\small bibli\-og\-ra\-phy2.tex}, and
  \texttt{\small ref\-er\-ences.tex}.  Different ways of citing any relevant
  works external to your paper.}
\item \texttt{conditional.tex}.  If you have {\LaTeX} code that works
  only by itself and need to do conditional processing, here's an example.
\item \texttt{\small complexCode/complexFigure.tex}.  An example of a complex
  figure containing side-by-side C code.
\item \texttt{figures.tex}.  Different ways of doing figures.
\item \texttt{includegraphics.tex}.  Different ways to include graphics.
\item \texttt{legalese.tex}.  Legal disclaimers.
\item \texttt{multipleAuthors.tex}.  Formatting examples for multiple authors.
\item \texttt{tables.tex}.  Different ways to do tables.
\end{itemize}

\subsection{Bad Examples}

A prior year's paper gave the example of setting \texttt{verbatim}
sections in \texttt{tt}.  Repetitiously and redundantly enough, that's
the default.  So, please, no instances of
\begin{verbatim}
  {\tt
  \begin{verbatim}
   ...
\end{verbatim}

\begin{small}
\centering
\textbf{Corrected.}  You might, however, wish to do something like this instead:
\begin{verbatim}
  \begin{small}
  \centering
  \textbf{Corrected.}  You ...
  \begin{verbatim}
    ...
\end{verbatim}
\end{small}
Of course, check the source of this document
(\lident{EXAMPLE/myPaper.tex}) for more ideas.  Valid font sizes, for
instance, include \texttt{normalsize}, \texttt{small},
\texttt{footnotesize}, \texttt{scriptsize}, and \texttt{tiny}.  Please
don't use anything larger than \texttt{normalsize}.


Another extant bad example is the practice of ending paragraphs with a
double backslash (\texttt{\textbackslash\textbackslash}) \textit{and}
a blank line.  This creates unwanted, superfluous whitespace between
paragraphs.  \LaTeX\ is, believe it or not, supposed to be easy.  Just
leave one or more blank lines between paragraphs and you'll be fine.


\section{Style packages}

I've included the \texttt{combine} package used for last year's
\textit{Proceedings}.  Just copy (or move) the \texttt{texmf}
directory to your home directory.  You should then be able to use the
``BigBuild'' script to produce a sample \textit{Proceedings}.

One environment is setting necessary to make everything work:
\begin{center}
{\footnotesize \texttt{export TEXINPUTS='.//:\$\{LOCALTEX\}//:'}}
\end{center}
%
% or for those of you who'd like to cut'n'paste from the source:
% export TEXINPUTS='.//:${LOCALTEX}//:'
%
If you add the above to your \texttt{\textasciitilde/.bashrc}, you can
dispense with \texttt{BigBuild} and just use \texttt{make}.

Should you wish to download and install the latest and greatest
version of \texttt{combine}, it may be found at
\begin{center}\small
\texttt{http://www.tex.ac.uk\linebreak[0]/tex-archive\linebreak[0]/macros\linebreak[0]/latex\linebreak[0]/contrib\linebreak[0]/supported\linebreak[0]/combine}
\end{center}

The most common cause of build problems is including style packages
that aren't compatible with \texttt{combine}.  Unfortunately, this
includes\footnote{At least using last year's versions, that was the case.}
things like \texttt{hyperref} and \texttt{html}---two
otherwise-wonderful packages for handling URLs and such.

\section{Graphics and Symbols}

For importing graphics, don't forget to omit any file extensions.
That's because \texttt{latex} and \texttt{pdflatex} look for
different formats.

The easiest ways to get special symbols such as
Registered\textregistered\ and Trademark\texttrademark\ 
is to use the \LaTeX\ 2e \texttt{{\textbackslash}text} constructs:
thus, \texttt{{\textbackslash}textregistered} and 
\texttt{{\textbackslash}texttrademark}.

\section{\TeX\ References}

See \texttt{\small http://www.tug.org/} and especially 
\texttt{\small http://www.tug.org/begin.html} for
online and paper references.

For a free and extremely useful document, try:
\texttt{\small http://www.tug.org\linebreak[0]/tex-archive\linebreak[0]/info\linebreak[0]/lshort\linebreak[0]/english\linebreak[0]/lshort.pdf}.  
Note that translations\footnote{French, for instance:
\url{http://www.tug.org/tex-archive/info/lshort/french/flshort-3.20.pdf};
note also that this section of the Example paper shows different ways
of handling URLs.}
are available, for those more comfortable in something other than
English: 
\texttt{\small http://www.tug.org\linebreak[0]/tex-archive\linebreak[0]/info\linebreak[0]/lshort/}

%%% Cut'n'paste versions of those URLs:
% http://www.tug.org/tex-archive/info/lshort/english/lshort.pdf
% http://www.tug.org/tex-archive/info/lshort/french/flshort-3.20.pdf
% http://www.tug.org/tex-archive/info/lshort/

I tend to use \textit{A Guide to \LaTeX} (Kopka \& Daly, ISBN 0-201-39825-7) and the
\textit{\LaTeX\ Graphics Companion} (Goossens, Rahtz, \& Mittelbach)
the most these days.

You are also welcome to send questions to me at
\texttt{{lockhart}{@}{redhat.com}} (work) or
\texttt{{lockhart}{@}{oco.net}} (home).
%
% {}'s begin a new environment in TeX, as in C.
% A few extra {}'s might let an email address escape notice 
% by spammers' collecting 'bots, should the .tex file wind 
% up on a website somewhere at some point.
%

As usual, please refrain from submitting anything remotely resembling
a Microsoft Word \texttt{.doc} file\ldots \texttt{<grimace>}.  It's a
\textit{lot} easier for me to fix up plain ASCII text and
convert/insert accompanying graphics, if you find yourself terminally
confused or in a dire emergency.

\begin{figure}[!ht]
\begin{center}
\begin{footnotesize}
\begin{verbatim}
  cd yourLastName
  make clean
  cd ..
  tar zcf yourLastName.tar.gz \
      yourLastName
\end{verbatim}
\end{footnotesize}
\caption{Submitting a paper}
\end{center}
\label{lockhart-fig1}
\end{figure}

\section{Simple rules to keep your formatting team happy}
\begin{enumerate}
\item To submit your paper, just \texttt{make clean} in your
  directory, \texttt{tar} it up, and send the resulting gzipped tarball to
  \texttt{papers@linuxsymposium.org} or \texttt{papers@gccsummit.org},
  as appropriate.  See Figure~\ref{lockhart-fig1} for an example.
\item Use the existing directory structure, please.  The directory
  names are intended to be the last name of the presenter (lowercase,
  punctuation omitted); the main paper should be
  \texttt{lastname.tex} and any additional files should be
  \texttt{lastname-file.extension}.   This is because we use the
  \texttt{combine} package to put all the papers together, and
  instruct {\LaTeX} to search the entire (sub)directory hierarchy for
  input files.  You don't want someone else's file by mistake, right?
  Putting your name on it helps to keep things straight.  The same
  goes for \verb|\label{}| and \verb|\ref{}| commands.
\item Omit file extensions and pathnames in your {\LaTeX} source,
  please.  By omitting the path and just saying \texttt{{\textbackslash}input\{lockhart-abstract\}},
  a paper can be built from both its directory and from its
  parent directory.  For graphics, omitting the extension lets \texttt{latex} or
  \texttt{pdflatex} pick its preferred input format for the best
  possible results.
\item No proprietary document/graphics formats, please.  This especially means MS
  Office, Visio, or other such tools.  \LaTeX\ can, however, import
  EPS and PDF, if you can save in those formats.
\item Originals, please.  For example, if you have photographs, send
  along the full-resolution JPG (crop out any undesired elements if
  necessary, but use the maximum resolution).  For diagrams, the XFig or Dia files.
  This ensures the best possible print quality.  Printing will be in
  black and white, but the online PDF's will be in full color.  Your
  screen is probably about 72dpi, but the typesetter is probably using
  something that's at least 1200dpi.  The more resolution, the better.
  Since hardcopy will be printed in Ottawa, the papersize will be
  North American ``letter.''  Please keep that in mind if you are
  concerned about page breaks and such.
\item Do \textbf{\textit{not}} use sans-serif fonts, or go changing
  global font sizes.  We're using 12-point Times Roman for body text.
  Likewise, please don't go haywire with italics.  I once received a
  huge collection of tables, each of which set the font size and face
  on an item-by-item basis.  \textit{Incorrectly}.  
\item The Postscript--to/from--PDF conversion tools aren't always a
  good choice\ldots try it yourself and see.  There's a good reason we
  use \texttt{pdflatex} directly\ldots
\item Those of you who like to begin lines of code with commas:  as
  previously mentioned, we're
  typesetting the code with the comma attached to the preceding
  identifier (as most publishers do).  Feel free to post your
  preferred version to the web and to refer to it in the paper.
\item If possible, please avoid trivial new macros.  Should you need
  to add something, though, please use
  \texttt{{\textbackslash}providecommand} rather than
  \texttt{{\textbackslash}newcommand}, and preface the command with
  your last name.  This minimizes naming conflicts in the global
  namespace of \texttt{combine}, and helps to ensure that you get the
  macro that you want.
\item Trivia note:  generally speaking, it takes longer to edit a
  submission from a {\TeX}spert than plain, unmarked ASCII.  If you
  consider yourself a {\LaTeX} expert and love to write fancy new
  commands, please consider contributing clean-ups or well-tested
  new features for the infrastructure rather than customizing the
  daylights out of your submission.  Thanks!
\end{enumerate}

This paper builds correctly using the tetex-1.0.7-66 package on Red
Hat Linux 9, and also on Fedora Core 2 (Test 1) with tetex-2.0.2-12.1.
Other distributions haven't been tested, but should work.  If you run
into problems, please let me know.

And remember, it's only typesetting, not rocket science.  Or hacking
compilers or kernels.  \texttt{:-)}  Have some fun along the way\ldots

\end{document}
