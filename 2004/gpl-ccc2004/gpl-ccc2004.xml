<?xml version='1.0' encoding='ISO-8859-1'?>

<!DOCTYPE article PUBLIC '-//OASIS//DTD DocBook XML V4.3//EN' 'http://www.docbook.org/xml/4.3/docbookx.dtd'>

<article id="gpl-enforcement-ccc2004">

<articleinfo>
	<title>Enforcing the GNU GPL - Copyright helps Copyleft</title>
	<authorgroup>
		<author>
			<personname>
				<firstname>Harald</firstname>
				<surname>Welte</surname>
			</personname>
		<!--
			<personblurb>Harald Welte</personblurb>
				<affiliation>
					<orgname>netfilter core team</orgname>
					<address>
						<email>laforge@netfilter.org</email>
					</address>
				</affiliation>

			-->
			<email>laforge@gpl-violations.org</email>
		</author>
		</authorgroup>
	<copyright>
		<year>2004</year>
		<holder>Harald Welte &lt;laforge@gpl-violations.org&gt; </holder>
	</copyright>
	<date>Dec 01, 2004</date>
	<edition>1</edition>
	<orgname>netfilter core team</orgname>
	<releaseinfo>
		$Revision: 1.4 $
	</releaseinfo>

	<abstract>
		<para>
More and more vendors of various computing devices, especially network-related
appliances such as Routers, NAT-Gateways and 802.11 Access Points are using
Linux and other GPL licensed free software in their products.
		</para>
		<para>
While the Linux community can look at this as a big success, there is a back
side of that coin:  A large number of those vendors have no idea about the GPL
license terms, and as a result do not fulfill their obligations under the GPL.
		</para>
		<para>
The netfilter/iptables project has started legal proceedngs against a number of
companies in violation of the GPL since December 2003.  Those legal proceedings
were quite successful so far, resulting in twelve amicable agreements and one
granted preliminary injunction.  The list of companies includes large
corporations such as Siemens, Asus and Belkin.
		</para>
		<para>
This paper and the corresponding presentation will give an overview about the
author's recent successful enforcement of the GNU GPL within German
jurisdiction.  
		</para>
		<para>
The paper will  go on describing what exactly is neccessarry to fully comply
with the GPL, including the author's legal position on corner cases such as
cryptographic signing.
		</para>
		<para>
In the end, it seems like the idea of the founding fathers of the GNU GPL
works:  Guaranteeing Copyleft by using Copyright.
		</para>
	</abstract>

</articleinfo>


<section>
<title>Legal Disclaimer</title>
<para>
The author of this paper is a software developer, not a lawyer.  The content of
this paper represents his knowledge after dealing with the legal issues of
about 20 gpl violation cases.
</para>
<para>
All information in this paper is presented on a nas-is basis.  There is no
warranty for correctness.
</para>
<para>
The paper does not comprise legal advise, and any details might be coupled to German copyright law (UrhG)
</para>
</section>

<section>
<title>What is copyrightable</title>
<para>
Since the GNU GPL is a copyright license, it can only cover copyrightable
works.  The exact definition of what is copyrightable and what not might vary
from legislation to legislation.
</para>
<para>
Software is considered the immaterial result of a creative act, and is treated
very much like literary works.  It might therefore be applicable to look at the
analogy of a printed book.
</para>
<para>
In order for a work to be copyrightable, it has to be non-trivial (German:
Sch&ouml;pfungsh&ouml;he).  Much like a lector of a book, anybody who just
corrects spelling mistakes, compiler warnings, or even functional fixes such as
fixing a signedness bug or a typecast are unlikely to be seen as a
copyrightable contribution to an existing work.
</para>
<para>
An indication for copyrightability can be the question:  Did the author have a
choice (i.e. between different algorithms)?  As soon as there are multiple ways
of getting a particular job done, and the author has to make decisions on which
way to go, this is an indication for copyrightability.
</para>
</section>

<section>
<title>The GNU GPL revisited</title>
<para>
As a copyright license, the GNU GPL mainly regulates distribution of a
copyrighted work, not usage.  To the opposite, the GNU GPL does not allow an
author to make any additional restrictions like <quote>must not be used for
military purpose</quote>.
</para>
<para>
As a summary, the license allows distribution of the source code (including
modifications, if any) if 
<itemizedlist>
<listitem>The GPL license itself is mentioned</listitem>
<listitem>A copy of the full license text accompanies every copy</listitem>
</itemizedlist>
</para>
<para>
The GPL allows distribution of the object code (including modifications) if
<itemizedlist>
<listitem>The GPL license itself is mentioned</listitem>
<listitem>A copy of the full license text accompanies every copy</listitem>
<listitem>The <quote>complete corresponding source code</quote> or a written offer to ship it to any third party is included with every copy</listitem>
</itemizedlist>
</para>
</section>

<section>
<title>Complete Source Code</title>
<para>
The GPL contains a very specific definition of what the term <quote>full source
code</quote> actually means in practise:
</para>
<quote><para>
... complete source code means all the source code for all modules it contains,
plus any associated interface definition files, plus the scripts used to
control compilation and installation of the executable.
</para></quote>
<para>
The interpretation of the paper's author of this (for C programs) is:
<itemizedlist>
<listitem>source code</listitem>
<listitem>Header Files</listitem>
<listitem>Makefiles</listitem>
<listitem>Tools for installation of a modified binary, even if they are not technically implemented as scripts</listitem>
</itemizedlist>
<para>
The general rule in case of any question is the intent of the license: To
enable the user to modify the source code and run modified versions.
</para>
<para>
This brings us to the conclusion that in case of a bundle of hardware and
software, the hardware can not be implemented in a way to only accept
cryptographically signed software, without providing either the original key,
or the option of setting a new key in the hardware.
</para>
</section>


<section>
<title>Derivative Work</title>
<para>
The question of derivative works is probably the hardest question with regard
to the GPL.  According to the license text, any derivative work can only be
distributed under the GPL, too.  However, the definition of a derivative work
is left to the legal framework of copyright.
</para>
<para>
The paper's author is convinced that any court decision would not look at the
particular technology used to integrate multiple software parts.  It is much
more a question of how much dependency there is between the two pieces.
</para>
<para>
If a program is written against a specific non-standard API, this can be
considered as an indication for a derivative work.  If a program is written
against standard APIs, and the GPL licensed parts that provide those APIs can
be easily exchanged with other [existing] implementations, then it can be considered as indication for no derivative work.
</para>
<para>
Unfortunately there is no precedent on this issue, so it's up to the first
court decisions on the issue of derivative works to determine.
</para>
</section>

<section>
<title>Collective Works</title>
<para>
<quote>... it is not the intent ... to claim rights or contest your rights to work written entirely by you; rather, the intent is to excercise the right to control the distribution of derivative or collective works ...</quote>
</para>
<para>
<quote>... mere aggregation of another work ... with the program on a volume of a storage or distribution medium does not bring the other work under the scope of this license</quote>
</para>
<para>
So the GPL allows <quote>mere aggregation</quote>, which is what e.g. the
GNU/Linux distributors like RedHat or SuSE do, when they ship GPL-licensed
programs together with a proprietary Macromedia Flash player on one CD- or
DVD-Medium.
</para>
<para>
Further research is required to determine what exactly would be a collective
work, and how far this is backed by copyright law.
</para>
</section>

<section>
<title>Non-Public Modifications</title>
<para>
Since the GPL regulates distribution and not use, any modifications that are
not distributed in any form do not require offering the source code.
</para>
<para>
Special emphasis has to be given on when distribution happens within the legal
context.
</para>
Undoubtedly, as soon as you distribute modifications to a third party, such as
a contractor or another company, you are bound by the GPL to either include the
full source code, or a written offer.  Please note that if you don't include
the source code at any given time, the written offer must be available to any third party!
</para>
<para>
Interestingly, at least in German copyright law, distribution can also happen
within an organization.  Apparently, as soon as a copy is distributed to a
group larger than a small number of close colleagues whom you know personally,
distribution happens - and thus the obligations of the GPL apply.
</para>
</section>

<section>
<title>GPL Violations</title>
<para>
The GPL is violated as soon as one or more of the obligations are not fulfilled.</para>
<para>
For this case, the GPL automatically revokes any right, even the usage right on
the original unmodified code.  So not only the distribution is infringing, also the mere use is no longer permitted.
</para>
<para>
This very strong provision is quite common in copyright licenses, especially in
the world of proprietary software.
</para>
</section>

<section>
<title>Past GPL Enforcement</title>
</section>

<section>
<title>The Linksys Case</title>
</section>

<section>
<title>Enforcement Case Timeline</title>
</section>

<section>
<title>Success so far</title>
</section>

<section>
<title>Future GPL Enforcement</title>
</section>

</article>

