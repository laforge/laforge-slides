Harald Welte is a data communications freelancer, enthusiast and hacker
who is working with Free Software (and particularly GNU/Linux)
since 1995  His major code contribution to the Linux kernel was as a
core developer of the netfilter/iptables packet filter.

He has co-started a number of other Free Software and Free Hardware
projects, mainly related to RFID such as librfid, OpenMRTD, OpenBeacon,
OpenPCD, OpenPICC. During 2006 and 2007 Harald became the co-founder of
OpenMoko, where he served as Lead System Architect for the worlds first
100% Open Free Software based mobile phone.

Aside from his technical contributions, Harald has been pioneering the legal
enforcement of the GNU GPL license as part of his gpl-violations.org project.
More than 150 inappropriate use of GPL licensed code by commercial companies
have been resolved as part of this effort, both in court and out of court. He
has received the 2007 "FSF Award for the Advancement of Free Software" and the
"2008 Google/O'Reilly Open Source award: Defender of Rights". 

In 2008, Harald started to work on Free Software on the GSM protocol side, both
for passive sniffing and protocol analysis, as well as an actual network-side
GSM stack implementation called OpenBSC.  In 2010, he expanded those
efforts by creating OsmocomBB, a GSM telephony-side baseband processor
firmware and protocol stack.  Other projects include
OsmocomTETRA, a receive-only implementation of the ETSI TETRA radio
interface.

Together with fellow developer Dieter Spaar, Harald has been giving many
incarnations of deeply technical trainings about mobile communications
protocols from the air inteface to the core network, with a special
emphasis on security.

Harald is co-founder of sysmocom GmbH, Berlin/Germany based company
working on innovative Free Software based products and solutions for
conventional and unconventional operators of mobile networks.  Said
projects are also used by various entities in research of mobile
security.
