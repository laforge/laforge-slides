OsmocomBB: Protocol stack and  baesband firmware for GSM mobile phones

By: Harald Welte[1]

The OsmocomBB project[2] is a Free Software implementation of the GSM
protocol stack running on a mobile phone.  

For decades, the cellular industry comprised by cellphone chipset makers and
network operators keep their hardware and system-level software as well as GSM
protocol stack implementations closed.  As a result, it was never possible
to send arbitrary data at the lower levels of the GSM protocol stack.
Existing phones only allow application-level data to be user-supplied, such as
SMS messages, IP over GPRS or circuit-switched data (CSD).  

Using OsmocomBB, the Free Software enethusiast as well as the security
researcher finally has a tool equivalent to an Ethernet card in the TCP/IP
protocol world:  A simple transceiver that will send arbitrary protocol
messages to a GSM network.

By the time Linux Kongress 2010 is held, it is expected that OsmocomBB
has proceeded to a level where it can make actual phone calls on any
GSM network.

[1] http://laforge.gnumonks.org/
[2] http://bb.osmocom.org/
