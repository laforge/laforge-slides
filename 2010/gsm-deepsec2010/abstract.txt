Deepsec 2010 GSM Security Workshop
======================================================================


* attacks from malicious phone 
  * RACH DoS using OsmocomBB
  * IMSI DETACH flood 
  * L2 fuzzing
  * BSC fuzzing using RR messages
  * MSC fuzzing using MM / CC messages
  * use 'emergency call' RACH but then regular SETUP
* passive attacks
  * GSM intercept using airprobe
  * extended GSM intercept with A5/1 decryption

* best security practises when deploying GSM
  * TMSI reallocation as often as possible
  * VLR large enough to never expire VLR records
  * offer A5/3 
  * don't offer A5/2
  * randomized padding of L2 frames
  * encrypted/authenticated backhaul
  * heuristics-based IMSI DETACH protection or DETACH disable
  * use 'late assignment' of TCH
  * use SMS over GPRS whenever possible
  * do SDCCH-reassignment on CS-SMS
  * always use frequency hopping over wide spectrum
  * make SI5/SI6 on SACCH less predictable
  * offer GEA3 and use whenever possible


In recent years, we have seen a significant increase of research in GSM
protocol-level and cryptographic security attacks:  The existing theoretical
weaknesses of A5/1 have been implemented and proven as practical, rainbow
tables have been computed and distributed widely on the internet.  A new
open-source GSM baseband software facilitates fine-grained control over all
information sent from a malicious user, enabling protocol fuzzing and flooding
attacks.

However, the publicly available attack tools are hard to use, and it is
difficult to reproduce the published attacks and assess how easy it is to
perform which type of attack on GSM networks.

This two-day workshop will re-visit all GSM security features and their
publicly know weaknesses.  It will introduce and demonstrate the various
publicly available attack tools;  Workshop participants will be trained
by the creators of the attack tools on how to use them against actual GSM
networks.

After extensive hands-on sessions performing the various attacks,
counter-measures will be presented, followed by a discussion of the best
current practises for configuring a secure-as-possible GSM network.

The target audience of this workshop is GSM network operators and IT security
consultants in the telecommunications industry.
