FOSS in the Mobile World
Reverse Engineering


How to find such a Linux-friendly device?

Look at hardware details of available devices
Use Google to find out what hardware they use
Use FCC database to get PCB photographs
Look at WM firmware images (registry/...)
At some point you buy one and take it apart

