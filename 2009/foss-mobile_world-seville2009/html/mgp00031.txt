FOSS in the Mobile World
Using HaRET


Using HARET
watch for IRQ changes/events
e.g. you see DMA3 interrupts while talking to the GSM
read MMIO config of DMA controller to determine user: SPI
read SPI controller configuration + DMA controller configuration
find RAM address of data buffers read/written by DMA
haretconsole writes logfiles
you can start to annotate the logfiles 
of course, all of this could be done using JTAG, too.
but with HaRET, you mostly don't need it!!!

