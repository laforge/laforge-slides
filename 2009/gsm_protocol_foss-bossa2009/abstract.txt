Free Software for the GSM protocol side

As we see an increased use of Free and Open Source Software (FOSS) in mobile
devices, some of them with GSM or UMTS cellular network connection, this FOSS
adoption exclusively happens on the "Application Processor" side.   To the
contrary, the GSM protocol stack on the 'digital baseband' CPU is typically
treated like if it was the biggest invention since sliced bread, and a most
valuable and proprietary one.

In fact, the entire GSM and UMTS protocol are specified in public documents,
available free of cost to anyone from the ETSI (European Telecommunications
Standardization Institute) or the 3GPP.

So there is no lacking information preventing any FOSS implementation of
those protocols.  It is merely the fact how the cellular industry is currently
structured.  It's an oligopol, with very few players in the market, and
artificial entry barriers for anyone who would want to compete with any part
of the stack.

In 2007, the THC GSM project developed tools called gsm-tvoid, gssm and gsmsp
as first humble attempts of an Open Source implementation of a GSM protocol
analyzer, based on the USRP and gnuradio software defined radio (SDR) platform.

In 2008, two independent Free Software projects for the GSM network side have
been release: OpenBTS and OpenBSC.

In 2009, Harald has been starting to work on the detailed hardware and software
architecture of an open source GSM development board, capable of running both
the MS (cellphone) and BTS (cell tower) side.

This presentation will give an overview about the existing FOSS efforts on the GSM
protocol side, their current status and future directions.
