Harald Welte is a freelancer, consultant, enthusiast, freedom fighter and
hcaker who is working with Free Software (and particularly the Linux kernel)
since 1995.  His first major code contribution to the kernel was within the
netfilter/iptables packet filter.

He has started a number of other Free Software and Free Hardware projects,
mainly related to RFID such as librfid, OpenMRTD, OpenBeacon, OpenPCD, OpenPICC.

During 2006 and 2007 Harald became the co-founder of OpenMoko, where he served
as Lead System Architect for the worlds first 100% Open Free Software based
mobile phone.

Aside from his technical contributions, Harald has been pioneering the legal
enforcement of the GNU GPL license as part of his gpl-violations.org project.
More than 150 inappropriate use of GPL licensed code by commercial companiess
have been resolved as part of this effort, both in court and out of court.

He has received the 2007 "FSF Award for the Advancement of Free Software" and
the "2008 Google/O'Reilly Open Source award: Defender of Rights".

Harald is currently working as "Open Source Liaison" for the taiwanese CPU,
chipset and peripheral design house VIA, helping them to understand how to
attain the best possible Free Software support for their components.

He continues to operate his consulting business hmw-consulting.
